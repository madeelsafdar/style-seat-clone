-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 30, 2019 at 02:59 AM
-- Server version: 10.1.38-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adeeqeah_wbi_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_action`
--

CREATE TABLE `acl_action` (
  `id` int(11) UNSIGNED NOT NULL,
  `path` varchar(200) DEFAULT NULL,
  `alias` varchar(200) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acl_action`
--

INSERT INTO `acl_action` (`id`, `path`, `alias`, `description`, `is_admin`) VALUES
(1, 'dashboard/index', 'Dashboard', NULL, 1),
(2, 'projects/index', 'Projects (Open Projects)', NULL, 1),
(3, 'projects/archived_projects', 'Archived Projects', NULL, 1),
(4, 'projects/myprojects', 'My Projects', NULL, 1),
(5, 'projects/project_tasklist', 'Update Tasks', NULL, 1),
(6, 'projects/add_project', 'Add Project', NULL, 1),
(7, 'projects/ajax_add_cost', 'Add Cost through Project Detail Page', NULL, 1),
(8, 'projects/edit_project', 'Edit Project', NULL, 1),
(9, 'projects/ajax_delete_project', 'Delete Project', NULL, 1),
(10, 'projects/change_tasks_order', 'Change Task Order', NULL, 1),
(11, 'projects/assignment_of_users', 'Change Assignment of Users Page', NULL, 1),
(12, 'projects/unarchive_project', 'Unarchive Project', NULL, 1),
(13, 'projects/project_details', 'Project Detail', NULL, 1),
(14, 'projects/ajax_delete_permanently_project', 'Delete Project Permanently', NULL, 1),
(15, 'projects/ajax_save_notes', 'Save Notes To Project', NULL, 1),
(16, 'groups/index', 'Groups', NULL, 1),
(17, 'groups/ajax_delete_group', 'Delete Group', NULL, 1),
(18, 'groups/edit_group', 'Edit Group', NULL, 1),
(19, 'groups/add_group', 'Add Group', NULL, 1),
(20, 'groups/change_permissions', 'Change Group Permissions', NULL, 1),
(21, 'phase_tasks/index', 'Tasks in a Phase', NULL, 1),
(22, 'phase_tasks/add_task', 'Add Task', NULL, 1),
(23, 'phase_tasks/edit_task', 'Edit Task', NULL, 1),
(24, 'phase_tasks/ajax_delete_phase_task', 'Delete Task', NULL, 1),
(25, 'tasks/ajax_change_task_status', 'Tasklist', NULL, 1),
(26, 'users/index', 'Users', NULL, 1),
(27, 'users/add_user', 'Add User', NULL, 1),
(28, 'users/edit_user', 'Edit User', NULL, 1),
(29, 'users/ajax_delete_user', 'Delete User', NULL, 1),
(30, 'project_types/index', 'Project Types', NULL, 1),
(31, 'project_types/add_project_type', 'Add Project Type', NULL, 1),
(32, 'project_types/edit_project_type', 'Edit Project Type', NULL, 1),
(33, 'project_types/ajax_delete_project_type', 'Delete Project Type', NULL, 1),
(34, 'departments/index', 'Departments', NULL, 1),
(35, 'departments/add_department', 'Add Department', NULL, 1),
(36, 'departments/edit_department', 'Edit Department', NULL, 1),
(37, 'departments/ajax_delete_department', 'Delete Department', NULL, 1),
(38, 'industry_sectors/index', 'Industry Sectors', NULL, 1),
(39, 'industry_sectors/add_industry_sector', 'Add Industry sector', NULL, 0),
(40, 'industry_sectors/edit_industry_sector', 'Edit Industry Sector', NULL, 1),
(41, 'industry_sectors/ajax_delete_industry_sector', 'Delete Industry Sector', NULL, 1),
(42, 'methods/index', 'Methods', NULL, 1),
(43, 'methods/add_method', 'Add Method', NULL, 1),
(44, 'methods/edit_method', 'Edit Method', NULL, 1),
(45, 'methods/ajax_delete_method', 'Delete Method', NULL, 1),
(46, 'regionals/index', 'Regionals', NULL, 1),
(47, 'regionals/add_regional', 'Add Regional', NULL, 1),
(48, 'regionals/edit_regional', 'Edit Regional', NULL, 1),
(49, 'regionals/ajax_delete_regional', 'Delete Regional', NULL, 1),
(50, 'currencies/index', 'Currencies', NULL, 1),
(51, 'currencies/add_currency', 'Add Currency', NULL, 1),
(52, 'currencies/edit_currency', 'Edit Currency', NULL, 1),
(53, 'currencies/ajax_delete_currency', 'Delete Currency', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `advertisement_id` int(11) NOT NULL,
  `advertisement_title` varchar(255) DEFAULT NULL,
  `advertisement_text` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`advertisement_id`, `advertisement_title`, `advertisement_text`) VALUES
(1, 'Advertisement 1', '<div style=\"background:red;height: 221px;width:100%;\">Advertisement 1 Here</div>'),
(2, 'Advertisement 2', '<div style=\"background:red;height: 221px;width:100%;\">Advertisement 2 Here</div>'),
(3, 'Advertisement 3', '<div style=\"background:red;height: 221px;width:100%;\">Advertisement 3 Here</div>'),
(4, 'Advertisement 4', '<div style=\"background:red;height: 221px;width:100%;\">Advertisement 4 Here</div>'),
(5, 'Advertisement 5', '<div style=\"background:red;height: 221px;width:100%;\">Advertisement 5 Here</div>'),
(6, 'Autem expedita elige', 'Fuga In nisi dolore');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Celebs', 1, '2016-05-17 22:31:38', NULL),
(2, 'Fails', 1, '2016-05-17 22:31:38', NULL),
(3, 'Funny', 1, '2016-05-17 22:31:38', NULL),
(4, 'Memes', 1, '2016-05-17 22:31:38', NULL),
(5, 'Music', 1, '2016-05-17 22:31:38', NULL),
(6, 'Animals', 1, '2016-05-17 22:31:38', NULL),
(7, 'Storytime', 1, '2016-05-17 22:31:38', NULL),
(8, 'Design Art & Tech', 1, '2016-05-17 22:31:38', NULL),
(9, 'Throwback', 1, '2016-05-17 22:31:38', NULL),
(10, 'News & Knowledge', 1, '2016-05-17 22:31:38', NULL),
(11, 'Interesting', 1, '2016-05-17 22:31:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `comment_text` text,
  `user_id` int(11) DEFAULT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `email_template_id` int(11) NOT NULL,
  `email_template_title` varchar(255) DEFAULT NULL,
  `email_template_subject` varchar(1000) DEFAULT NULL,
  `email_template_content` text,
  `allowed_tag_words` varchar(1000) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`email_template_id`, `email_template_title`, `email_template_subject`, `email_template_content`, `allowed_tag_words`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Account Creation', 'Your Account has been created', 'Dear User,\r\nYour account has been created on Blacktwitter using the email {email} and username {username}. Kindly visit the following link to activate your account.\r\n\r\n{activation_link}', '{username},{email},{activation_link}', NULL, '2015-07-29 14:39:18', '2016-02-17 10:38:10'),
(4, 'Account Deleted', 'Your Account has been deleted.', '<p>Your account has been deleted.</p>', '{first_name},{last_name},{email}', NULL, '2015-07-29 14:39:19', '2016-02-10 07:12:36');

-- --------------------------------------------------------

--
-- Table structure for table `embeded_video_user`
--

CREATE TABLE `embeded_video_user` (
  `embeded_video_user_id` int(11) NOT NULL,
  `embed_code` text,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `embeded_video_user`
--

INSERT INTO `embeded_video_user` (`embeded_video_user_id`, `embed_code`, `user_id`) VALUES
(1, '\n<iframe width=\"480\" height=\"270\" src=\"https://www.youtube.com/embed/haZMaCq1p78?feature=oembed\" frameborder=\"0\" allowfullscreen></iframe>\n', 1),
(2, '\n<iframe width=\"480\" height=\"270\" src=\"https://www.youtube.com/embed/haZMaCq1p78?feature=oembed\" frameborder=\"0\" allowfullscreen></iframe>\n', 1),
(3, '\n<iframe width=\"459\" height=\"344\" src=\"https://www.youtube.com/embed/ojsB8-CI2zA?feature=oembed\" frameborder=\"0\" allowfullscreen></iframe>\n', 10),
(4, '\n<iframe width=\"459\" height=\"344\" src=\"https://www.youtube.com/embed/04IZ_-KbvnM?feature=oembed\" frameborder=\"0\" allowfullscreen></iframe>\n', 10),
(8, '\n<iframe frameborder=\"0\" width=\"480\" height=\"269\" src=\"http://www.dailymotion.com/embed/video/x4p18zx\" allowfullscreen></iframe>\n', 25),
(9, '\n<iframe src=\"https://player.vimeo.com/video/30476100\" width=\"480\" height=\"270\" frameborder=\"0\" title=\"Hot Chicks of Occupy Wall Street\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n', 25),
(17, 'www.instagram.com/p/gVRwDwR7WN/', 13),
(18, '\n<p>https://www.instagram.com/p/BJjQ8N-DePT/</p>\n', 13),
(20, '', NULL),
(21, '\n<iframe width=\"480\" height=\"270\" src=\"https://www.youtube.com/embed/kxt7bDFJeds?feature=oembed\" frameborder=\"0\" allowfullscreen></iframe>\n', 13),
(22, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `favroute_pictures`
--

CREATE TABLE `favroute_pictures` (
  `favroute_picture_id` int(11) NOT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favroute_pictures`
--

INSERT INTO `favroute_pictures` (`favroute_picture_id`, `picture_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 10, 1, '2016-05-31 05:16:02', NULL),
(2, 3, 1, 1, '2016-08-01 12:38:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `gallery_id` int(11) NOT NULL,
  `gallery_title` varchar(255) DEFAULT NULL,
  `gallery_description` text,
  `gallery_meta_tags` text,
  `user_id` int(11) DEFAULT NULL,
  `share_with_community` tinyint(1) DEFAULT '0',
  `gallery_views` text,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`gallery_id`, `gallery_title`, `gallery_description`, `gallery_meta_tags`, `user_id`, `share_with_community`, `gallery_views`, `status`, `created_at`, `updated_at`) VALUES
(1, 'sss', 'ddd', 'koskd, sdsd, asda', 1, 0, NULL, 1, '2016-05-26 12:20:54', NULL),
(2, 'Test', 'Test Description', 'Test Tag', 1, 0, NULL, 1, '2016-05-26 03:24:32', NULL),
(3, '', '', '', 1, 0, NULL, 1, '2016-05-26 06:54:17', NULL),
(4, '', '', '', 1, 0, NULL, 1, '2016-05-26 06:54:55', NULL),
(5, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:49:39', NULL),
(6, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:49:47', NULL),
(7, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:49:55', NULL),
(8, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:50:14', NULL),
(9, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:50:40', NULL),
(10, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:50:53', NULL),
(11, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:57:26', NULL),
(12, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:57:34', NULL),
(13, '', '', '', 1, 0, NULL, 1, '2016-05-27 06:57:42', NULL),
(14, '', '', '', 1, 0, NULL, 1, '2016-05-27 07:01:10', NULL),
(15, '', '', '', 1, 0, NULL, 1, '2016-05-27 07:01:23', NULL),
(16, '', '', '', 1, 0, NULL, 1, '2016-05-27 07:01:48', NULL),
(17, '', '', '', 1, 0, NULL, 1, '2016-05-27 07:02:16', NULL),
(18, '', '', '', 1, 0, NULL, 1, '2016-05-29 03:54:24', NULL),
(19, '', '', '', 1, 0, NULL, 1, '2016-05-29 03:55:10', NULL),
(20, '', '', '', 1, 0, NULL, 1, '2016-05-29 03:55:55', NULL),
(21, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:01:08', NULL),
(22, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:01:33', NULL),
(23, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:03:00', NULL),
(24, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:03:31', NULL),
(25, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:03:54', NULL),
(26, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:04:07', NULL),
(27, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:05:41', NULL),
(28, '', '', '', 1, 0, NULL, 1, '2016-05-29 04:05:57', NULL),
(29, 'title 1', 'description 1', 'tags,tags,tags', 1, 0, NULL, 1, '2016-05-30 01:31:40', NULL),
(30, '', '', '', 1, 0, NULL, 1, '2016-06-08 04:45:27', NULL),
(31, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:10:57', NULL),
(32, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:11:50', NULL),
(33, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:14:31', NULL),
(34, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:14:51', NULL),
(35, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:15:06', NULL),
(36, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:15:14', NULL),
(37, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:15:25', NULL),
(38, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:15:37', NULL),
(39, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:16:17', NULL),
(40, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:16:58', NULL),
(41, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:17:32', NULL),
(42, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:17:56', NULL),
(43, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:18:16', NULL),
(44, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:18:41', NULL),
(45, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:19:16', NULL),
(46, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:19:24', NULL),
(47, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:19:32', NULL),
(48, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:19:40', NULL),
(49, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:19:48', NULL),
(50, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:20:34', NULL),
(51, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:21:13', NULL),
(52, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:22:11', NULL),
(53, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:23:04', NULL),
(54, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:23:31', NULL),
(55, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:23:42', NULL),
(56, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:23:54', NULL),
(57, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:24:04', NULL),
(58, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:24:32', NULL),
(59, '', '', '', 1, 0, NULL, 1, '2016-07-13 02:29:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_categories`
--

CREATE TABLE `gallery_categories` (
  `gallery_category_id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_categories`
--

INSERT INTO `gallery_categories` (`gallery_category_id`, `gallery_id`, `category_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2016-05-26 12:20:54', NULL),
(2, 1, 2, 1, '2016-05-26 12:20:54', NULL),
(3, 1, 3, 1, '2016-05-26 12:20:54', NULL),
(4, 2, 1, 1, '2016-05-26 03:24:32', NULL),
(5, 3, 0, 1, '2016-05-26 06:54:17', NULL),
(6, 4, 0, 1, '2016-05-26 06:54:55', NULL),
(7, 5, 0, 1, '2016-05-27 06:49:39', NULL),
(8, 6, 0, 1, '2016-05-27 06:49:47', NULL),
(9, 7, 0, 1, '2016-05-27 06:49:55', NULL),
(10, 8, 0, 1, '2016-05-27 06:50:14', NULL),
(11, 9, 0, 1, '2016-05-27 06:50:40', NULL),
(12, 10, 0, 1, '2016-05-27 06:50:53', NULL),
(13, 11, 0, 1, '2016-05-27 06:57:26', NULL),
(14, 12, 0, 1, '2016-05-27 06:57:34', NULL),
(15, 13, 0, 1, '2016-05-27 06:57:42', NULL),
(16, 14, 0, 1, '2016-05-27 07:01:10', NULL),
(17, 15, 0, 1, '2016-05-27 07:01:23', NULL),
(18, 16, 0, 1, '2016-05-27 07:01:48', NULL),
(19, 17, 0, 1, '2016-05-27 07:02:16', NULL),
(20, 18, 0, 1, '2016-05-29 03:54:24', NULL),
(21, 19, 0, 1, '2016-05-29 03:55:10', NULL),
(22, 20, 0, 1, '2016-05-29 03:55:55', NULL),
(23, 21, 0, 1, '2016-05-29 04:01:08', NULL),
(24, 22, 0, 1, '2016-05-29 04:01:33', NULL),
(25, 23, 0, 1, '2016-05-29 04:03:00', NULL),
(26, 24, 0, 1, '2016-05-29 04:03:31', NULL),
(27, 25, 0, 1, '2016-05-29 04:03:54', NULL),
(28, 26, 0, 1, '2016-05-29 04:04:07', NULL),
(29, 27, 0, 1, '2016-05-29 04:05:41', NULL),
(30, 28, 0, 1, '2016-05-29 04:05:57', NULL),
(31, 29, 1, 1, '2016-05-30 01:31:40', NULL),
(32, 29, 2, 1, '2016-05-30 01:31:40', NULL),
(33, 29, 3, 1, '2016-05-30 01:31:40', NULL),
(34, 30, 0, 1, '2016-06-08 04:45:27', NULL),
(35, 31, 0, 1, '2016-07-13 02:10:57', NULL),
(36, 32, 0, 1, '2016-07-13 02:11:50', NULL),
(37, 33, 0, 1, '2016-07-13 02:14:31', NULL),
(38, 34, 0, 1, '2016-07-13 02:14:51', NULL),
(39, 35, 0, 1, '2016-07-13 02:15:06', NULL),
(40, 36, 0, 1, '2016-07-13 02:15:14', NULL),
(41, 37, 0, 1, '2016-07-13 02:15:25', NULL),
(42, 38, 0, 1, '2016-07-13 02:15:37', NULL),
(43, 39, 0, 1, '2016-07-13 02:16:17', NULL),
(44, 40, 0, 1, '2016-07-13 02:16:58', NULL),
(45, 41, 0, 1, '2016-07-13 02:17:32', NULL),
(46, 42, 0, 1, '2016-07-13 02:17:56', NULL),
(47, 43, 0, 1, '2016-07-13 02:18:16', NULL),
(48, 44, 0, 1, '2016-07-13 02:18:41', NULL),
(49, 45, 0, 1, '2016-07-13 02:19:16', NULL),
(50, 46, 0, 1, '2016-07-13 02:19:24', NULL),
(51, 47, 0, 1, '2016-07-13 02:19:32', NULL),
(52, 48, 0, 1, '2016-07-13 02:19:40', NULL),
(53, 49, 0, 1, '2016-07-13 02:19:48', NULL),
(54, 50, 0, 1, '2016-07-13 02:20:34', NULL),
(55, 51, 0, 1, '2016-07-13 02:21:13', NULL),
(56, 52, 0, 1, '2016-07-13 02:22:11', NULL),
(57, 53, 0, 1, '2016-07-13 02:23:04', NULL),
(58, 54, 0, 1, '2016-07-13 02:23:31', NULL),
(59, 55, 0, 1, '2016-07-13 02:23:42', NULL),
(60, 56, 0, 1, '2016-07-13 02:23:54', NULL),
(61, 57, 0, 1, '2016-07-13 02:24:04', NULL),
(62, 58, 0, 1, '2016-07-13 02:24:32', NULL),
(63, 59, 0, 1, '2016-07-13 02:29:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_comments`
--

CREATE TABLE `gallery_comments` (
  `gallery_comment_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `comment_text` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_comments`
--

INSERT INTO `gallery_comments` (`gallery_comment_id`, `user_id`, `gallery_id`, `comment_text`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'my comment here', 1, '2016-05-26 12:21:10', NULL),
(2, 1, 1, 'sssddd ddaa', 1, '2016-05-26 12:21:21', NULL),
(3, 1, 3, 'Test', 1, '2016-05-28 05:10:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_votes`
--

CREATE TABLE `gallery_votes` (
  `gallery_votes_id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `ip_address` text,
  `is_up` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Common User', 'Common User');

-- --------------------------------------------------------

--
-- Table structure for table `group_action`
--

CREATE TABLE `group_action` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_group` int(11) DEFAULT NULL,
  `id_action` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_action`
--

INSERT INTO `group_action` (`id`, `id_group`, `id_action`) VALUES
(158, 2, 1),
(159, 2, 2),
(160, 2, 3),
(161, 2, 4),
(162, 2, 5),
(163, 2, 6),
(164, 2, 7),
(165, 2, 8),
(166, 2, 9),
(167, 2, 10),
(168, 2, 11),
(169, 2, 12),
(170, 2, 13),
(171, 2, 14),
(172, 2, 15),
(173, 2, 16),
(174, 2, 17),
(175, 2, 18),
(176, 2, 19),
(177, 2, 20),
(178, 2, 21),
(179, 2, 22),
(180, 2, 23),
(181, 2, 24),
(182, 2, 25),
(183, 2, 26),
(184, 2, 27),
(185, 2, 28),
(186, 2, 29),
(187, 2, 30),
(188, 2, 31),
(189, 2, 32),
(190, 2, 33),
(195, 2, 38),
(196, 2, 39),
(197, 2, 40),
(198, 2, 41),
(199, 2, 42),
(200, 2, 43),
(201, 2, 44),
(202, 2, 45),
(203, 2, 46),
(204, 2, 47),
(205, 2, 48),
(206, 2, 49),
(207, 2, 50),
(208, 2, 51),
(209, 2, 52),
(210, 2, 53),
(211, 2, 34),
(212, 2, 35),
(213, 2, 36),
(214, 2, 37);

-- --------------------------------------------------------

--
-- Table structure for table `image_categories`
--

CREATE TABLE `image_categories` (
  `picture_category_id` int(11) NOT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `to_id` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `message_text` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `picture_id` int(11) NOT NULL,
  `picture_title` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `views` text,
  `discription` text,
  `is_video` tinyint(1) DEFAULT NULL,
  `embed_code` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`picture_id`, `picture_title`, `file_name`, `gallery_id`, `user_id`, `views`, `discription`, `is_video`, `embed_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Picture Title', 'picture_1.png', 0, 1, '1', NULL, NULL, NULL, 1, '2016-05-26 12:19:43', NULL),
(2, 'Picture Title', 'picture_2.png', 1, 1, '14', NULL, NULL, NULL, 1, '2016-05-26 12:20:54', NULL),
(3, 'Picture Title', 'picture_3.png', 2, 1, '19', NULL, NULL, NULL, 1, '2016-05-26 03:24:32', NULL),
(4, 'Picture Title', 'picture_4.png', 3, 1, '7', NULL, NULL, NULL, 1, '2016-05-26 06:54:17', NULL),
(5, 'Picture Title', 'picture_5.png', 3, 1, '1', NULL, NULL, NULL, 1, '2016-05-26 06:54:18', NULL),
(6, 'Picture Title', 'picture_6.png', 3, 1, '5', NULL, NULL, NULL, 1, '2016-05-26 06:54:18', NULL),
(7, 'Picture Title', 'picture_7.png', 4, 1, NULL, NULL, NULL, NULL, 1, '2016-05-26 06:54:55', NULL),
(8, 'Picture Title', 'picture_8.png', 4, 1, '1', NULL, NULL, NULL, 1, '2016-05-26 06:54:55', NULL),
(9, 'Picture Title', 'picture_9.png', 4, 1, NULL, NULL, NULL, NULL, 1, '2016-05-26 06:54:56', NULL),
(10, 'Picture Title', 'picture_10.png', 4, 1, NULL, NULL, NULL, NULL, 1, '2016-05-26 06:54:56', NULL),
(11, 'Picture Title', 'picture_11.png', 4, 1, NULL, NULL, NULL, NULL, 1, '2016-05-26 06:54:57', NULL),
(12, 'Picture Title', 'picture_12.png', 4, 1, NULL, NULL, NULL, NULL, 1, '2016-05-26 06:54:58', NULL),
(13, 'Picture Title', 'picture_13.png', 5, 1, '1', NULL, NULL, NULL, 1, '2016-05-27 06:49:39', NULL),
(14, 'Picture Title', 'picture_14.png', 6, 1, '1', NULL, NULL, NULL, 1, '2016-05-27 06:49:47', NULL),
(15, 'Picture Title', 'picture_15.png', 7, 1, '1', NULL, NULL, NULL, 1, '2016-05-27 06:49:55', NULL),
(16, 'Picture Title', 'picture_16.png', 8, 1, '2', NULL, NULL, NULL, 1, '2016-05-27 06:50:14', NULL),
(17, 'Picture Title', 'picture_17.png', 9, 1, NULL, NULL, NULL, NULL, 1, '2016-05-27 06:50:40', NULL),
(18, 'Picture Title', 'picture_18.png', 10, 1, '3', NULL, NULL, NULL, 1, '2016-05-27 06:50:53', NULL),
(19, 'Picture Title', 'picture_19.png', 11, 1, '1', NULL, NULL, NULL, 1, '2016-05-27 06:57:26', NULL),
(20, 'Picture Title', 'picture_20.png', 12, 1, '4', NULL, NULL, NULL, 1, '2016-05-27 06:57:34', NULL),
(21, 'Picture Title', 'picture_21.png', 13, 1, '2', NULL, NULL, NULL, 1, '2016-05-27 06:57:42', NULL),
(22, 'Picture Title', 'picture_22.png', 14, 1, '2', NULL, NULL, NULL, 1, '2016-05-27 07:01:10', NULL),
(23, 'Picture Title', 'picture_23.png', 15, 1, '2', NULL, NULL, NULL, 1, '2016-05-27 07:01:23', NULL),
(24, 'Picture Title', 'picture_24.png', 16, 1, '2', NULL, NULL, NULL, 1, '2016-05-27 07:01:48', NULL),
(25, 'Picture Title', 'picture_25.png', 17, 1, '11', NULL, NULL, NULL, 1, '2016-05-27 07:02:16', NULL),
(26, 'Picture Title', 'picture_26.png', 18, 1, '5', NULL, NULL, NULL, 1, '2016-05-29 03:54:24', NULL),
(27, 'Picture Title', 'picture_27.png', 19, 1, '1', NULL, NULL, NULL, 1, '2016-05-29 03:55:10', NULL),
(28, 'Picture Title', 'picture_28.png', 20, 1, '1', NULL, NULL, NULL, 1, '2016-05-29 03:55:55', NULL),
(29, 'Picture Title', 'picture_29.png', 21, 1, '3', NULL, NULL, NULL, 1, '2016-05-29 04:01:08', NULL),
(30, 'Picture Title', 'picture_30.png', 22, 1, '1', NULL, NULL, NULL, 1, '2016-05-29 04:01:33', NULL),
(31, 'Picture Title', 'picture_31.png', 23, 1, '1', NULL, NULL, NULL, 1, '2016-05-29 04:03:00', NULL),
(32, 'Picture Title', 'picture_32.png', 24, 1, '3', NULL, NULL, NULL, 1, '2016-05-29 04:03:31', NULL),
(33, 'Picture Title', 'picture_33.png', 25, 1, '4', NULL, NULL, NULL, 1, '2016-05-29 04:03:54', NULL),
(34, 'Picture Title', 'picture_34.png', 26, 1, '6', NULL, NULL, NULL, 1, '2016-05-29 04:04:07', NULL),
(35, 'Picture Title', 'picture_35.png', 26, 1, '2', NULL, NULL, NULL, 1, '2016-05-29 04:04:09', NULL),
(36, 'Picture Title', 'picture_36.png', 26, 1, '1', NULL, NULL, NULL, 1, '2016-05-29 04:04:10', NULL),
(37, 'Picture Title', 'picture_37.png', 27, 1, '2', NULL, NULL, NULL, 1, '2016-05-29 04:05:41', NULL),
(38, 'Picture Title', 'picture_38.png', 28, 1, '3', NULL, NULL, NULL, 1, '2016-05-29 04:05:57', NULL),
(39, 'Picture Title', 'picture_39.png', 29, 1, '9', NULL, NULL, NULL, 1, '2016-05-30 01:31:40', NULL),
(40, 'Picture Title', 'picture_40.png', 30, 1, '5', NULL, NULL, NULL, 1, '2016-06-08 04:45:27', NULL),
(41, 'Picture Title', 'picture_41.png', 31, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:10:57', NULL),
(42, 'Picture Title', 'picture_42.png', 32, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:11:50', NULL),
(43, 'Picture Title', 'picture_43.png', 33, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:14:31', NULL),
(44, 'Picture Title', 'picture_44.png', 34, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:14:51', NULL),
(45, 'Picture Title', 'picture_45.png', 35, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:15:06', NULL),
(46, 'Picture Title', 'picture_46.png', 36, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:15:14', NULL),
(47, 'Picture Title', 'picture_47.png', 37, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:15:25', NULL),
(48, 'Picture Title', 'picture_48.png', 38, 1, '2', NULL, NULL, NULL, 1, '2016-07-13 02:15:37', NULL),
(49, 'Picture Title', 'picture_49.png', 39, 1, '2', NULL, NULL, NULL, 1, '2016-07-13 02:16:17', NULL),
(50, 'Picture Title', 'picture_50.png', 40, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:16:58', NULL),
(51, 'Picture Title', 'picture_51.png', 41, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:17:32', NULL),
(52, 'Picture Title', 'picture_52.png', 42, 1, '3', NULL, NULL, NULL, 1, '2016-07-13 02:17:56', NULL),
(53, 'Picture Title', 'picture_53.png', 43, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:18:16', NULL),
(54, 'Picture Title', 'picture_54.png', 44, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:18:41', NULL),
(55, 'Picture Title', 'picture_55.png', 45, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:19:16', NULL),
(56, 'Picture Title', 'picture_56.png', 46, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:19:24', NULL),
(57, 'Picture Title', 'picture_57.png', 47, 1, '2', NULL, NULL, NULL, 1, '2016-07-13 02:19:32', NULL),
(58, 'Picture Title', 'picture_58.png', 48, 1, '4', NULL, NULL, NULL, 1, '2016-07-13 02:19:40', NULL),
(59, 'Picture Title', 'picture_59.png', 49, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:19:48', NULL),
(60, 'Picture Title', 'picture_60.png', 50, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:20:34', NULL),
(61, 'Picture Title', 'picture_61.png', 51, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:21:13', NULL),
(62, 'Picture Title', 'picture_62.png', 52, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:22:11', NULL),
(63, 'Picture Title', 'picture_63.png', 53, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:23:04', NULL),
(64, 'Picture Title', 'picture_64.png', 54, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:23:31', NULL),
(65, 'Picture Title', 'picture_65.png', 55, 1, '1', NULL, NULL, NULL, 1, '2016-07-13 02:23:42', NULL),
(66, 'Picture Title', 'picture_66.png', 56, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:23:54', NULL),
(67, 'Picture Title', 'picture_67.png', 57, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:24:04', NULL),
(68, 'Picture Title', 'picture_68.png', 58, 1, '7', NULL, NULL, NULL, 1, '2016-07-13 02:24:32', NULL),
(69, 'Picture Title', 'picture_69.png', 59, 1, NULL, NULL, NULL, NULL, 1, '2016-07-13 02:29:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `picture_comments`
--

CREATE TABLE `picture_comments` (
  `picture_comment_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `comment_text` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `picture_votes`
--

CREATE TABLE `picture_votes` (
  `picture_votes_id` int(11) NOT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `ip_address` text,
  `is_up` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picture_votes`
--

INSERT INTO `picture_votes` (`picture_votes_id`, `picture_id`, `ip_address`, `is_up`, `status`, `created_at`, `updated_at`) VALUES
(12, 36, '180.178.180.115', 1, 1, '2016-07-31 01:47:30', NULL),
(13, 35, '180.178.180.115', 1, 1, '2016-07-31 01:47:34', NULL),
(14, 48, '::1', 1, 1, '2019-04-28 02:46:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL,
  `setting_name` varchar(255) DEFAULT NULL,
  `setting_alias` varchar(255) DEFAULT NULL,
  `setting_value` text,
  `parent_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `setting_name`, `setting_alias`, `setting_value`, `parent_id`) VALUES
(1, 'general_settings', 'General Settings', NULL, 0),
(21, 'text_logo', 'Site Name (Also used as text logo)', '#Blacktwitter', 1),
(22, 'is_picture_logo', 'Use Picture as Logo', '', 1),
(23, 'picture_logo_url', 'Picture Logo', 'http://shehrozasmat.com/blacktwitter/public/site_files/logo.', 1),
(24, 'homepage_settings', 'Home Page Settings', NULL, 0),
(25, 'items_to_show', 'Items to Show', '24', 24),
(28, 'site_email\r\n', 'Site Email', 'admin@blacktwitter.com', 1),
(30, 'social_settings', 'Social Settings', NULL, 0),
(31, 'facebook_link', 'Facebook Link', 'https://www.facebook.com/smile.alwayzzzzz/', 30),
(33, 'flickr_link', 'Flickr Link', '', 30),
(34, 'linkedin_link', 'Linked In Link', '', 30),
(35, 'twitter_link', 'Twitter Link', '', 30),
(36, 'tumblr_link', 'Tumblr Link', '', 30),
(37, 'advertisement', 'Advertisement', NULL, 0),
(38, 'home_page_advertisement', 'Home Page Advertisement', '<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIHBhUTBxITFhIXFhcVFRAXGBIXFhUZGBIZFxUbFRgYHSgsGBslHxoWITEtJikrLi4uGR8/ODMsNzQtLjcBCgoKDg0OGxAQGy0mICYtLi0rLy0uLTUvLy0uLTctLS0tLS0tLS0tNS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMgA8AMBEQACEQEDEQH/xAAaAAEAAwEBAQAAAAAAAAAAAAAABQYHBAID/8QANxAAAgEDAwIEBQMCBQUBAAAAAAECAwQRBQYSITETIkFRB2FxgZEVMqEjsUJSYpLBFzNDcuEU/8QAGgEBAQEBAQEBAAAAAAAAAAAAAAQDBQIBBv/EADARAQACAQMEAAMGBwEBAAAAAAABAgMEERITITFBBRQyIlFhgaHBI0JxkbHR8fAV/9oADAMBAAIRAxEAPwCtndflAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+wEzebXu7LTvHuaWKeE+WVlKXbK+5lXNS1uMS3vpcta85jshjVgASc9BuIaQrmUP6Dx58r1eF0+pn1a8uHttOC/DqbdkYaMUpou37nW5P9OpuSXRzfSKftyfr9DO+WtPqlriwZMv0w7NU2Ze6XbudxSzBdZSg1Livdpeh4pqMdp2iWmTR5ccbzG/8ARGaTpVXWLnw9PjynjljKXRfU0vetI3sxxYrZJ2o+F3bSs7qVO4WJxbjJezXc9RMTG8PNqzWZrPl8T6+AAAAAAAAAAAAAAAAAAAAAAEvtTS/1nX6VKSzFy5T/APWPWX57fczzX4UmW2nxdTLFfTYKt9S1m/ubGp/hpRy/fmmpdP8AT5PycuKzStbu7N65LWxfh/lhlzQlaXEoV1iUG4yXzi8M68TvG787as1mYn0vK2Nb6bpkZ7lunSnP9sY8cReOzym5Y9cYI/mbWttjq6MaLHSu+W20ymdx2P6b8MvCU4zUeGKke0k6qaa+zM8VuWfdvnpw0nHffx/llJ0XGa9tTlfbAVPQakadZJqUv8s+WZZ9sr1OZm+zm3vG8O3p976aIxztP7oy01HVNrU6j1qjO5pY6S5pqHu3JRb4475RpNMOWY4ztLKuTUYN+pHKFd2NaS1Pcc1YVZ2+YzknDEmo8liGWllLK649DfUWiuOOUbpdJWb5Z4zs9UtGt7rXbqGt3rpunUSjUko5qtuXJv6Yj+R1LRSs1qRhpbJeMl9tp/um7P4eWt7aupa3spU1nNRRjx6d+rfoYzq71naa91Ffh+O0cq37K9tPaj19TqVqnh29N4lVwm3hZfHL6YWG2/c3zZ+ntER3lLptL1t5mdohJ3Wyre906dXa9y6zp/upy4tvpno0lh47ZXUyjUWrO2SNm9tHS1Zthtvso6LHOAAAAAAAAAAAAAAAAAABpPwpso2tnXvLrpHHCMn6Rj5qj/svsyDV23mKQ6vw6nGtsku7TdR0m3153FtXn41RtNtz4vm8dU127fQ82pmmnGY7NKZNNGTnFu8oH4jWEdN3bSrzX9Oq4zl9acoqf8Yf3NdLbljmPuTa6kUzxafE/t5dnxgoTlcUKiTdPjKPLulJyT+2Vj64POimNpj20+J1netvTs1i3na/CiELlNSUaeYvus1E0meaTE6jeGmWs10cRP4MuOg4y722169joELzbtxUnUkot06ceLx15LCk+bT9GvfoSTmra/C8OjGmvTHGXFaZn8Fk2FrGoahdShq9OXhKP/dnB05cs9F1S5ev0MNRjx1jevlVo8ue8zGSO337I3aNKnb/ABJuY2WPDSnhLsuseSX0eUe80zOCsyy00VjVWivhFw21Pce97lPMaMasnUqfJv8AbH/U/wCDTrRjxRPtj8tObUWj1v3du/Nak6KstEpzVCC4zlGMsSx2hHC7L192edPjj67eWmszTt0scdo8/wCklsqcZ/D6rHwnVcZVFUoJ8XJ55cfrjBnnietHfbw20sxOmmNt/O8OfautqMak9u6VLHljUcayWcZaXmj1a6/TJ6zYvEXv+jxp8/mceP8AVms5KU24LCbbS9k30Re5LyAAAAAAAAAAAAAAAAAAJuO6LiGg/wD5IcFR48XiPmaby8v3Zl0a8+c+VHzN4x9P0hGsrqap03re57jXLOFPUODUHlSUcSzxw8v5mWPDXHO9W+XU3y1itvTu0vfl5ptgqVNwkorEZTTcor0Wc9fueL6alp3a49dlpXjDkv8Ad11qOlOheSjKDeXJrzN8uXf6nquClbcoZ31WS9OFvCBNk6b0DdVzoMHGyknBvPhyWY5917fYxyYKZO8qMOpyYu1fCUv/AIi3t3Rcafh08rHKCfL7Nt4M66THH4tb/EMto2jsgdD1qrod66tjx5tOL5LPRvqb5McXjaU+HNbFblXyktO3pdadVqyt/DzVn4k8xz5sJdOvRdDO2npbbf01prMtJmY2793b/wBSL73pf7P/AKePlMf4vf8A9DN+H9kPpm5bjS9SqVrOSUqsnKpFrMZNycuq+rePqa3w0vWKz6Y49TkpebV9+Ulq+/bzVLR05OFOMliXBNNp91lvp9jxTS46zu1y67LeOPhVShGAAAAAAAAAAAAAAAAADIABkAAyAAAAAABnHcAAyHzcD6ZAAAAAAAAAAAAAAAAAPraxhO6irqTjTckpyXVqOfM0vfB8nfbs+123jfw1DT9w2U9ao2ejWtOdGaxKphZXRvrFx64x1bfqc+2LJwm9p7uxTPi6kYsdYmPvVbdO3YUd5xttN8sarh07+Hyfmx8kssow5ZnFyt6R6jTxGfhT2t+pala7a1WhZUbWm4T4qc3xyuUuKbyny93lktKXy1m8ytyZMeC9cUV7e1V3ptmNnuqnS09KMK7jxXpBufGX2XcpwZt8czPpHq9NFc0Vr/MtOq6ja7Tv6FpRtacoTUfEnLGUnLim8p8n3byT0pfLE33WZMmPT2rjisKj8R9Dho2sxdmuNOrFyUV2jJPEkvl1T+5Tpck3r39ItdhjHeJr4lWLW3ld3EYUFmcnxispZb7LLKJmIjeUdazadodt/oNzp11CneUpRnU/ZHo+XXHTD92vyea5a2iZiWl8GSkxW0d58POraLX0ZxWpU+Dlnim4tvGM9n80KZK3+l8yYb4/rhyWtNVrmMaslGLlFSm+0U3hy+y6nqZ2js8ViJmIlpd7uSx0GpRo6NQo1qcsc5rDaXLj/lfKXd9f+SCuHJk3tadpde+oxYuNaREwhfinotLSr6lOxioqrGfKEVhJwceqXpnl/BrpMk2iYn0n+IYa0tE197rVql3Y7UtKFPUbaE5un1cadKT8iipOTl7tv8MnpXJlmZiVmTJiwVrFq+vuUfcd7Q3NrVCOh0/CUsU35IR6yn+7EX1wivFW2Kszed3Pz5KZ8lYxxt6XLVdRtdp31C0o2tOUJpeJN4yk5cU3lPk+7eSWlL5Ym8yuyZcentXHFe3tUfiPocNG1mLs48adWLkortGSeJJfLqn9ynS5ZvTv6Ra7BXFeOPiVTKUQAAAAAAAAAAAAAD1CDnLEE232S6v8HyexHfwtezdfnti88O5t3/VnDlKSlGcYvyrimu3XPzJ8+KMsbxPhbpc84LcZr5T2o6NHR/iNa1IzlKNaUped8mpKLi+r7rzRwY1yTfBaPuU3xRi1VbfejPiLBvfNPC7qil/vwaaaf4M/mx10T8xH5LJvKrGO9tO5ekp5+7io/wAmGGJ6V1epmPmMat/FKm5bsp4XeEMfPztG+kn+HKT4jE9aHf8AGSSda1XqlWf5dLH9meNF/N+TX4nP0/mzj6FzlNw2lXeu6HQr6nT/AKtNvhN95dOPNfVHJzRwvNaz2foNNacuOt7x3j/27JNz6lV1TW6k7+LjJNx8N/8AjUXjj9v5ydLFStKRFXF1GS18kzf/AIijRiv2ydtwsrdX+4GoUo4nShL1fpOX/C9fwR5802np0/N0tJpq1jrZfHr/AGhNw7jev7ihVdNulCUVCj/ilFT5NPGesv46GuPF06THtPm1HWyxbbtHiFl1H4mVbe8Sha8IpJzjU5Ko136dFjp26Mwro4mPKu/xG0T2r/fy79y6XSst4WNxbxUfEquM0lhN4XF49+rz9jPFe1sdqz6aZ8Va5qXj3P8AxAfFKDe66ePWnDHz87RvpJ/hym+IRPWh3/GWSda1S7pVn+XSx/ZnjReLNfic/TH9f2ZwXOUAAAAAAAAAAAAAA6NPvJ6ffQq2374SUl7PHo/k+33PNqxaJiXql5paLR6aJV3Xpeq16dxqlKorin2jiUllPK6x6SSfbJFGDNSJrWe0upOq015i947wqm5901Na1uFegnBUmvCi+rWJcsy+baRRiwxSk1n35RajUzkyReO23haZbt03Va1K41elUjc0l5YpSksp5WGujSfVZxgn6GWu9az2lZ81p8kxfJHeFR3NuKeta4rimnDhx8Jd3FRlyi3889SnFhilOKLPqLZcnOPXhbnu3TdXlSr63TnG4pdYpKUk2mmsY7rKys9iboZab1pPaV3zWnybXyR3hUN36+9xat4nFxglxhB4ylnOXj1bKcOKMddkOpz9a/L16RunKk72P6i5KlnM+KzJr2X17Glt9vs+WNOPKOXhdda+IGL+h+hxlGhS/dTa4+J048ceiS7fNklNL9mefmXQy677Ven4hEb51W01u7jW0vnGo/LUjKOFJJeWSee67fj2NtPjvSONvyYavLjyzFqefaA090438HfZdJSTml1binlr79jW2+08fKanHlHLw0+833pt9RUbylUnFPKjKmmk8YWFkgrpste8T+rr212ntG0x+ipbn1y1qXlvU2zT8KVJzlJ8FHLzBw7PqukvyUYsd4iYyT5RajNjma2xRtsna25dK1qrCvrVGca8UsxxKSljqk+PSS+pjGHNT7NJ7KZ1Gmy7XyR3hXd3brlr2q06lsnCFJ5pJ4cs8k3J/PMV0+RRhwRjrMT78pNTqpy3i0dojwsr3bpurzpV9bpTjcUv2pKUot5z0a7rKys9ifoZab1pPaVfzenybWyR3hUN3a+9xar4nHjBLjCL6tLOcv5tlWHF067ItTnnNfl69IQ1TgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/2Q==\">', 37);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `identifier` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `picture`, `identifier`) VALUES
(1, '127.0.0.1', 'administrator', 'ydmcKVvPpN10pt1WRALSnO4f548a2d633b59275c', '', 'admin@admin.com', '', NULL, NULL, '.zt/MQXccBMJ3LiCLc2zMO', 1268889823, 1556463652, 1, 'Black Twitter', 'Admin', 'user_1.jpg', NULL),
(10, '85.115.52.201', 'qwertyuser09', 'uv0ojPLFK0jwiQCVCyLF1.ee3c55c44f1898b3cd', NULL, 'qwertyuser09@qwertyuser09.com', NULL, NULL, NULL, NULL, 1464687270, 1469010578, 1, 'qwertyuser09', '.', NULL, '103363962925604717152'),
(12, '103.255.6.70', 'shehroz asmat', 'UIu7d6jbT2tSYNpnMEhL3O842182ec76b73816fc', NULL, 'Shehroz Asmat@Shehroz Asmat.com', NULL, NULL, NULL, NULL, 1470966991, 1470966991, 1, 'Shehroz', 'Asmat', NULL, '100961661143075307893'),
(13, '180.178.182.92', 'adeel_safdar', 'AT9tML/UGsY7BJ18yqyLne467848e7df79e1aa32', NULL, 'mughals@live.com', NULL, NULL, NULL, NULL, 1471368890, 1475222493, 1, 'Adeel', 'Safdar', NULL, '1352059512'),
(14, '180.178.182.92', 'adeel_safdar1', 'GSP53xsxfYERUhOIQzxUM.28b7b6465c84fa1f81', NULL, 'adeelsafdar007@gmail.com', NULL, 't4nL9mO7.yxlZTsWANT-h.6e420a4529fbdf54f6', 1473261281, NULL, 1471368924, 1471368924, 1, 'adeel', 'safdar', NULL, '106392365398031026532'),
(15, '85.115.52.202', 'simon_binitie', 'xERffW1nFwoKOkNPvKBRq.f8c1ddbd733f416772', NULL, 'sirsimon@gmail.com', NULL, NULL, NULL, NULL, 1471422412, 1471422412, 1, 'Simon', 'Binitie', NULL, '10153850151832717'),
(25, '180.178.180.172', 'adeelsafdar009', 'IFUgi9IlSCG/WTYy0us.Qu3273b9cca71af54928', NULL, 'adeelsafdar@mail.com', '', NULL, NULL, NULL, 1471801052, 1471801158, 1, '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(9, 10, 2),
(11, 12, 2),
(12, 13, 2),
(13, 14, 2),
(14, 15, 2),
(24, 25, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_action`
--
ALTER TABLE `acl_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`advertisement_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `embeded_video_user`
--
ALTER TABLE `embeded_video_user`
  ADD PRIMARY KEY (`embeded_video_user_id`);

--
-- Indexes for table `favroute_pictures`
--
ALTER TABLE `favroute_pictures`
  ADD PRIMARY KEY (`favroute_picture_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
  ADD PRIMARY KEY (`gallery_category_id`);

--
-- Indexes for table `gallery_comments`
--
ALTER TABLE `gallery_comments`
  ADD PRIMARY KEY (`gallery_comment_id`);

--
-- Indexes for table `gallery_votes`
--
ALTER TABLE `gallery_votes`
  ADD PRIMARY KEY (`gallery_votes_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_action`
--
ALTER TABLE `group_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_categories`
--
ALTER TABLE `image_categories`
  ADD PRIMARY KEY (`picture_category_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`picture_id`);

--
-- Indexes for table `picture_comments`
--
ALTER TABLE `picture_comments`
  ADD PRIMARY KEY (`picture_comment_id`);

--
-- Indexes for table `picture_votes`
--
ALTER TABLE `picture_votes`
  ADD PRIMARY KEY (`picture_votes_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_action`
--
ALTER TABLE `acl_action`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `advertisement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `embeded_video_user`
--
ALTER TABLE `embeded_video_user`
  MODIFY `embeded_video_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `favroute_pictures`
--
ALTER TABLE `favroute_pictures`
  MODIFY `favroute_picture_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
  MODIFY `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `gallery_comments`
--
ALTER TABLE `gallery_comments`
  MODIFY `gallery_comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery_votes`
--
ALTER TABLE `gallery_votes`
  MODIFY `gallery_votes_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `group_action`
--
ALTER TABLE `group_action`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `image_categories`
--
ALTER TABLE `image_categories`
  MODIFY `picture_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `picture_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `picture_comments`
--
ALTER TABLE `picture_comments`
  MODIFY `picture_comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `picture_votes`
--
ALTER TABLE `picture_votes`
  MODIFY `picture_votes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
