<?php defined('BASEPATH') OR exit('No direct script access allowed');

class images extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','categories_model'));
	}

	public function get_all_images_ajax()
	{
		$loaded_result = $_POST['loaded_result'];
		$items_per_load = $_POST['items_per_load'];

		$images_data = $this->images_model->get_all_images($loaded_result,$items_per_load);

		echo json_encode($images_data);

	}

	public function get_all_images_by_category_ajax()
	{
		$loaded_result = $_POST['loaded_result'];
		$items_per_load = $_POST['items_per_load'];
		$category_id = $_POST['category_id'];

		$images_data = $this->images_model->get_all_images_by_category($loaded_result,$items_per_load,$category_id);

		echo json_encode($images_data);

	}

	public function my_images()
	{
		$data['all_my_images'] = $this->images_model->get_all_my_images();
		$this->load->view('my_images',$data);
	}

	public function add_view_to_picture()
	{
		$picture_id = $_POST['picture_id'];
		$user = $this->ion_auth->user()->row();

		$total_views = $this->images_model->get_picture_views($picture_id);
		$total_views = $total_views->views;
		
		if($total_views != ''){

			$total_views++;

			$data = array(
				'views' => $total_views
				);

			$this->db->where('picture_id', $picture_id);
			$this->db->update('pictures', $data);
		}else{
			$data = array(
				'views' => 1
				);

			$this->db->where('picture_id', $picture_id);
			$this->db->update('pictures', $data);
		}

	}

	public function edit_image_details($picture_id = '')
	{

		$user = $this->ion_auth->user()->row();
		$data['image_info'] = $this->images_model->get_image_info($picture_id);
		$data['all_categories'] = $this->categories_model->get_all_categories();

		if($user && $user->id == $data['image_info']->user_id){
			$this->load->view('edit_image_details',$data);
		}else{
			redirect(base_url());
		}
	}

	public function save_image_detail()
	{
		// var_dump($_POST);
		// array(4) { ["picture_id"]=> string(2) "17" ["picture_title"]=> string(13) "Picture Title" ["discription"]=> string(0) "" ["image_categories"]=> array(2) { [0]=> string(1) "1" [1]=> string(1) "2" } } 
		$datestring = "Y-m-d h:i:s";
			$time       = time();

		$picture_id = $this->input->post('picture_id');
		$picture_title = $this->input->post('picture_title');
		$discription = $this->input->post('discription');

		$data = array(
			'picture_title' => $picture_title,
			'discription' => $discription
			);

		$this->db->where('picture_id', $picture_id);
		$this->db->update('pictures', $data); 

		$image_categories = $this->input->post('image_categories');

		if($image_categories != ''){

			$this->db->delete('image_categories', array('picture_id' => $picture_id)); 

			$count = 0;
			$to_insert_image_categories = array();
			foreach ($image_categories as $image_category) {

				$to_insert_image_categories[$count]['picture_id'] = $picture_id;
				$to_insert_image_categories[$count]['category_id'] = $image_category;
				$to_insert_image_categories[$count]['status'] = 1;
				$to_insert_image_categories[$count]['created_at'] = date($datestring, $time);

				$count++;
			}

			$this->db->insert_batch('image_categories', $to_insert_image_categories); 

		}

		redirect($_SERVER['HTTP_REFERER']);

	}

	public function toggle_favroute()
	{

		$user = $this->ion_auth->user()->row();

		if($user){
			$datestring = "Y-m-d h:i:s";
			$time       = time();

			$picture_id = $this->input->post('picture_id');
			$is_favrouted_id = $this->images_model->check_if_favroute_before($picture_id);

			if(!$is_favrouted_id){
				$data = array(
					'picture_id' => $picture_id ,
					'user_id' => $user->id ,
					'status' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('favroute_pictures', $data); 

				echo 'favrouted';

			}else{

				$this->db->where('favroute_picture_id', $is_favrouted_id);
				$this->db->delete('favroute_pictures'); 

				echo 'unfavrouted';

			}
		}else{
			echo 'please_login';
		}

		

	}

}