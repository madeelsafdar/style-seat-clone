<?php defined('BASEPATH') OR exit('No direct script access allowed');

class users extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model'));
	}

	public function register(){
		$this->load->view('register');
	}

	public function signin(){
		$this->load->view('signin');
	}

	public function logout(){
		$logout = $this->ion_auth->logout();
		redirect(base_url());
	}

	public function save(){

		$this->form_validation->set_error_delimiters('<label class="error" generated="true">', '</label>');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[conf_password]');
		$this->form_validation->set_rules('conf_password', 'Password Confirmation', 'trim|required');

		if ($this->form_validation->run() == FALSE) {

			$validation_errors = array();
			$values            = array();

			$validation_errors['username'] = form_error('username');
			$validation_errors['email'] = form_error('email');
			$validation_errors['password'] = form_error('password');

			$values['username'] = set_value('username');
			$values['email'] = set_value('email');
			$values['password'] = set_value('password');

			@session_start();

			$_SESSION['values'] = $values;
			$_SESSION['errors'] = $validation_errors;

			if(isset($_POST['ajax_register'])){
				$to_return['redirect_to'] = base_url() . 'users/register';
				echo json_encode($to_return);
			}else{
				redirect(base_url() . 'users/register');
			}

		}else{

			$username = strtolower($this->input->post('username'));
			$email    = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => '',
				'last_name'  => '',
				'company'    => '',
				'phone'      => '',
				);

			$group = array(2);

			$user_saved = $this->ion_auth->register($username, $password, $email, $additional_data, $group);


			$unique_str = md5(uniqid(rand(), true));

			$data = array(
				'activation_code' => $unique_str
				);

			$this->db->where('id', $user_saved);
			$this->db->update('users', $data); 

			//send mail to user

			// {first_name},{last_name},{email}

			$sendto = $email;

			$email_template_id='1';
			$parse_data = array(
				'username' => $this->input->post('username'),
				'email' => $this->input->post('contact_person_last_name'),
				'activation_link' => '<a href="'.base_url('users/activate').'/'.$user_saved.'/'.$unique_str.'">Activate Account</a>'
				);
			$this->send_email($sendto, $email_template_id, $parse_data);


			if(isset($_POST['ajax_register'])){

				$to_return = '';

				if ($user_saved) {
					$this->session->set_flashdata('success_message', 'Your account has been created successfully. Plese check your inbox to verify it.');
					$to_return['redirect_to'] = base_url() . 'users/register';
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error creating your account. Please try again later.');
					$to_return['redirect_to'] = base_url() . 'users/register';
				}

				echo json_encode($to_return);

			}else{	
				if ($user_saved) {
					$this->session->set_flashdata('success_message', 'Your account has been created successfully. Plese check your inbox to verify it.');
					redirect(base_url() . 'users/register');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error creating your account. Please try again later.');
					redirect(base_url() . 'users/register');
				}
			}

		}

	}

	public function my_profile()
	{
		$this->load->view('my_profile');
	}

	public function test()
	{	
		$this->load->library('AutoEmbed');
		$data['embed_code'] = $this->autoembed->parse('https://vimeo.com/ondemand/wizardmode/164479194');
		$this->load->view('video_embed',$data);
	}

	public function save_profile()
	{
		$this->form_validation->set_error_delimiters('<label class="error" generated="true">', '</label>');

		$user = $this->ion_auth->user()->row();

		if($user->identifier != ''){
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['first_name'] = form_error('first_name');
				$validation_errors['last_name'] = form_error('last_name');

				$values['first_name'] = set_value('first_name');
				$values['last_name'] = set_value('last_name');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'users/my_profile');

			}else{

				$first_name = $_POST['first_name'];
				$last_name = $_POST['last_name'];

				$data = array(
					'first_name' => $first_name,
					'last_name' => $last_name
					);

				$this->db->where('id', $user->id);
				$this->db->update('users', $data); 

			}
		}else{
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]');
			$this->form_validation->set_rules('password', 'Password', 'trim|matches[conf_password]');
			$this->form_validation->set_rules('conf_password', 'Password Confirmation', 'trim');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['first_name'] = form_error('first_name');
				$validation_errors['last_name'] = form_error('last_name');
				$validation_errors['email'] = form_error('email');

				if(isset($_POST['password']) && $_POST['password'] != ''){
					$validation_errors['password'] = form_error('password');
				}

				$values['first_name'] = set_value('first_name');
				$values['last_name'] = set_value('last_name');
				$values['email'] = set_value('email');

				if(isset($_POST['password']) && $_POST['password'] != ''){
					$values['password'] = set_value('password');
				}

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'users/my_profile');

			}else{

				$first_name = $_POST['first_name'];
				$last_name = $_POST['last_name'];
				$email = $_POST['email'];

				$data = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'email' => $email
					);

				$this->db->where('id', $user->id);
				$this->db->update('users', $data); 

				if(isset($_POST['password']) && $_POST['password'] != ''){
					$data = array(
						'password' => $_POST['password'],
						);
					$this->ion_auth->update($user->id, $data);
				}

			}
		}

		redirect(base_url() . 'users/my_profile');
		
		
	}

	public function activate($user_id = '',$activation_code = '')
	{

		$is_present = $this->users_model->check_activation_code($user_id,$activation_code);

		if($is_present){
			$data = array(
				'activation_code' => '',
				'active' => 1
				);

			$this->db->where('id', $user_id);
			$this->db->update('users', $data); 

			$this->session->set_flashdata('success_message', 'Your account has been activated. Please Login Now.');
			redirect(base_url() . 'users/signin');
		}else{
			$this->session->set_flashdata('failure_message', 'Request Not Found.');
			redirect(base_url() . 'users/signin');
		}

	}

	public function signin_process(){


		$this->form_validation->set_error_delimiters('<label class="error" generated="true">', '</label>');

		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			$remember = (bool) $this->input->post('remember_me');

			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
			{
				// $this->session->set_flashdata('success_message', $this->ion_auth->messages());
				// redirect(base_url());

				if(isset($_POST['ajax_login'])){
					$to_return['redirect_to'] = base_url();
				}else{
					redirect(base_url());
				}

				echo json_encode($to_return);

			}
			else
			{
				$this->session->set_flashdata('failure_message', $this->ion_auth->errors());
				if(isset($_POST['ajax_login'])){
					$to_return['redirect_to'] = base_url() . 'users/signin';
				}else{
					redirect(base_url() . 'users/signin');
				}

				echo json_encode($to_return);

			}
		}
		else
		{
			// $this->session->set_flashdata('failure_message', (validation_errors()) ? validation_errors() : $this->session->flashdata('message'));

			// if(isset($_POST['ajax_login'])){
			// 	$to_return['redirect_to'] = base_url() . 'users/signin';
			// }else{
			// 	redirect(base_url() . 'users/signin');
			// }

			// echo json_encode($to_return);

			// *****************************************************

			$validation_errors = array();
			$values            = array();

			$validation_errors['email'] = form_error('email');
			$validation_errors['password'] = form_error('password');

			$values['email'] = set_value('email');
			$values['password'] = set_value('password');

			@session_start();

			$_SESSION['values'] = $values;
			$_SESSION['errors'] = $validation_errors;

			if(isset($_POST['ajax_register'])){
				$to_return['redirect_to'] = base_url() . 'users/signin';
			}else{
				redirect(base_url() . 'users/signin');
			}

			echo json_encode($to_return);

		}
	}

}