<?php defined('BASEPATH') OR exit('No direct script access allowed');

class random extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','comments_model'));
	}

	public function index()
	{
		$data['image_info'] = $this->images_model->get_random_image();
		$data['all_picture_comments'] = $this->comments_model->get_comment_of_picture($data['image_info']->picture_id);
		$this->load->view('random',$data);
	}

}