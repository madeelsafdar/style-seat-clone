<?php defined('BASEPATH') OR exit('No direct script access allowed');

class categories extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model'));
	}

	public function show_posts($category_id = '')
	{

		$data['category_id'] = $category_id;
		$this->load->view('show_posts',$data);
	}

}