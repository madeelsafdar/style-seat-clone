<?php defined('BASEPATH') OR exit('No direct script access allowed');

class comments extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model'));
	}

	public function save()
	{
		$user = $this->ion_auth->user()->row();
		$datestring = "Y-m-d h:i:s";
		$time       = time();

		$data = array(
			'comment_text' => $_POST['comment_text'] ,
			'picture_id' => $_POST['picture_id'] ,
			'user_id' => $user->id,
			'status' => 1,
			'created_at' => date($datestring, $time)
			);

		$this->db->insert('comments', $data); 

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function gallery_save()
	{
		$user = $this->ion_auth->user()->row();
		$datestring = "Y-m-d h:i:s";
		$time       = time();

		$data = array(
			'comment_text' => $_POST['comment_text'] ,
			'gallery_id' => $_POST['gallery_id'] ,
			'user_id' => $user->id,
			'status' => 1,
			'created_at' => date($datestring, $time)
			);

		$this->db->insert('gallery_comments', $data); 

		redirect($_SERVER['HTTP_REFERER']);
	}

}