<?php defined('BASEPATH') OR exit('No direct script access allowed');

class galleries extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','comments_model','categories_model'));
	}

	public function index()
	{
		$all_galleries = $this->galleries_model->get_all_galleries();
		$this->load->view('admin/galleries/manage');
	}

}