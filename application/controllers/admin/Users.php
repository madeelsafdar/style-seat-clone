<?php defined('BASEPATH') OR exit('No direct script access allowed');

class users extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model'));
	}

	function index(){
		//get all users
		$data['all_users'] = $this->users_model->get_all_users_except_admin();
		$this->load->view('admin/users/users',$data);
	}

	public function add_user()
	{
		$data['add'] = 1;
		$data['all_group'] = $this->groups_model->get_all_groups();
		$this->load->view('admin/users/add_edit_users',$data);
	}

	public function edit_user($user_id = '')
	{
		$data['edit'] = 1;
		$data['all_group'] = $this->groups_model->get_all_groups();
		$data['user_info'] = $this->users_model->get_user_info_by_id($user_id);
		$this->load->view('admin/users/add_edit_users',$data);
	}

	public function ajax_delete_user()
	{
		$user_id = $this->input->post('user_id');
		$user_delete = $this->ion_auth->delete_user($user_id);

		if(!$user_delete)
		{
		}
		else
		{
			echo 'deleted';
		}
		
	}

	public function save_user()
	{
		// var_dump($_POST);
		// die;
		if($this->input->post('add')){

			$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			// $this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|required|matches[password]');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['first_name'] = form_error('first_name');
				$validation_errors['last_name'] = form_error('last_name');
				// $validation_errors['username'] = form_error('username');
				$validation_errors['email'] = form_error('email');
				$validation_errors['phone'] = form_error('phone');
				$validation_errors['password'] = form_error('password');
				$validation_errors['conf_password'] = form_error('conf_password');

				$values['first_name'] = set_value('first_name');
				$values['last_name'] = set_value('last_name');
				// $values['username'] = set_value('username');
				$values['email'] = set_value('email');
				$values['phone'] = set_value('phone');
				$values['password'] = set_value('password');
				$values['conf_password'] = set_value('conf_password');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/users/add_user');

			}else{

				// $username = strtolower($this->input->post('username'));
				$username = strtolower($this->input->post('first_name').'_'.$this->input->post('last_name'));
				$email    = $this->input->post('email');
				$password = $this->input->post('password');

				$additional_data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => '',
					'phone'      => $this->input->post('phone'),
					);

				$group = array($this->input->post('group_id'));

				$user_saved = $this->ion_auth->register($username, $password, $email, $additional_data, $group);

				if ($user_saved) {
					$this->session->set_flashdata('success_message', 'User added successfully.');
					redirect(base_url() . 'admin/users');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error creating this User.');
					redirect(base_url() . 'admin/users');
				}

			}

		}
		if($this->input->post('edit')){

			$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			// $this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required');

			if($this->input->post('password')){
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|required|matches[password]');
			}

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['first_name'] = form_error('first_name');
				$validation_errors['last_name'] = form_error('last_name');
				// $validation_errors['username'] = form_error('username');
				$validation_errors['email'] = form_error('email');
				$validation_errors['phone'] = form_error('phone');
				$validation_errors['password'] = form_error('password');
				$validation_errors['conf_password'] = form_error('conf_password');

				$values['first_name'] = set_value('first_name');
				$values['last_name'] = set_value('last_name');
				// $values['username'] = set_value('username');
				$values['email'] = set_value('email');
				$values['phone'] = set_value('phone');
				$values['password'] = set_value('password');
				$values['conf_password'] = set_value('conf_password');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/users/edit_user/'.$this->input->post('user_id'));

			}else{

				$id = $this->input->post('user_id');

				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				// $username = $this->input->post('username');
				$username = strtolower($this->input->post('first_name').'_'.$this->input->post('last_name'));
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');


				if($this->input->post('password')){

					$password = $this->input->post('password');

					$data = array(
						'first_name' => $first_name,
						'last_name' => $last_name,
						'password' => $password,
						'username' => $username,
						'email' => $email,
						'phone' => $phone,
						);

				}else{

					$data = array(
						'first_name' => $first_name,
						'last_name' => $last_name,
						'username' => $username,
						'email' => $email,
						'phone' => $phone,
						);

				}

				$user_updated = $this->ion_auth->update($id, $data);

				if ($user_updated) {
					$this->session->set_flashdata('success_message', 'User updated successfully.');
					redirect(base_url() . 'admin/users');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error updating this User.');
					redirect(base_url() . 'admin/users');
				}

			}

		}
	}

}