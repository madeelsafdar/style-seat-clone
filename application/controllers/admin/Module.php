<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class module extends My_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $table = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('table_model'));
	}

	public function manage($table = '')
	{
		$this->table = $table;
		$data['table'] = $table;
		$data['all_fields'] = $this->table_model->get_field_names_without_primary_key($table);
		$foreign_keys = $this->table_model->get_all_foreign_keys($table);

		$foreign_fields = array();
		foreach ($foreign_keys as $foreign_key) {
			$foreign_fields[] = $this->table_model->get_title_field($foreign_key['foreign_table']);
		}
		
		$data['foreign_fields'] = $foreign_fields;

		$data['primary_key'] = $this->table_model->get_primary_key($table);
		$this->load->view('admin/module/manage',$data);
	}

	public function get_records($table = '')
	{

		$iDisplayLength = $this->input->get('iDisplayLength');
		$iDisplayStart = $this->input->get('iDisplayStart');
		$sSortDir_0 = $this->input->get('sSortDir_0');
		$iSortCol_0 = $this->input->get('iSortCol_0');
		$sSearch = $this->input->get('sSearch');

		$all_fields = $this->table_model->get_field_names_without_primary_key($table);
		$primary_key = $this->table_model->get_primary_key($table);
		$individual_search = array();

		// Individual filter remaining

		
		
		$all_records = $this->table_model->dt_get_records($table,$iDisplayLength,$iDisplayStart,$sSearch,$sSortDir_0,$iSortCol_0,$individual_search);
		$iTotalRecords = $this->table_model->dt_get_records_count($table,$sSearch,$sSortDir_0,$iSortCol_0,$individual_search);

		$result = array('iTotalRecords' => $iTotalRecords,'iTotalDisplayRecords' => $iTotalRecords,'aaData' => $all_records);
		echo json_encode($result);
	}

	public function add($table = '')
	{
		$this->table = $table;
		$data['table'] = $table;
		$data['all_fields'] = $this->table_model->get_field_names_without_primary_key($table);
		$foreign_keys = $this->table_model->get_all_foreign_keys($table);
		$data['foreign_keys'] = $foreign_keys;

		$foreign_fields = array();
		foreach ($foreign_keys as $foreign_key) {
			$foreign_fields[] = $this->table_model->get_title_field($foreign_key['foreign_table']);
			$data['all_'.$foreign_key['foreign_table']] = $this->table_model->get_all_records($foreign_key['foreign_table']);
		}
		
		$data['foreign_fields'] = $foreign_fields;
		$this->load->view('admin/module/add',$data);
	}

	public function edit($table = '',$record_id = '')
	{
		$this->table = $table;
		$data['table'] = $table;
		$data['all_fields'] = $this->table_model->get_field_names_without_primary_key($table);
		$data['primary_key'] = $this->table_model->get_primary_key($table);
		$data['record_info'] = $this->table_model->get_record_info_by_id($table,$record_id);
		$this->load->view('admin/module/edit',$data);
	}

	public function save($table = '')
	{
		$datestring = "Y-m-d h:i:s";
		$time = time();

		if($this->input->post('add')){

			$all_posted_vars = $this->input->post(NULL, TRUE);

			foreach ($all_posted_vars as $posted_var_key => $posted_var_value) {
				$this->form_validation->set_rules($posted_var_key, $this->general->convert_to_label($posted_var_key), 'trim|required');
			}

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				foreach ($all_posted_vars as $posted_var_key => $posted_var_value) {
					$validation_errors[$posted_var_key] = form_error($posted_var_key);
					$values[$posted_var_key] = set_value($posted_var_key);
				}

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/module/add/'.$table);

			}else{

				$data = array();

				foreach ($all_posted_vars as $posted_var_key => $posted_var_value) {
					if($posted_var_key != 'add' && $posted_var_key != 'edit'){
						$data[$posted_var_key] = $posted_var_value;
					}
				}

				$data['created_at'] = date($datestring, $time);

				$this->db->insert($table, $data); 

				redirect(base_url() . 'admin/module/manage/'.$table);
			}

		}else if($this->input->post('edit')){
			$all_posted_vars = $this->input->post(NULL, TRUE);

			foreach ($all_posted_vars as $posted_var_key => $posted_var_value) {
				$this->form_validation->set_rules($posted_var_key, $this->general->convert_to_label($posted_var_key), 'trim|required');
			}

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				foreach ($all_posted_vars as $posted_var_key => $posted_var_value) {
					$validation_errors[$posted_var_key] = form_error($posted_var_key);
					$values[$posted_var_key] = set_value($posted_var_key);
				}

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/module/edit/'.$table);

			}else{

				$data = array();
				$primary_key_name = $this->table_model->get_primary_key($table);

				foreach ($all_posted_vars as $posted_var_key => $posted_var_value) {
					if($posted_var_key != 'add' && $posted_var_key != 'edit' && $posted_var_key != $primary_key_name){
						$data[$posted_var_key] = $posted_var_value;
					}
				}

				$data['updated_at'] = date($datestring, $time);

				$this->db->where($primary_key_name,$this->input->post($primary_key_name));
				$this->db->update($table, $data); 

				redirect(base_url() . 'admin/module/manage/'.$table);

			}
		}

	}

	public function delete($table = '',$record_id = '')
	{
		$this->table_model->delete_record($table,$record_id);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function change_status($table = '',$record_id = '')
	{
		$this->table_model->change_record_status($table,$record_id);
		redirect($_SERVER['HTTP_REFERER']);
	}


}
