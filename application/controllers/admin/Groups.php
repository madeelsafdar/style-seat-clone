<?php defined('BASEPATH') OR exit('No direct script access allowed');

class groups extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('permissions_model','groups_model'));
	}

	public function index(){

		$data['all_groups'] = $this->groups_model->get_all_groups();

		$this->load->view('admin/groups/groups',$data);
	}

	public function users_in_group($group_id = ''){
		$data['users_in_group'] = $this->groups_model->get_users_in_a_group($group_id);
		$data['group_info'] = $this->groups_model->get_group_info($group_id);
		$this->load->view('admin/groups/users_in_group',$data);
	}

	public function ajax_delete_group()
	{

		// var_dump($_POST);

		// die;
		$group_id = $this->input->post('group_id');
		$no_of_users = $this->groups_model->get_no_of_users_in_group($group_id);
		
		if($no_of_users > 0){
			echo $no_of_users;
		}else{
			$group_delete = $this->ion_auth->delete_group($group_id);

			if(!$group_delete)
			{
				// var_dump($this->ion_auth->messages());
			}
			else
			{
				echo 'deleted';
			}
		}
	}

	public function edit_group($group_id = '')
	{
		//Get Group Info
		$data['edit'] = 1;

		$data['group_info'] = $this->groups_model->get_group_info($group_id);

		$this->load->view('admin/groups/add_edit_groups',$data);
	}

	public function add_group()
	{
		$data['add'] = 1;
		$this->load->view('admin/groups/add_edit_groups',$data);
	}

	public function save_group()
	{
		// var_dump($_POST);
		if($this->input->post('add')){

			$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

			$this->form_validation->set_rules('name', 'Group Title', 'trim|required');
			$this->form_validation->set_rules('description', 'Group Description', 'trim');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['name'] = form_error('name');
				$validation_errors['description'] = form_error('description');

				$values['name'] = set_value('name');
				$values['description'] = set_value('description');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/groups/add_group');

			}else{

				$name = $this->input->post('name');
				$description = $this->input->post('description');

				$group = $this->ion_auth->create_group($name, $description);

				if ($group) {
					$this->session->set_flashdata('success_message', 'Group has been created successfully.');
					redirect(base_url() . 'admin/groups');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error creating this Group.');
					redirect(base_url() . 'admin/groups');
				}

			}

		}
		if($this->input->post('edit')){

			$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

			$this->form_validation->set_rules('name', 'Group Title', 'trim|required');
			$this->form_validation->set_rules('description', 'Group Description', 'trim');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['name'] = form_error('name');
				$validation_errors['description'] = form_error('description');

				$values['name'] = set_value('name');
				$values['description'] = set_value('description');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/groups/add_group');

			}else{

				$group_id = $this->input->post('group_id');
				$group_name = $this->input->post('name');
				$group_description = $this->input->post('description');

				$group_update = $this->ion_auth->update_group($group_id, $group_name, $group_description);

				if ($group_update) {
					$this->session->set_flashdata('success_message', 'Group updated successfully.');
					redirect(base_url() . 'admin/groups');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error updating this Group.');
					redirect(base_url() . 'admin/groups');
				}

			}

		}
	}

	public function change_permissions($group_id = '')
	{

		$data['group_info'] = $this->permissions_model->get_group_info($group_id);
		$data['all_modules'] = $this->permissions_model->get_all_modules();
		$data['all_permissions_of_group'] = $this->permissions_model->get_front_permissions_of_group($group_id);
		$data['a'] = '';

		$this->load->view('admin/groups/change_permissions',$data);

	}

	public function save_permissions()
	{
		$group_id = $this->input->post('group_id');
		$new_permissions = $this->input->post('new_permissions');

		$all_allowed_modules = $this->permissions_model->get_allowed_module_ids_of_group($group_id);

		// echo "New Permissions";
		// var_dump($new_permissions);

		// echo "Old Permissions";
		// var_dump($all_allowed_modules);
		
		/////////////To Delete/////////////
		if($all_allowed_modules != '' && $new_permissions != ''){
			$to_delete_permissions = array_diff($all_allowed_modules, $new_permissions);
		}elseif($all_allowed_modules == '' && $new_permissions != '') {
			$to_delete_permissions = array();
		}elseif ($all_allowed_modules != '' && $new_permissions == '') {
			$to_delete_permissions = $all_allowed_modules;
		}else{
			$to_delete_permissions = array();
		}
		

		if (!empty($to_delete_permissions)) {

			$delete_ids_str = implode($to_delete_permissions, ', ');
			$delete_permissions_query = "DELETE FROM group_action WHERE id_action IN ($delete_ids_str) AND id_group = $group_id";
			$this->db->query($delete_permissions_query);

		}

		// echo "Delete";
		// var_dump($to_delete_permissions);

		/////////////To Create/////////////
		if($all_allowed_modules != '' && $new_permissions != ''){
			$to_insert_permissions = array_diff($new_permissions, $all_allowed_modules);
		}elseif($all_allowed_modules == '' && $new_permissions != '') {
			$to_insert_permissions = $new_permissions;
		}elseif ($all_allowed_modules != '' && $new_permissions == '') {
			$to_insert_permissions = array();
		}else{
			$to_insert_permissions = array();
		}

		if (!empty($to_insert_permissions)) {
			
			$insert_permission_data = array();
			$count = 0;
			foreach ($to_insert_permissions as $module_id) {
				$insert_permission_data[$count]['id_group'] = $group_id;
				$insert_permission_data[$count]['id_action'] = $module_id;
				$count++;
			}

		// echo "Insert";
		// var_dump($insert_permission_data);

			$change_permissions = $this->db->insert_batch('group_action', $insert_permission_data);

		}

		// if ($change_permissions) {
			$this->session->set_flashdata('success_message', 'Group permissions have been updated.');
			redirect(base_url() . 'admin/groups');
		// }

		// $ref = $this->input->server('HTTP_REFERER', TRUE);
		// redirect($ref, 'location');
		
	}

}