<?php defined('BASEPATH') OR exit('No direct script access allowed');

class index extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		redirect('admin/auth/index', 'refresh');
	}

}