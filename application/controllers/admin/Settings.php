<?php
class settings extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('settings_model');
	}

	public function index($setting_id=''){
		
		$data['sub_settings']=$this->settings_model->get_settings($setting_id);
		$setting_title=$this->settings_model->get_setting_title($setting_id);
		$data['parent_setting_id']=$setting_title->setting_id;
		$data['setting_title'] =$setting_title->setting_alias;
		
		$this->load->view('admin/general_settings',$data);
	}

	public function update_setting_value(){

		if(isset($_FILES['input_logo_file'])){
			$name = $_FILES["input_logo_file"]["name"];
			$ext = end((explode(".", $name)));
		}

		$parent_setting_id=$this->input->post('parent_setting_id');
		$sub_setting=$this->settings_model->get_settings($parent_setting_id);
		$case="CASE setting_name ";
		$setting_name_str="";
		foreach ($sub_setting as $setting) {
			if($setting['setting_name'] == 'picture_logo_url' && isset($_FILES['input_logo_file'])){
				$setting_value=base_url().'public/site_files/logo'.'.'.$ext;
				$setting_name=$setting['setting_name'];
				$setting_parent_id=$parent_setting_id;
				$case.="WHEN '$setting_name' THEN '$setting_value' ";
				$setting_name_str.="'$setting_name',";
			}else if($setting['setting_name'] == 'home_page_advertisement'){
				$setting_value=$this->input->post($setting['setting_name']);
				$setting_name=$setting['setting_name'];
				$setting_parent_id=$parent_setting_id;
				$case.="WHEN '$setting_name' THEN '$setting_value' ";
				$setting_name_str.="'$setting_name',";
			}else{
				$setting_value=$this->input->post($setting['setting_name']);
				$setting_name=$setting['setting_name'];
				$setting_parent_id=$parent_setting_id;
				$case.="WHEN '$setting_name' THEN '$setting_value' ";
				$setting_name_str.="'$setting_name',";
			}
		}
		$case.="END";
		$setting_name_str=rtrim($setting_name_str, ',');
		$query="UPDATE settings
		SET setting_value = ($case)
		WHERE setting_name IN($setting_name_str);";
		$this->db->query($query);
		
		if($this->input->post('is_picture_logo') != '' && isset($_FILES['input_logo_file'])){
			$config['upload_path'] = 'public/site_files/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name'] = 'logo'.'.'.$ext;
			$config['overwrite'] = true;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('input_logo_file'))
			{
				echo $this->upload->display_errors();
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
			}
		}
		
		

		redirect($_SERVER['HTTP_REFERER']);

	}

}

?>