<?php defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('permissions_model','groups_model'));
	}

	public function index(){
		$data['sss'] = '';
		$this->load->view('admin/dashboard', $data);
	}

}