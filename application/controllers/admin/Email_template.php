<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class email_template extends MY_Controller
	{
		

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('email_template_model'));
		
	}

	public function index()
	{	
		
	$data['all_email_templates']= $this->email_template_model->get_all_email_templates();
	$this->load->view('admin/email_settings',$data);
		
	}

	public function open_email_template($email_template_id="")
	{
		$data['email_template_info']=$this->email_template_model->get_email_template_by_id($email_template_id);

		$this->load->view('admin/email_template', $data);

	}


public function send_email(){
	$this->load->model('send_email_model');
	$parse_data = array(
      'first_name' => 'Adeel',
      'last_name' => 'Safdar',
      'project_title' => 'Mission Impossible 1'
      );
	$sendto='madeel.xyper@gmail.com';
	$email_template_id='2';
	$this->send_email_model->send_email($sendto, $email_template_id, $parse_data);
}


public function save_email_template()
{

	$datestring = "Y-m-d h:i:s";
	$time = time();

	//var_dump($_POST);
	//die();

	if($this->input->post('add')){

		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

		$this->form_validation->set_rules('email_template_title', 'email_template Title', 'trim|required');
		$this->form_validation->set_rules('email_template_subject', 'email_template Subject', 'trim|required');
		$this->form_validation->set_rules('email_template_content', 'email_template content', 'trim|required');

		if ($this->form_validation->run() == FALSE) {

			$validation_errors = array();
			$values            = array();

			$validation_errors['email_template_title'] = form_error('email_template_title');
			$validation_errors['email_template_subject'] = form_error('email_template_subject');
			$validation_errors['email_template_content'] = form_error('email_template_content');

			$values['email_template_title'] = set_value('email_template_title');
			$values['email_template_subject'] = set_value('email_template_subject');
			$values['email_template_content'] = set_value('email_template_content');

			@session_start();

			$_SESSION['values'] = $values;
			$_SESSION['errors'] = $validation_errors;

			redirect($_SERVER['HTTP_REFERER']);

		}else{

			$email_template_title = $this->input->post('email_template_title');
			$email_template_subject = $this->input->post('email_template_subject');
			$email_template_content = $this->input->post('email_template_content');

			$data = array(
				'email_template_title' => $email_template_title ,
				'email_template_subject' => $email_template_subject ,
				'email_template_content' => $email_template_content,
				'created_at' => date($datestring, $time),
				'updated_at' => date($datestring, $time)
				);

			$email_template = $this->db->insert('email_templates', $data); 


			if ($email_template) {
				$this->session->set_flashdata('success_message', 'Email Tempalte added successfully.');
				redirect(base_url() . 'settings/email_templates');
			} else {
				$this->session->set_flashdata('failure_message', 'There was an error creating this Email Template.');
				redirect(base_url() . 'settings/email_templates');
			}


		}

	}

	if($this->input->post('edit')){

		$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

		$this->form_validation->set_rules('email_template_title', 'email_template Title', 'trim|required');
		$this->form_validation->set_rules('email_template_subject', 'email_template Subject', 'trim|required');
		$this->form_validation->set_rules('email_template_content', 'email_template content', 'trim|required');

		if ($this->form_validation->run() == FALSE) {

			$validation_errors = array();
			$values            = array();

			$validation_errors['email_template_title'] = form_error('email_template_title');
			$validation_errors['email_template_subject'] = form_error('email_template_subject');
			$validation_errors['email_template_content'] = form_error('email_template_content');

			$values['email_template_title'] = set_value('email_template_title');
			$values['email_template_subject'] = set_value('email_template_subject');
			$values['email_template_content'] = set_value('email_template_content');

			@session_start();

			$_SESSION['values'] = $values;
			$_SESSION['errors'] = $validation_errors;

			redirect($_SERVER['HTTP_REFERER']);

		}else{

			$email_template_id = $this->input->post('email_template_id');
			$email_template_title = $this->input->post('email_template_title');
			$email_template_subject = $this->input->post('email_template_subject');
			$email_template_content = $this->input->post('email_template_content');

			$data = array(
				'email_template_title' => $email_template_title ,
				'email_template_subject' => $email_template_subject ,
				'email_template_content' => $email_template_content,
				'updated_at' => date($datestring, $time)
				);

			$this->db->where('email_template_id', $email_template_id);
			$email_template_update = $this->db->update('email_templates', $data);

			if ($email_template_update) {
				$this->session->set_flashdata('success_message', 'Email Template updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('failure_message', 'There was an error updating this Email Template.');
			redirect($_SERVER['HTTP_REFERER']);
			}

		}

	}

}
	}
?>