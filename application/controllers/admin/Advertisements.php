<?php defined('BASEPATH') OR exit('No direct script access allowed');

class advertisements extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('permissions_model','groups_model','advertisements_model'));
	}

	public function index(){
		$data['all_advertisements'] = $this->advertisements_model->get_all_advertisements();
		$this->load->view('admin/advertisements/advertisements',$data);
	}

	public function add_advertisement()
	{
		$data['add'] = 1;
		$this->load->view('admin/advertisements/add_edit_advertisements',$data);
	}

	public function edit_advertisement($advertisement_id = '')
	{
		$data['edit'] = 1;
		$data['advertisement_info'] = $this->advertisements_model->get_advertisement_info_by_id($advertisement_id);
		$this->load->view('admin/advertisements/add_edit_advertisements',$data);
	}

	public function ajax_delete_advertisement()
	{
		$advertisement_id = $this->input->post('advertisement_id');
		$advertisement_delete = $this->db->delete('advertisements', array('advertisements_id' => $advertisements_id)); 

		if(!$advertisement_delete)
		{
		}
		else
		{
			echo 'deleted';
		}
		
	}

	public function save_advertisement()
	{
		// var_dump($_POST);
		// die;
		if($this->input->post('add')){

			$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

			$this->form_validation->set_rules('advertisement_title', 'Advertisement Title', 'trim|required');
			$this->form_validation->set_rules('advertisement_text', 'Advertisement Text', 'trim|required');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['advertisement_title'] = form_error('advertisement_title');
				$validation_errors['advertisement_text'] = form_error('advertisement_text');

				$values['advertisement_title'] = set_value('advertisement_title');
				$values['advertisement_text'] = set_value('advertisement_text');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/advertisements/add_advertisement');

			}else{

				$data = array(
					'advertisement_title' => $this->input->post('advertisement_title'),
					'advertisement_text' => $this->input->post('advertisement_text')
					);

				$advertisement_saved = $this->db->insert('advertisements', $data); 

				if ($advertisement_saved) {
					$this->session->set_flashdata('success_message', 'advertisement added successfully.');
					redirect(base_url() . 'admin/advertisements');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error creating this advertisement.');
					redirect(base_url() . 'admin/advertisements');
				}

			}

		}
		if($this->input->post('edit')){

			$this->form_validation->set_error_delimiters('<label class="error" style="height: 33px;" generated="true">', '</label>');

			$this->form_validation->set_rules('advertisement_title', 'Advertisement Title', 'trim|required');
			$this->form_validation->set_rules('advertisement_text', 'Advertisement Text', 'trim|required');

			if ($this->form_validation->run() == FALSE) {

				$validation_errors = array();
				$values            = array();

				$validation_errors['advertisement_title'] = form_error('advertisement_title');
				$validation_errors['advertisement_text'] = form_error('advertisement_text');

				$values['advertisement_title'] = set_value('advertisement_title');
				$values['advertisement_text'] = set_value('advertisement_text');

				@session_start();

				$_SESSION['values'] = $values;
				$_SESSION['errors'] = $validation_errors;

				redirect(base_url() . 'admin/advertisements/edit_advertisement/'.$this->input->post('advertisement_id'));

			}else{

				$advertisement_id = $this->input->post('advertisement_id');
				$advertisement_title = $this->input->post('advertisement_title');
				$advertisement_text = $this->input->post('advertisement_text');

				$data = array(
					'advertisement_title' => $advertisement_title,
					'advertisement_text' => $advertisement_text
					);

				$this->db->where('advertisement_id', $advertisement_id);
				$advertisement_updated = $this->db->update('advertisements', $data); 

				if ($advertisement_updated) {
					$this->session->set_flashdata('success_message', 'advertisement updated successfully.');
					redirect(base_url() . 'admin/advertisements');
				} else {
					$this->session->set_flashdata('failure_message', 'There was an error updating this advertisement.');
					redirect(base_url() . 'admin/advertisements');
				}

			}

		}
	}

}