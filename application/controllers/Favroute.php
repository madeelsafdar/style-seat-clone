<?php defined('BASEPATH') OR exit('No direct script access allowed');

class favroute extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','favroute_model'));
	}

	public function my_favroutes()
	{

		$data['all_my_favroutes'] = $this->favroute_model->get_all_my_favroutes();
		$this->load->view('my_favroutes',$data);
	}

	public function remove_favroute($favroute_picture_id = '')
	{
		$this->db->delete('favroute_pictures', array('favroute_picture_id' => $favroute_picture_id)); 
		redirect($_SERVER['HTTP_REFERER']);
	}

}