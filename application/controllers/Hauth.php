<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HAuth extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','categories_model'));
	}

	public function index()
	{
		$this->load->view('hauth/home');
	}

	public function login($provider)
	{
		// log_message('debug', "controllers.HAuth.login($provider) called");

		try
		{
			// log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');
			$this->load->library('HybridAuthLib');

			if ($this->hybridauthlib->providerEnabled($provider))
			{
				// log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
				$service = $this->hybridauthlib->authenticate($provider);

				if ($service->isUserConnected())
				{
					// log_message('debug', 'controller.HAuth.login: user authenticated.');

					$user_profile = $service->getUserProfile();

					// log_message('info', 'controllers.HAuth.login: user profile:'.PHP_EOL.print_r($user_profile, TRUE));

					$data['user_profile'] = $user_profile;

					$identifier = $user_profile->identifier;
					$webSiteURL = $user_profile->webSiteURL;
					$profileURL = $user_profile->profileURL;
					$photoURL = $user_profile->photoURL;
					$displayName = $user_profile->displayName;
					$description = $user_profile->description;
					$firstName = $user_profile->firstName;
					$lastName = $user_profile->lastName;
					$gender = $user_profile->gender;
					$language = $user_profile->language;
					$age = $user_profile->age;
					$birthDay = $user_profile->birthDay;
					$birthMonth = $user_profile->birthMonth;
					$birthYear = $user_profile->birthYear;
					$email = $user_profile->email;
					$emailVerified = $user_profile->emailVerified;
					$phone = $user_profile->phone;
					$address = $user_profile->address;
					$country = $user_profile->country;
					$region = $user_profile->region;
					$city = $user_profile->city;
					$zip = $user_profile->zip;

					$is_exist = $this->users_model->check_user_by_identifier($identifier);

					if($is_exist){
						// echo "1";
						// var_dump($is_exist);

						if($email != ''){
							$email = $email;
						}else{
							$email = str_replace(' ', '_', strtolower($displayName)).'@'.str_replace(' ', '_', strtolower($provider)).'.com';
						}

						
						$password = $is_exist->identifier;

						if ($this->ion_auth->login($email, $password, 0)){

							// if(isset($_POST['ajax_login'])){
							// 	$to_return['redirect_to'] = base_url();
							// }else{
								redirect(base_url());
							// }

							// echo json_encode($to_return);

						}

					}else{
						// register

						$username = str_replace(' ', '_', strtolower($displayName));
						if($email != ''){
							$email = $email;
						}else{
							$email = str_replace(' ', '_', strtolower($displayName)).'@'.str_replace(' ', '_', strtolower($provider)).'.com';
						}
						$password = $identifier;

						$additional_data = array(
							'first_name' => $firstName,
							'last_name'  => $lastName,
							'company'    => '',
							'phone'      => $phone,
							'identifier' => $identifier,
							);

						$group = array(2);

						$user_saved = $this->ion_auth->register($username, $password, $email, $additional_data, $group);

						$data = array(
							'active' => 1
							);

						$this->db->where('id', $user_saved);
						$this->db->update('users', $data); 

						// $sendto = $email;

						// $email_template_id='1';
						// $parse_data = array(
						// 	'username' => $this->input->post('username'),
						// 	'email' => $this->input->post('contact_person_last_name'),
						// 	'activation_link' => '<a href="'.base_url('users/activate').'/'.$user_saved.'/'.$unique_str.'">Activate Account</a>'
						// 	);
						// $this->send_email($sendto, $email_template_id, $parse_data);

						//login

						if ($this->ion_auth->login($email, $password, 0)){

							// if(isset($_POST['ajax_login'])){
							// 	$to_return['redirect_to'] = base_url();
							// }else{
								redirect(base_url());
							// }

							// echo json_encode($to_return);

						}

					}

					// $this->load->view('hauth/done',$data);
				}
				else // Cannot authenticate user
				{
					show_error('Cannot authenticate user');
				}
			}
			else // This service is not enabled.
			{
				// log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
				show_404($_SERVER['REQUEST_URI']);
			}
		}
		catch(Exception $e)
		{
			$error = 'Unexpected error';
			switch($e->getCode())
			{
				case 0 : $error = 'Unspecified error.'; break;
				case 1 : $error = 'Hybriauth configuration error.'; break;
				case 2 : $error = 'Provider not properly configured.'; break;
				case 3 : $error = 'Unknown or disabled provider.'; break;
				case 4 : $error = 'Missing provider application credentials.'; break;
				case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
				         //redirect();
				if (isset($service))
				{
					log_message('debug', 'controllers.HAuth.login: logging out from service.');
					$service->logout();
				}
				show_error('User has cancelled the authentication or the provider refused the connection.');
				break;
				case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
				break;
				case 7 : $error = 'User not connected to the provider.';
				break;
			}

			if (isset($service))
			{
				$service->logout();
			}

			log_message('error', 'controllers.HAuth.login: '.$error);
			show_error('Error authenticating user.');
		}
	}

	public function endpoint()
	{

		// log_message('debug', 'controllers.HAuth.endpoint called.');
		// log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			// log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
			$_GET = $_REQUEST;
		}

		// log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
		require_once APPPATH.'/third_party/hybridauth/index.php';

		// var_dump($_REQUEST);
		// echo "Adeel Safdar";

	}
}

/* End of file hauth.php */
/* Location: ./application/controllers/hauth.php */
