<?php defined('BASEPATH') OR exit('No direct script access allowed');

class gallery extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','comments_model','categories_model'));
	}

	public function view_gallery($gallery_id = '')
	{
		$data['gallery_info'] = $this->galleries_model->get_gallery_images_by_id($gallery_id);
		$data['all_gallery_comments'] = $this->comments_model->get_comment_of_gallery($gallery_id);
		// $this->load->view('view_gallery',$data);
		$this->load->view('stacked_gallery',$data);
	}

	public function edit_gallery($gallery_id = '')
	{
		$data['gallery_info'] = $this->galleries_model->get_gallery_images_by_id($gallery_id);
		$data['gallery_info_without_pics'] = $this->galleries_model->get_gallery_categories($gallery_id);
		$data['all_categories'] = $this->categories_model->get_all_categories();
		$data['all_gallery_comments'] = $this->comments_model->get_comment_of_gallery($gallery_id);
		// $this->load->view('view_gallery',$data);
		$this->load->view('edit_gallery',$data);
	}

	public function view_image($image_id = '')
	{
		$data['image_info'] = $this->images_model->get_image_info($image_id);

		$data['next_image_id'] = $this->images_model->get_next_image_id($image_id);
		$data['prev_image_id'] = $this->images_model->get_prev_image_id($image_id);

		$data['similar_categories_images'] = $this->images_model->get_related_images($image_id);
		$data['similar_categories_galleries'] = $this->galleries_model->get_related_galleries($image_id);

		$data['all_picture_comments'] = $this->comments_model->get_comment_of_picture($image_id);

		$this->load->view('view_image',$data);
	}

	public function my_galleries()
	{
		$user = $this->ion_auth->user()->row();
		if($user){
			$data['all_galleries_info'] = $this->galleries_model->get_all_galleries_by_id($user->id);
			$this->load->view('my_galleries',$data);
		}else{
			redirect(base_url());
		}
		
	}

	public function get_all_galleries_ajax()
	{
		$loaded_result = $_POST['loaded_result'];
		$items_per_load = $_POST['items_per_load'];
		$set_post_order_by_created_at = $this->input->post('set_post_order_by_created_at');
		$set_post_order_by_views = $this->input->post('set_post_order_by_views');

		$images_data = $this->galleries_model->get_all_galleries($loaded_result,$items_per_load,$set_post_order_by_created_at,$set_post_order_by_views);

		echo json_encode($images_data);
	}

	public function get_all_galleries_by_category_ajax()
	{
		$loaded_result = $_POST['loaded_result'];
		$items_per_load = $_POST['items_per_load'];
		$category_id = $_POST['category_id'];

		$images_data = $this->galleries_model->get_all_galleries_by_category($loaded_result,$items_per_load,$category_id);

		echo json_encode($images_data);
	}

	public function get_all_galleries_by_search_ajax()
	{
		$loaded_result = $_POST['loaded_result'];
		$items_per_load = $_POST['items_per_load'];
		$search_str = $_POST['search_str'];

		$images_data = $this->galleries_model->get_all_galleries_by_search_str($loaded_result,$items_per_load,$search_str);

		echo json_encode($images_data);
	}

	public function save_gallery_detail()
	{
		$datestring = "Y-m-d h:i:s";
		$time       = time();

		$gallery_id = $this->input->post('gallery_id');
		$gallery_title = $this->input->post('gallery_title');
		$gallery_description = $this->input->post('gallery_description');

		$data = array(
			'gallery_title' => $gallery_title,
			'gallery_description' => $gallery_description
			);

		$this->db->where('gallery_id', $gallery_id);
		$this->db->update('galleries', $data); 

		$gallery_categories = $this->input->post('gallery_categories');

		if($gallery_categories != ''){

			$this->db->delete('gallery_categories', array('gallery_id' => $gallery_id)); 

			$count = 0;
			$to_insert_gallery_categories = array();
			foreach ($gallery_categories as $gallery_category) {

				$to_insert_gallery_categories[$count]['gallery_id'] = $gallery_id;
				$to_insert_gallery_categories[$count]['category_id'] = $gallery_category;
				$to_insert_gallery_categories[$count]['status'] = 1;
				$to_insert_gallery_categories[$count]['created_at'] = date($datestring, $time);

				$count++;
			}

			$this->db->insert_batch('gallery_categories', $to_insert_gallery_categories); 

		}

		redirect($_SERVER['HTTP_REFERER']);

	}

	public function add_view_to_gallery()
	{
		$gallery_id = $_POST['gallery_id'];
		$user = $this->ion_auth->user()->row();

		$total_views = $this->galleries_model->get_gallery_views($gallery_id);
		$total_views = $total_views->views;
		
		if($total_views != ''){

			$total_views++;

			$data = array(
				'views' => $total_views
				);

			$this->db->where('gallery_id', $gallery_id);
			$this->db->update('galleries', $data);
		}else{
			$data = array(
				'views' => 1
				);

			$this->db->where('gallery_id', $gallery_id);
			$this->db->update('galleries', $data);
		}

	}

	public function get_gallery_details()
	{
		$gallery_id = $this->input->post('gallery_id');
		$picture_number = $this->input->post('picture_number');
		$data['gallery_info'] = $this->galleries_model->get_gallery_images_by_id($gallery_id,$picture_number);
		$data['all_gallery_comments'] = $this->comments_model->get_comment_of_gallery($gallery_id);
		echo json_encode($data);
	}

}