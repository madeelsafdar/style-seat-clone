<?php defined('BASEPATH') OR exit('No direct script access allowed');

class about extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model'));
	}

	public function index()
	{
		$this->load->view('about');
	}

}