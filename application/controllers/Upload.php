<?php defined('BASEPATH') OR exit('No direct script access allowed');

class upload extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model'));
	}

	public function ajax_upload_user_pictures()
	{

		// var_dump($_POST);
		// die();
		
		$is_same_album = $_POST['is_same_album'];
		$type = $_POST['type'];
		$datestring = "Y-m-d h:i:s";
		$time       = time();
		$gallery_title = $_POST['gallery_title'];
		$gallery_description = $_POST['gallery_description'];
		$gallery_meta_tags = $_POST['gallery_meta_tags'];
		$gallery_categories = $this->input->post('gallery_categories');

		$user = $this->ion_auth->user()->row();
		$next_gallery_id = $this->general_model->get_next_auto_id('galleries');
		$next_picture_id = $this->general_model->get_next_auto_id('pictures');
		
		if($is_same_album == 0){

			mkdir('./public/pictures_uploaded/gallery_'.$next_gallery_id.'/', 0777, TRUE);
			mkdir('./public/thumbnails_uploaded/gallery_'.$next_gallery_id.'/', 0777, TRUE);

			$data = array(
				'user_id' => $user->id ,
				'share_with_community' => 0 ,
				'gallery_title' => $gallery_title,
				'gallery_description' => $gallery_description,
				'gallery_meta_tags' => $gallery_meta_tags,
				'status' => 1 ,
				'created_at' => date($datestring, $time)
				);

			$this->db->insert('galleries', $data); 

			if($gallery_categories != ''){
				$gallery_categories_ids = explode(',', $gallery_categories);
				$gallery_categories_array = array();

				$count = 0;
				foreach ($gallery_categories_ids as $key => $gallery_categories_id) {
					$gallery_categories_array[$count]['category_id'] = $gallery_categories_id;
					$gallery_categories_array[$count]['gallery_id'] = $next_gallery_id;
					$gallery_categories_array[$count]['status'] = 1;
					$gallery_categories_array[$count]['created_at'] = date($datestring, $time);
					$count++;
				}

				$this->db->insert_batch('gallery_categories', $gallery_categories_array); 
			}

			

			if($type == 'base64'){
				$data = $_POST['file'];
				
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);

				file_put_contents('./public/pictures_uploaded/gallery_'.$next_gallery_id.'/picture_'.$next_picture_id.'.png', $data);

				// *********************************************************

				$source_path = 'public/pictures_uploaded/gallery_'.$next_gallery_id.'/'.'picture_'.$next_picture_id.'.png';
				$target_path = 'public/thumbnails_uploaded/gallery_'.$next_gallery_id.'/'.'picture_'.$next_picture_id.'.png';

				$this->load->library('image_lib');

				$img_cfg['image_library'] = 'gd2';
				$img_cfg['source_image'] = $source_path;
				$img_cfg['maintain_ratio'] = TRUE;
				$img_cfg['create_thumb'] = FALSE;
				$img_cfg['new_image'] = $target_path;
				$img_cfg['width'] = 250;
				$img_cfg['quality'] = 100;
				$img_cfg['height'] = 250;

				$this->image_lib->initialize($img_cfg);
				$this->image_lib->resize();
				$this->image_lib->clear();

				// *********************************************************

				$data = array(
					'picture_title' => 'Picture Title' ,
					'file_name' => 'picture_'.$next_picture_id.'.png' ,
					'gallery_id' => $next_gallery_id ,
					'user_id' => $user->id ,
					'status' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('pictures', $data); 

			}else{

				$name = $_FILES["file"]["name"];
				$ext = end((explode(".", $name)));
				$config['upload_path'] = 'public/pictures_uploaded/gallery_'.$next_gallery_id.'/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = 'picture_'.$next_picture_id.'.'.$ext;
				$config['overwrite'] = true;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('file'))
				{
					echo $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
				}

				// *********************************************************

				$source_path = 'public/pictures_uploaded/gallery_'.$next_gallery_id.'/'.'picture_'.$next_picture_id.'.'.$ext;
				$target_path = 'public/thumbnails_uploaded/gallery_'.$next_gallery_id.'/'.'picture_'.$next_picture_id.'.'.$ext;

				$this->load->library('image_lib');

				$img_cfg['image_library'] = 'gd2';
				$img_cfg['source_image'] = $source_path;
				$img_cfg['maintain_ratio'] = TRUE;
				$img_cfg['create_thumb'] = FALSE;
				$img_cfg['new_image'] = $target_path;
				$img_cfg['width'] = 250;
				$img_cfg['quality'] = 100;
				$img_cfg['height'] = 250;

				$this->image_lib->initialize($img_cfg);
				$this->image_lib->resize();
				$this->image_lib->clear();

				// *********************************************************

				$data = array(
					'picture_title' => 'Picture Title' ,
					'file_name' => 'picture_'.$next_picture_id.'.'.$ext ,
					'gallery_id' => $next_gallery_id ,
					'user_id' => $user->id ,
					'status' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('pictures', $data); 

			}

			echo $next_gallery_id;

		}else{

			$current_gallery_id = $next_gallery_id - 1;

			if($type == 'base64'){
				$data = $_POST['file'];

				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);

				file_put_contents('./public/pictures_uploaded/gallery_'.$current_gallery_id.'/picture_'.$next_picture_id.'.png', $data);

				// *********************************************************

				$source_path = 'public/pictures_uploaded/gallery_'.$current_gallery_id.'/'.'picture_'.$next_picture_id.'.png';
				$target_path = 'public/thumbnails_uploaded/gallery_'.$current_gallery_id.'/'.'picture_'.$next_picture_id.'.png';

				$this->load->library('image_lib');

				$img_cfg['image_library'] = 'gd2';
				$img_cfg['source_image'] = $source_path;
				$img_cfg['maintain_ratio'] = TRUE;
				$img_cfg['create_thumb'] = FALSE;
				$img_cfg['new_image'] = $target_path;
				$img_cfg['width'] = 250;
				$img_cfg['quality'] = 100;
				$img_cfg['height'] = 250;

				$this->image_lib->initialize($img_cfg);
				$this->image_lib->resize();
				$this->image_lib->clear();

				// *********************************************************

				$data = array(
					'picture_title' => 'Picture Title' ,
					'file_name' => 'picture_'.$next_picture_id.'.png' ,
					'gallery_id' => $current_gallery_id ,
					'user_id' => $user->id ,
					'status' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('pictures', $data); 

			}else{

				$name = $_FILES["file"]["name"];
				$ext = end((explode(".", $name)));
				$config['upload_path'] = 'public/pictures_uploaded/gallery_'.$current_gallery_id.'/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['file_name'] = 'picture_'.$next_picture_id.'.'.$ext;
				$config['overwrite'] = true;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('file'))
				{
					echo $this->upload->display_errors();
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
				}

				// *********************************************************

				$source_path = 'public/pictures_uploaded/gallery_'.$current_gallery_id.'/'.'picture_'.$next_picture_id.'.'.$ext;
				$target_path = 'public/thumbnails_uploaded/gallery_'.$current_gallery_id.'/'.'picture_'.$next_picture_id.'.'.$ext;

				$this->load->library('image_lib');

				$img_cfg['image_library'] = 'gd2';
				$img_cfg['source_image'] = $source_path;
				$img_cfg['maintain_ratio'] = TRUE;
				$img_cfg['create_thumb'] = FALSE;
				$img_cfg['new_image'] = $target_path;
				$img_cfg['width'] = 250;
				$img_cfg['quality'] = 100;
				$img_cfg['height'] = 250;

				$this->image_lib->initialize($img_cfg);
				$this->image_lib->resize();
				$this->image_lib->clear();

				// *********************************************************

				$data = array(
					'picture_title' => 'Picture Title' ,
					'file_name' => 'picture_'.$next_picture_id.'.'.$ext ,
					'gallery_id' => $current_gallery_id ,
					'user_id' => $user->id ,
					'status' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('pictures', $data); 

			}

			echo $current_gallery_id;

		}



	}

}