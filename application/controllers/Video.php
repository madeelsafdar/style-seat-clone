<?php defined('BASEPATH') OR exit('No direct script access allowed');

class video extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','favroute_model','video_model'));
	}

	public function save_video_embed()
	{
		$embed_url = $_POST['embed_url'];
		$this->load->library('AutoEmbed');
		$embed_code = $this->autoembed->parse($embed_url);
		$user = $this->ion_auth->user()->row();

		$data = array(
			'embed_code' => $embed_code ,
			'user_id' => $user->id
			);

		$this->db->insert('embeded_video_user', $data); 

		redirect($_SERVER['HTTP_REFERER']);

	}

	public function my_videos()
	{
		$data['all_my_videos'] = $this->video_model->get_all_my_videos();
		$this->load->view('my_videos',$data);
	}

	public function view_all()
	{
		$data['all_videos'] = $this->video_model->get_all_videos();
		$this->load->view('view_all_videos',$data);
	}

}