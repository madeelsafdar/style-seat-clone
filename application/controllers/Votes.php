<?php defined('BASEPATH') OR exit('No direct script access allowed');

class votes extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('users_model','groups_model','galleries_model','general_model','images_model','votes_model'));
	}

	public function getUserIP()
    {
         $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
    }

	public function vote_up_picture()
	{

		// $user = $this->ion_auth->user()->row();

		// if($user){
			$datestring = "Y-m-d h:i:s";
			$time       = time();

			$ip_address = $this->getUserIP();
			$picture_id = $this->input->post('picture_id');
			$is_voted = $this->votes_model->check_if_voted_before($picture_id);

			if(!$is_voted){
				$data = array(
					'picture_id' => $picture_id ,
					'ip_address' => $ip_address ,
					'is_up' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('picture_votes', $data); 

				echo 'success';

			}else{

				echo 'already_voted';

			}
		// }else{
		// 	echo 'please_login';
		// }

		

	}

	public function vote_up_gallery()
	{

		// $user = $this->ion_auth->user()->row();

		// if($user){
			$datestring = "Y-m-d h:i:s";
			$time       = time();

			$ip_address = $this->getUserIP();
			$gallery_id = $this->input->post('gallery_id');
			$is_voted = $this->votes_model->check_if_voted_before_gallery($gallery_id);

			if(!$is_voted){
				$data = array(
					'gallery_id' => $gallery_id ,
					'ip_address' => $ip_address ,
					'is_up' => 1 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('gallery_votes', $data); 

				echo 'success';

			}else{

				echo 'already_voted';

			}
		// }else{
		// 	echo 'please_login';
		// }

		

	}


	public function vote_down_picture()
	{

		// $user = $this->ion_auth->user()->row();

		// if($user){
			$datestring = "Y-m-d h:i:s";
			$time       = time();

			$picture_id = $this->input->post('picture_id');
			// $user = $this->ion_auth->user()->row();
			$ip_address = $this->getUserIP();
			$is_voted = $this->votes_model->check_if_voted_before($picture_id);

			if(!$is_voted){
				$data = array(
					'picture_id' => $picture_id ,
					'ip_address' => $ip_address ,
					'is_up' => 0 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('picture_votes', $data); 

				echo 'success';

			}else{

				echo 'already_voted';

			}
		// }else{
		// 	echo 'please_login';
		// }

		
	}

	public function vote_down_gallery()
	{

		// $user = $this->ion_auth->user()->row();

		// if($user){
			$datestring = "Y-m-d h:i:s";
			$time       = time();

			$gallery_id = $this->input->post('gallery_id');
			// $user = $this->ion_auth->user()->row();
			$ip_address = $this->getUserIP();
			$is_voted = $this->votes_model->check_if_voted_before_gallery($gallery_id);

			if(!$is_voted){
				$data = array(
					'gallery_id' => $gallery_id ,
					'ip_address' => $ip_address ,
					'is_up' => 0 ,
					'created_at' => date($datestring, $time)
					);

				$this->db->insert('gallery_votes', $data); 

				echo 'success';

			}else{

				echo 'already_voted';

			}
		// }else{
		// 	echo 'please_login';
		// }

		
	}

}