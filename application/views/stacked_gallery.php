<?php $this->load->view('includes/header'); ?>
<?php
function humanTiming ($ptime )
{

	$estimate_time = time() - $ptime;

	if( $estimate_time < 1 )
	{
		return 'less than 1 second ago';
	}

	$condition = array(
		12 * 30 * 24 * 60 * 60  =>  'year',
		30 * 24 * 60 * 60       =>  'month',
		24 * 60 * 60            =>  'day',
		60 * 60                 =>  'hour',
		60                      =>  'minute',
		1                       =>  'second'
		);

	foreach( $condition as $secs => $str )
	{
		$d = $estimate_time / $secs;

		if( $d >= 1 )
		{
			$r = round( $d );
			return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
		}
	}

}
?>
<div class="container">
	<div class="row">

		

		<div class="col-md-9">


			
			<article class="post">

				
				<div class="col-md-12" style="margin: 10px 0px 0px;">

					<div class="col-md-10" style="paddng:0;">
						<h3><?php echo $gallery_info->gallery_title; ?></h3>
					</div>
					<div class="col-md-2" style="paddng:0;">
						<?php
						$user = $this->ion_auth->user()->row();
						if($user && $user->id == $gallery_info->user_id){
							?>
							<a href="<?php echo base_url('gallery/edit_gallery').'/'.$gallery_info->gallery_id; ?>" class="btn btn-block btn-primary search-btn" >Edit Gallery</a>
							<?php
						}
						?>
					</div>

					<div class="col-md-12" style="padding:0;">
						<hr style="border: 1px solid #bcbaba;margin-top: 5px;margin-bottom: 10px;">
					</div>

				</div>
				

				<!-- <img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $image_info->gallery_id; ?>/<?php echo $image_info->file_name; ?>" alt="Image Alternative text" title="4 Strokes of Fun"> -->

				<?php 
					// var_dump($gallery_info);
				?>

				<?php
				$images_comma_seperated = $gallery_info->images_comma_seperated;

				$images_info_array = explode('|', $images_comma_seperated);
				foreach ($images_info_array as $key => $images_info) {
					$images_info_array = explode(',', $images_info);


					$picture_file_name = $images_info_array[0];
					$picture_title = $images_info_array[1];
					$picture_id = $images_info_array[2];

					?>
					<header class="post-header">
						<div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
							<h3><?php echo $picture_title; ?></h3>

							<!-- HOVER IMAGE -->
							<a class="hover-img" href="<?php echo base_url('gallery/view_image').'/'.$picture_id; ?>">
								<img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $gallery_info->gallery_id; ?>/<?php echo $picture_file_name; ?>" alt="Image Alternative text" title="Old No7">
								<!-- <div class="hover-title">Posuere per sit</div> -->
							</a>
						</div>
					</header>
					<?php
				}
				?>

				
			</article>

			<?php
			$user = $this->ion_auth->user()->row();
			if($user){
				?>
				<h2>Leave a Comment</h2>
				<form method="post" action="<?php echo base_url('comments/gallery_save'); ?>">

					<input type="hidden" name="gallery_id" value="<?php echo $gallery_info->gallery_id; ?>">

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Comment</label>
								<textarea name="comment_text" class="form-control" placeholder="Your Comment Here"></textarea>
							</div>
						</div>
					</div>
					<input type="submit" name="" value="Post a Comment" class="btn btn-primary">
				</form>
				<div class="gap"></div>
				<h2 class="mb20">Comments</h2>
				<!-- START COMMENTS -->
				<ul class="comments-list">
					<li>
						<!-- COMMENT -->

						<?php
						foreach ($all_gallery_comments as $key => $comment) {
							$time = strtotime($comment['comment_creation_datetime']);
							?>
							<article class="comment">
								<div class="comment-inner">
									<span class="comment-author-name"><?php
										if($comment['first_name'] == '' && $comment['last_name'] == ''){
											echo $comment['username'];
										}else{
											echo $comment['first_name'].' '.$comment['last_name'];
										}
										?></span>
										<p class="comment-content"><?php echo $comment['comment_text']; ?></p>
										<span class="comment-time"><?php echo humanTiming($time); ?></span>
									</div>
								</article>
								<?php
							}
							?>


						</li>
					</ul>
					<?php
				}
				?>

				
				<!-- END COMMENTS -->
				<div class="gap"></div>


			</div>

			<div class="col-md-3">
				<aside class="sidebar-right hidden-phone">

					<?php
				// var_dump($image_info);
					?>

				</aside>


			</div>

		</div>

	</div>
	<?php $this->load->view('includes/footer'); ?>
	<script type="text/javascript">
		add_view_to_picture(<?php echo $gallery_info->gallery_id; ?>);
	</script>