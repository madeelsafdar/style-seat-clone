<?php $this->load->view('includes/header'); ?>
<div class="container">
	<div class="row">

		<div class="col-md-3">
			<aside class="sidebar-right hidden-phone">

				<?php
				// var_dump($image_info);
				?>

			</aside>


		</div>

		<div class="col-md-9">
			
			<article class="post">
				<header class="post-header">
					<!-- <a class="hover-img" href="post-sidebar-right.html"> -->
					<img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $image_info->gallery_id; ?>/<?php echo $image_info->file_name; ?>" alt="Image Alternative text" title="4 Strokes of Fun">

					<!-- <i class="fa fa-link hover-icon"></i> -->


					<!-- </a> -->
				</header>
				<div class="post-inner">

					<form method="post" action="<?php echo base_url('images/save_image_detail'); ?>">

						<input type="hidden" name="picture_id" id="picture_id" value="<?php echo $image_info->picture_id; ?>">

						<div class="form-group">
							<label for="email">Image Title:</label>
							<input  class="form-control" type="text" name="picture_title" id="" value="<?php echo $image_info->picture_title; ?>">
						</div>

						<div class="form-group">
							<label for="email">Image Description:</label>
							<textarea class="form-control" type="text" name="discription"><?php echo $image_info->discription; ?></textarea>
						</div>

						<div class="form-group">
							<label for="email">Categories:</label>
							<select  class="form-control" name="image_categories[]" id="image_categories" multiple="">
								<?php
								$category_ids_array = explode('|', $image_info->categories_of_picture_ids);
								foreach ($all_categories as $category) {
									?>
									<option <?php if(in_array($category['category_id'], $category_ids_array)){ echo 'selected'; } ?> value="<?php echo $category['category_id']; ?>"><?php echo $category['category_title']; ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="form-group">
						<button type="submit" class="btn btn-default">Save</button>
						</div>
					</form>

				</div>
			</article>


			<div class="gap"></div>
		</div>
		
	</div>

</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
	add_view_to_picture(<?php echo $image_info->picture_id; ?>);
</script>