<?php $this->load->view('includes/header'); ?>
<?php
function humanTiming ($ptime )
{

   $estimate_time = time() - $ptime;

    if( $estimate_time < 1 )
    {
        return 'less than 1 second ago';
    }

    $condition = array(
                12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;

        if( $d >= 1 )
        {
            $r = round( $d );
            return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
        }
    }

}
?>
<div class="container">
	<div class="row">

		

		<div class="col-md-9">


			
			<article class="post">

				
			<div class="col-md-12" style="margin: 10px 0px 0px;">

								<div class="col-md-10" style="paddng:0;">
									<h3><?php echo $gallery_info->gallery_title; ?></h3>
								</div>
								<div class="col-md-2" style="paddng:0;">
									<?php
									$user = $this->ion_auth->user()->row();
									if($user && $user->id == $gallery_info->user_id){
										?>
										<a href="<?php echo base_url('gallery/edit_gallery'); ?>" class="btn btn-block btn-primary search-btn" >Edit Gallery</a>
										<?php
									}
									?>
								</div>

								<div class="col-md-12" style="padding:0;">
									<hr style="border: 1px solid #bcbaba;margin-top: 5px;margin-bottom: 10px;">
								</div>

							</div>
				

					<!-- <img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $image_info->gallery_id; ?>/<?php echo $image_info->file_name; ?>" alt="Image Alternative text" title="4 Strokes of Fun"> -->

					<?php 
					// var_dump($gallery_info);
					?>

					<?php
					$images_comma_seperated = $gallery_info->images_comma_seperated;

					$images_info_array = explode('|', $images_comma_seperated);
					foreach ($images_info_array as $key => $images_info) {
						$images_info_array = explode(',', $images_info);


						$picture_file_name = $images_info_array[0];
						$picture_title = $images_info_array[1];
						$picture_id = $images_info_array[2];

						?>
						<header class="post-header">
						<div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
							<h3><?php echo $picture_title; ?></h3>

							<!-- HOVER IMAGE -->
							<a class="hover-img" href="<?php echo base_url('gallery/view_image').'/'.$picture_id; ?>">
								<img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $gallery_info->gallery_id; ?>/<?php echo $picture_file_name; ?>" alt="Image Alternative text" title="Old No7">
								<!-- <div class="hover-title">Posuere per sit</div> -->
							</a>
						</div>
						</header>
						<?php
					}
					?>

					<div class="post-inner">

					<form method="post" action="<?php echo base_url('gallery/save_gallery_detail'); ?>">

						<input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo $gallery_info->gallery_id; ?>">

						<div class="form-group">
							<label for="email">Gallery Title:</label>
							<input  class="form-control" type="text" name="gallery_title" id="gallery_title" value="<?php echo $gallery_info->gallery_title; ?>">
						</div>

						<div class="form-group">
							<label for="email">Gallery Description:</label>
							<textarea class="form-control" type="text" name="gallery_description"><?php echo $gallery_info->gallery_description; ?></textarea>
						</div>

						<div class="form-group">
							<label for="email">Categories:</label>
							<select  class="form-control" name="gallery_categories[]" id="gallery_categories" multiple="">
								<?php
								$category_ids_array = explode('|', $gallery_info_without_pics->categories_of_gallery_ids);
								foreach ($all_categories as $category) {
									?>
									<option <?php if(in_array($category['category_id'], $category_ids_array)){ echo 'selected'; } ?> value="<?php echo $category['category_id']; ?>"><?php echo $category['category_title']; ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="form-group">
						<button type="submit" class="btn btn-default">Save</button>
						</div>
					</form>

				</div>
				
			</article>

				
				<!-- END COMMENTS -->
				<div class="gap"></div>


			</div>

			<div class="col-md-3">
				<aside class="sidebar-right hidden-phone">

					<?php
				// var_dump($image_info);
					?>

				</aside>


			</div>

		</div>

	</div>
	<?php $this->load->view('includes/footer'); ?>
	<script type="text/javascript">
		add_view_to_picture(<?php echo $image_info->picture_id; ?>);
	</script>