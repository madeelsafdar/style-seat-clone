<!-- Footer Section Start -->
        <div class="footer--section">
            <!-- Footer Widgets Start -->
            <div class="footer--widgets">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-xs-6 col-xxs-12">
                            <!-- Footer Widget Start -->
                            <div class="footer--widget">
                                <!-- Widget Logo Start -->
                                <div class="widget--logo">
                                    <a href="index.html">
                                        <img src="img/footer-img/logo.png" alt="SMM24 Logo">
                                    </a>
                                </div>
                                <!-- Widget Logo End -->

                                <!-- About Widget Start -->
                                <div class="about--widget">
                                    <div class="body">
                                        <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malo versions from the 1914 translation.</p>
                                    </div>

                                    <div class="footer">
                                        <a href="about.html">Read More<i class="fa fa-angle-double-right"></i></a>
                                    </div>

                                    <ul class="social nav">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                    </ul>
                                </div>
                                <!-- About Widget End -->
                            </div>
                            <!-- Footer Widget End -->
                        </div>

                        <div class="col-md-3 col-xs-6 col-xxs-12">
                            <!-- Footer Widget Start -->
                            <div class="footer--widget">
                                <!-- Widget Title Start -->
                                <div class="widget--title">
                                    <h2 class="h4">Recent Blog or Latest News</h2>
                                </div>
                                <!-- Widget Title End -->

                                <!-- Recent Posts Widget Start -->
                                <div class="recent-posts--widget">
                                    <ul>
                                        <li>
                                            <div class="title">
                                                <p><a href="blog-details.html">There are many variations of passages of Lorem Ipsum available, but the major.</a></p>
                                            </div>

                                            <div class="date">
                                                <p><a href="#" class="active">19 January 2017</a></p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                            <div class="title">
                                                <p><a href="blog-details.html">There are many variations of passages of Lorem Ipsum available, but the major.</a></p>
                                            </div>

                                            <div class="date">
                                                <p><a href="#" class="active">19 January 2017</a></p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- Recent Posts Widget End -->
                            </div>
                            <!-- Footer Widget End -->
                        </div>

                        <div class="col-md-3 col-xs-6 col-xxs-12">
                            <!-- Footer Widget Start -->
                            <div class="footer--widget">
                                <!-- Widget Title Start -->
                                <div class="widget--title">
                                    <h2 class="h4">Usefull Links</h2>
                                </div>
                                <!-- Widget Title End -->

                                <!-- Links Widget Start -->
                                <div class="links--widget">
                                    <ul class="nav">
                                        <li><a href="#">Help Center</a></li>
                                        <li><a href="#">My Account</a></li>
                                        <li><a href="#">Awards &amp; Reviews</a></li>
                                        <li><a href="#">User Guide</a></li>
                                        <li><a href="#">Shopping Cart</a></li>
                                        <li><a href="#">Support Forum</a></li>
                                    </ul>
                                </div>
                                <!-- Links Widget End -->
                            </div>
                            <!-- Footer Widget End -->
                        </div>

                        <div class="col-md-3 col-xs-6 col-xxs-12">
                            <!-- Footer Widget Start -->
                            <div class="footer--widget">
                                <!-- Widget Title Start -->
                                <div class="widget--title">
                                    <h2 class="h4">Contact Information</h2>
                                </div>
                                <!-- Widget Title End -->

                                <!-- Contact Widget Start -->
                                <div class="contact--widget">
                                    <p>You can contact or visit us during working time, our contact info is below.</p>

                                    <ul class="nav">
                                        <li><span>Tel :</span> <a href="tel:+01234567894321">+0123 456 789 4321</a></li>
                                        <li><span>E-mail :</span> <a href="mailto:example@example.com">example@example.com</a></li>
                                        <li><span>Address :</span> 148 - Mirpur, Dhaka, Bangladesh</li>
                                        <li><span>Working Hour :</span> Mon - Sat 09 am - 10 pm</li>
                                    </ul>
                                </div>
                                <!-- Contact Widget End -->
                            </div>
                            <!-- Footer Widget End -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer Widgets End -->

            <!-- Footer Copyright Start -->
            <div class="footer--copyright">
                <div class="container">
                    <p class="float--left">Copyright &copy; by <a href="index.html">SMM24</a> 2017. All Rights Reserved.</p>

                    <ul class="nav float--right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Buy Theme</a></li>
                    </ul>
                </div>
            </div>
            <!-- Footer Copyright End -->
        </div>
        <!-- Footer Section End -->

        <!-- Back To Top Button Start -->
        <div class="back-to-top-btn">
            <a href="#" class="active"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- Back To Top Button End -->
    </div>
    <!-- Wrapper End -->

    <!-- ==== jQuery Library ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/jquery-3.2.1.min.js"></script>

    <!-- ==== Bootstrap Framework ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/bootstrap.min.js"></script>

    <!-- ==== Sticky Plugin ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/jquery.sticky.min.js"></script>

    <!-- ==== Owl Carousel Plugin ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/owl.carousel.min.js"></script>

    <!-- ==== Magnific Popup Plugin ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/jquery.magnific-popup.min.js"></script>

    <!-- ==== Isotope Plugin ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/isotope.min.js"></script>

    <!-- ==== Validation Plugin ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/jquery.validate.min.js"></script>

    <!-- ==== RetinaJS Plugin ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/retina.min.js"></script>

    <!-- ==== Main JavaScript ==== -->
    <script src="<?php echo base_url(); ?>public/themes/front/html-template/js/main.js"></script>

</body>
</html>
