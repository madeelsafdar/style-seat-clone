<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- ==== Document Title ==== -->
    <title>SMM24 - Multipurpose Social Media Management and Marketing HTML Template</title>
    
    <!-- ==== Document Meta ==== -->
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- ==== Favicons ==== -->
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="icon" href="favicon.png" type="image/x-icon">

    <!-- ==== Google Font ==== -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500%7CLora:400,700" rel="stylesheet">

    <!-- ==== Font Awesome ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- ==== Bootstrap Framework ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- ==== Owl Carousel Plugin ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/owl.carousel.min.css" rel="stylesheet">
    
    <!-- ==== Magnific Popup Plugin ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/magnific-popup.css" rel="stylesheet">
    
    <!-- ==== Main Stylesheet ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/style.css" rel="stylesheet">
    
    <!-- ==== Responsive Stylesheet ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/responsive-style.css" rel="stylesheet">

    <!-- ==== Theme Color Stylesheet ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/colors/color-1.css" rel="stylesheet" id="changeColorScheme">
    
    <!-- ==== Custom Stylesheet ==== -->
    <link href="<?php echo base_url(); ?>public/themes/front/html-template/css/custom.css" rel="stylesheet">

    <!-- ==== HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries ==== -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <!-- Preloader Start -->
    <!-- <div id="preloader">
        <div class="preloader--spinners">
            <div class="preloader--spinner-1"></div>
            <div class="preloader--spinner-2"></div>
        </div>
    </div> -->
    <!-- Preloader End -->

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Header Section Start -->
        <div class="header--section">
            <!-- Header Topbar Start -->
            <div class="header--topbar">
                <div class="container">
                    <!-- Logo Start -->
                    <div class="header--logo float--left">
                        <a href="index.html"><img src="img/logo.png" alt="SMM24 Logo"></a>
                    </div>
                    <!-- Logo End -->

                    <!-- Header Info Start -->
                    <div class="header--info float--right">
                        <ul class="nav">
                            <li class="hidden-xxs">
                                <strong>Call Us</strong>
                                <a href="tel:+012345678910"><i class="fa fa-phone-square"></i>+0123 456 789 10</a>
                            </li>

                            <li>
                                <strong>E-Mail Us</strong>
                                <a href="mailto:example@example.com"><i class="fa fa-envelope-o"></i>you.yours@yourmail.com</a>
                            </li>

                            <li class="hidden-xs">
                                <strong>Follow Us</strong>
                                <ul class="nav header--social">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- Header Info End -->
                </div>
            </div>
            <!-- Header Topbar End -->

            <!-- Header Navbar Start -->
            <nav class="header--navbar navbar" data-trigger="sticky">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#headerNav" aria-expanded="false" aria-controls="headerNav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div id="headerNav" class="navbar-collapse collapse float--left">
                        <!-- Header Nav Links Start -->
                        <ul class="header--nav-links nav navbar-nav">
                            <li class="active"><a href="index.html">Home</a></li>
                            <li><a href="about.html">About</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<i class="fa fa-angle-down"></i></a>

                                <ul class="dropdown-menu">
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="service-details.html">Service Details</a></li>
                                </ul>
                            </li>
                            <li class="dropdown megamenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages<i class="fa fa-angle-down"></i></a>

                                <div class="dropdown-menu" data-bg-img="img/header-img/megamenu-bg.png">
                                    <ul class="nav col-md-2 col-sm-3">
                                        <li class="title"><span>Pages</span></li>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="team.html">Team</a></li>
                                        <li><a href="testimonials.html">Testimonials</a></li>
                                        <li><a href="pricing.html">Pricing</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                    </ul>
                                    
                                    <ul class="nav col-md-2 col-sm-3">
                                        <li class="title"><span>Services</span></li>
                                        <li><a href="services.html">Services</a></li>
                                        <li><a href="service-details.html">Service Details</a></li>
                                        <li><a href="case-studies.html">Case Studies</a></li>
                                        <li><a href="case-study-details.html">Case Study Details</a></li>
                                        <li><a href="coming-soon.html">Coming Soon</a></li>
                                    </ul>

                                    <ul class="nav col-md-2 col-sm-3">
                                        <li class="title"><span>Shop</span></li>
                                        <li><a href="shop.html">Shop</a></li>
                                        <li><a href="shop-details.html">Shop Details</a></li>
                                        <li><a href="cart.html">Cart</a></li>
                                        <li><a href="checkout.html">Checkout</a></li>
                                        <li><a href="404.html">404</a></li>
                                    </ul>

                                    <ul class="nav col-md-2 col-sm-3">
                                        <li class="title"><span>Blog</span></li>
                                        <li><a href="blog-sidebar-left.html">Blog Sidebar Left</a></li>
                                        <li><a href="blog-sidebar-right.html">Blog Sidebar Right</a></li>
                                        <li><a href="blog-details.html">Blog Details</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shop<i class="fa fa-angle-down"></i></a>

                                <ul class="dropdown-menu">
                                    <li><a href="shop.html">Shop</a></li>
                                    <li><a href="shop-details.html">Shop Details</a></li>
                                    <li><a href="cart.html">Cart</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog<i class="fa fa-angle-down"></i></a>

                                <ul class="dropdown-menu">
                                    <li><a href="blog-sidebar-left.html">Blog Sidebar Left</a></li>
                                    <li><a href="blog-sidebar-right.html">Blog Sidebar Right</a></li>
                                    <li><a href="blog-details.html">Blog Details</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                        <!-- Header Nav Links End -->
                    </div>

                    <!-- Header Buttons Start -->
                    <div class="header--buttons float--right">
                        <a href="cart.html"><i class="fa fa-shopping-basket"></i>(<span>3</span>)</a>
                        <a href="#" class="btn btn-default active">Get A Free Quote</a>
                    </div>
                    <!-- Header Buttons End -->
                </div>
            </nav>
            <!-- Header Navbar End -->
        </div>
        <!-- Header Section End -->