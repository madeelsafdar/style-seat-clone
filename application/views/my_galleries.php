<?php $this->load->view('includes/header'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row row-wrap">

				<div class="col-md-12">
					<h2>My Galleries</h2>
					<hr style="border: 1px solid #bcbaba;">
				</div>

				<?php
				foreach ($all_galleries_info as $key => $gallery_info) {

					$images_comma_seperated = $gallery_info['images_comma_seperated'];
					$images_info_array = explode('|', $images_comma_seperated);
					$images_count = count($images_info_array);
					$is_image = 0;

					$human_readable_date =  date("F jS, Y",strtotime($gallery_info['created_at'])); 

					foreach ($images_info_array as $key => $images_info) {

						$images_info_array = explode(',', $images_info);
						$gallery_id = $gallery_info['gallery_id'];

						if($images_info_array[0] != ''){
							$picture_file_name = $images_info_array[0];
							$picture_title = $images_info_array[1];
							$picture_id = $images_info_array[2];
						}else{
							$picture_file_name = '';
							$picture_title = '';
							$picture_id = '';
						}

						

						$filepath = base_url().'public/pictures_uploaded/gallery_'.$gallery_info['gallery_id'].'/'.$picture_file_name;
						
						if($gallery_info['gallery_views'] == ''){
							$total_gallery_views = 0;
						}else{
							$total_gallery_views = $gallery_info['gallery_views'];
						}

						if(@getimagesize($filepath)){
							?>
							<div class="col-md-4">
								<h5><?php echo $gallery_info['gallery_title']; ?></h5>
								<a class="hover-img" href="<?php echo base_url('gallery/view_gallery').'/'.$gallery_info['gallery_id']; ?>">
									<img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $gallery_info['gallery_id']; ?>/<?php echo $picture_file_name; ?>" alt="Image Alternative text" title="Featured Image">
									<h3 class="hover-title">
										<?php echo $images_count; ?> images - <?php echo $human_readable_date; ?> - <?php echo $total_gallery_views; ?> views
									</h3>
								</a>
							</div>
							<?php
							$is_image = 1;
							break;
						}
					}

					if($is_image == 0){
						?>
						<div class="col-md-4">
							<h5><?php echo $gallery_info['gallery_title']; ?></h5>
							<a class="hover-img" href="<?php echo base_url('gallery/view_gallery').'/'.$gallery_info['gallery_id']; ?>">
								<img src="https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600" alt="Image Alternative text" title="Featured Image">
								<h3 class="hover-title">
									<?php echo $images_count; ?> images - <?php echo $human_readable_date; ?> - <?php echo $total_gallery_views; ?> views
								</h3>
							</a>
						</div>
						<?php
					}

				}
				?>








			</div>
			<div class="gap gap-small"></div>
		</div>
	</div>

</div>
<?php $this->load->view('includes/footer'); ?>