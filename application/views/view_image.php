<?php $this->load->view('includes/header'); ?>
<?php
function humanTiming ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
    	31536000 => 'year',
    	2592000 => 'month',
    	604800 => 'week',
    	86400 => 'day',
    	3600 => 'hour',
    	60 => 'minute',
    	1 => 'second'
    	);

    foreach ($tokens as $unit => $text) {
    	if ($time < $unit) continue;
    	$numberOfUnits = floor($time / $unit);
    	return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }

}
?>
<div class="container">
	<div class="row">

		

		<div class="col-md-9">


			
			<article class="post">

				

				<header class="post-header">

					<div class="col-md-12">
						<div id="left_image_btn" style="text-align: center;height: 0;position: absolute;left:0px;">
							<?php
							if(!empty($prev_image_id)){
								?>
								<a class="btn btn-small btn-primary" href="<?php echo base_url('gallery/view_image').'/'.$prev_image_id->picture_id; ?>" style="border-radius: 0;">
									<i class="fa fa-chevron-left" style="color: #FFF;" aria-hidden="true"></i>
								</a>
								<?php
							}
							?>
						</div>

						<div id="right_image_btn" style="text-align: center;height: 0;position: absolute;right:0px;">

							<?php
							if(!empty($next_image_id)){
								?>
								<a class="btn btn-small btn-primary" href="<?php echo base_url('gallery/view_image').'/'.$next_image_id->picture_id; ?>" style="border-radius: 0;">
									<i class="fa fa-chevron-right" style="color: #FFF;" aria-hidden="true"></i>
								</a>
								<?php
							}
							?>
						</div>
					</div>

					

					<img id="single_image" src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $image_info->gallery_id; ?>/<?php echo $image_info->file_name; ?>" alt="Image Alternative text" title="4 Strokes of Fun">


				</img>





			</header>
			<div class="post-inner">
				<h4 class="post-title"><?php echo $image_info->picture_title; ?></h4>
				<ul class="post-meta">
					<li style="margin-right:0px;">
						<a class="btn btn-small btn-primary" onclick="vote_up_picture(<?php echo $image_info->picture_id; ?>)" href="javascript:void(0);">
							<i class="fa fa-arrow-up" style="color: #FFF;" aria-hidden="true"></i>
						</a>
					</li>
					<li style="margin-right:0px;">
						<a class="btn btn-small btn-primary" onclick="vote_down_picture(<?php echo $image_info->picture_id; ?>)" href="javascript:void(0);">
							<i class="fa fa-arrow-down" style="color: #FFF;" aria-hidden="true"></i>
						</a>
					</li>
					<li style="margin-right:0px;">
						<a class="btn btn-small btn-primary" onclick="toggle_favroute(<?php echo $image_info->picture_id; ?>)" href="javascript:void(0);">
							<i class="fa fa-heart" style="color: #FFF;" aria-hidden="true"></i>
						</a>
					</li>
					<?php
					$user = $this->ion_auth->user()->row();
					if($user && $user->id == $image_info->user_id){
						?>
						<li style="margin-right:0px;">
							<a class="btn btn-small btn-primary" href="<?php echo base_url('images/edit_image_details').'/'.$image_info->picture_id; ?>">
								<i class="fa fa-pencil" style="color: #FFF;" aria-hidden="true"></i>
							</a>
						</li>
						<?php
					}
					?>
					<li style="margin-right:0px;height: 25px;">
						<div class="col-md-12" style="padding: 0;">
							<label style="padding: 0;margin: 0;">Points:</label> <?php echo $image_info->vote_up; ?>
						</div>
						<div class="col-md-12" style="padding: 0;">
							<label style="padding: 0;margin: 0;">Views:</label> <span class="vote_count"><?php echo $image_info->views; ?></span>
						</div>
					</li>
					<li style="margin-right:0px;height: 25px;width: 100%;margin-bottom: 5px;">
					<div class="col-md-12" style="padding: 0;">
						<div id="share"></div>
						</div>
					</li>
				</ul>
				<p class="post-desciption"><?php echo $image_info->discription; ?></p>
			</div>
		</article>

		<?php
		$user = $this->ion_auth->user()->row();
		if($user){
			?>
			<h2>Leave a Comment</h2>
			<form method="post" action="<?php echo base_url('comments/save'); ?>">

				<input type="hidden" name="picture_id" value="<?php echo $image_info->picture_id; ?>">

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Comment</label>
							<textarea name="comment_text" class="form-control" placeholder="Your Comment Here"></textarea>
						</div>
					</div>
				</div>
				<input type="submit" name="" value="Post a Comment" class="btn btn-primary">
			</form>
			<div class="gap"></div>
			<h2 class="mb20">Comments</h2>
			<!-- START COMMENTS -->
			<ul class="comments-list">
				<li>
					<!-- COMMENT -->

					<?php
					foreach ($all_picture_comments as $key => $comment) {
						$time = strtotime($comment['comment_creation_datetime']);
						?>
						<article class="comment">
							<div class="comment-inner">
								<span class="comment-author-name"><?php
									if($comment['first_name'] == '' && $comment['last_name'] == ''){
										echo $comment['username'];
									}else{
										echo $comment['first_name'].' '.$comment['last_name'];
									}
									?></span>
									<p class="comment-content"><?php echo $comment['comment_text']; ?></p>
									<span class="comment-time"><?php echo humanTiming($time); ?> ago</span>
								</div>
							</article>
							<?php
						}
						?>


					</li>
				</ul>
				<?php
			}
			?>


			<!-- END COMMENTS -->
			<div class="gap"></div>


		</div>

		<div class="col-md-3">
			<aside class="sidebar-right hidden-phone" style="text-align: center;">
			<h4>More images in this category</h4>
			<?php
			// var_dump($similar_categories_galleries);
			?>
			<hr style="margin: 0px;border: 1px solid;width: 100%;padding: 0px;margin-bottom:5px;">
				<?php
				$count = 0;
				$is_image = 0;
				if($similar_categories_galleries != '' && $similar_categories_galleries != 'NULL'){
					foreach ($similar_categories_galleries as $key => $gallery) {
						
						if($gallery != ""){

							if($count == 0){
								?>
								<div class="col-md-12">
									<div class="product-thumb">
										<header class="product-header">
											<img onerror="this.src='https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600'" src="<?php echo base_url(); ?>public/thumbnails_uploaded/gallery_<?php echo $gallery['gallery_id']; ?>/<?php echo $gallery['file_name']; ?>" alt="Image Alternative text" title="Our Coffee miss u">
										</header>
										<div class="product-inner">

											<h5 class="product-title"><?php echo $gallery['picture_title']; ?></h5>
										</div>
									</div>
								</div>
								<?php

							}else{
								?>
								<div class="col-md-12" style="margin-top:10px;">
									<div class="product-thumb">
										<header class="product-header">
											<img onerror="this.src='https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600'" src="<?php echo base_url(); ?>public/thumbnails_uploaded/gallery_<?php echo $gallery['gallery_id']; ?>/<?php echo $gallery['file_name']; ?>" alt="Image Alternative text" title="Our Coffee miss u">
										</header>
										<div class="product-inner">

											<h5 class="product-title"><?php echo $gallery['picture_title']; ?></h5>
										</div>
									</div>
								</div>
								<?php
							}
							$is_image++;
						}else{
							$is_image = 0;
						}
						
						$count++;
					}
				}

				if($is_image == 0){
					echo "No images are available currently";
				}
				
				?>

			</aside>


		</div>

	</div>

</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
	add_view_to_picture(<?php echo $image_info->picture_id; ?>);
</script>
<script type="text/javascript">
	var img_height = $("#single_image").height();
	var top_pixels = img_height/2;
	$("#left_image_btn").css("top",top_pixels);
	$("#right_image_btn").css("top",top_pixels);
</script>
<script type="text/javascript">
	$("#share").jsSocials({
    shares: ["twitter", { share: "facebook", label: "Share" }, "whatsapp"],
    url: '<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $image_info->gallery_id; ?>/<?php echo $image_info->file_name; ?>',
    text: '<?php echo $image_info->picture_title; ?>',
    shareIn : "popup",
    showCount: false
});
</script>