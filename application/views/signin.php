<?php $this->load->view('includes/header'); ?>
<?php
$is_validation = false;
@session_start(); 
if(isset($_SESSION['values']) && isset($_SESSION['errors'])){ 
   $values = $_SESSION['values']; 
   $errors = $_SESSION['errors'];

   unset($_SESSION['values']); 
   unset($_SESSION['errors']); 
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding: 0;">
            <!-- Messages -->
            <?php if($this->session->flashdata('success_message')){ ?>
            <div class="alert alert-success" id="success_message">
                <p>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </p>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('failure_message')){ ?>
            <div class="alert alert-danger alert-error" id="failure_message">
                <p>
                <?php echo $this->session->flashdata('failure_message'); ?>
                </p>
            </div>
            <?php } ?>
            <!-- Messages -->
        </div>
    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-md-12 box">

            <h3>Sign In</h3>

            <hr>

            <form class="dialog-form" action="<?php echo base_url('users/signin_process'); ?>" method="post" id="" <?php if ($is_validation) { echo "data-parsley-validate"; } ?>>
            <div class="form-group">
                <label>E-mail</label>
                <input type="text" placeholder="Email" name="email" class="form-control" <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Email is required" data-parsley-trigger="change" required';
                        }
                        ?>
                         value="<?php
                        if(isset($values['email']) && $values['email'] != ''){
                           echo $values['email'];
                        }
                        ?>"
                        >
                         <?php
                        if(isset($errors['email']) && $errors['email'] != ''){
                           echo $errors['email'];
                        }
                        ?>
            </div>

            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" placeholder="My secret password" class="form-control" <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Password is required" data-parsley-equalto-message="Password and Repeat Password should be same" data-parsley-equalto="#conf_password" required';
                        }
                        ?>
                        value="<?php
                        if(isset($values['password']) && $values['password'] != ''){
                           echo $values['password'];
                        }
                        ?>"
                        >
                         <?php
                        if(isset($errors['password']) && $errors['password'] != ''){
                           echo $errors['password'];
                        }
                        ?>
            </div>
            <input type="submit" value="Sign In" name="normal_signin" class="btn btn-primary">
        </form>
        </div>
    </div>
    <div class="gap"></div>

</div>

<?php $this->load->view('includes/footer'); ?>