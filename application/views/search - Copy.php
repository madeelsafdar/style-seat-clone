<?php $this->load->view('includes/header'); ?>
<style type="text/css">

.col-masonry{
     position: inherit !important;
     left: 0 !important;
     top: 0 !important;
   }

  .home_gallery{
    height: auto !important;
  }

@media screen and (min-width: 992px) {
   .col-md-2.grid_element_custom {
    min-height: 171px;
}
}

@media screen and (max-width: 992px) {
   .grid_element_custom{
     position: inherit !important;
     left: 0 !important;
     top: 0 !important;
   }
 }
</style>
<div class="container">
  <div class="text-center">
    <!-- <h2 class="mb30">New Deals in Your City</h2> -->

    <article class="post">
      <div class="post-inner">

        <div style="position: relative; min-height: 1176.45px;" class="row row-wrap home_gallery" id="masonry">

                    <!-- <div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry">
                        <div class="product-thumb" style="">
                            <header class="product-header">
                                <img src="<?php //echo base_url(); ?>public/front/couponia_theme/img/800x600.png" alt="Image Alternative text" title="Our Coffee miss u">
                            </header>
                            <div class="product-inner" style="padding: 6px;">
                                <div class="product-meta" style="margin: 0px;">
                                    <ul class="product-price-list">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <p class="product-location" style="margin: 5px;">100 Points</p>
                            </div>
                        </div>
                      </div> -->



                    </div>  

                  </div>
                </article>

                <a href="javascript:void(0);" onclick="load_more()" class="btn btn-primary btn-ghost">Load More</a>
              </div>
              <div class="gap"></div>
            </div>
            <?php $this->load->view('includes/footer'); ?>

            <script type="text/javascript">
              var loaded_result = 0;
              var items_per_load = '<?php echo $GLOBALS["all_settings"]["items_to_show"]; ?>';
              var logged_in = '<?php echo $this->ion_auth->logged_in(); ?>';

              $(document).ready(function(){
      // url = '<?php echo base_url("images/get_all_images_by_category_ajax"); ?>';
      url = '<?php echo base_url("gallery/get_all_galleries_by_search_ajax"); ?>';

      var data_to_send = {
        loaded_result: loaded_result,
        items_per_load: items_per_load,
        search_str: '<?php echo $search_str; ?>'
      };

      $.ajax({
        url : url,
        type: "POST",
        async: false,
        dataType: "json",
        data : data_to_send,
        success: function(data, textStatus, jqXHR)
        {

// to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header">'+'<?php echo $GLOBALS["all_settings"]["home_page_advertisement"]; ?>'+'</header></div></div>';

// $(".home_gallery").append(to_append);

$(data).each(function(index, el) {
            // filepath = '<?php echo base_url(); ?>'+'public/pictures_uploaded/gallery_'+el.gallery_id+'/'+el.file_name;
            filepath = '<?php echo base_url(); ?>'+'public/thumbnails_uploaded/gallery_'+el.gallery_id+'/'+el.file_name;

             if(el.no_of_pictures == 1){
      images_no_str = el.no_of_pictures+' Image';
      click_link = '<?php echo base_url("gallery/view_image"); ?>'+'/'+el.picture_id;
    }else{
      images_no_str = el.no_of_pictures+' Images';
      click_link = '<?php echo base_url("gallery/view_gallery"); ?>'+'/'+el.gallery_id;
    }

            if(logged_in){
              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_gallery('+el.gallery_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_gallery('+el.gallery_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

               // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_gallery('+el.gallery_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_gallery('+el.gallery_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';



    to_append = '<div class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="'+click_link+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"><div style="background: rgba(213, 213, 213, 0.57) none repeat scroll 0% 0%; width: 100%; position: absolute; left: 0px; text-align: center; color: rgb(0, 0, 0); top: 42%;">'+images_no_str+'</div></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_gallery('+el.gallery_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_gallery('+el.gallery_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';
            }else{
              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';
               to_append = '<div class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="'+click_link+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"><div style="background: rgba(213, 213, 213, 0.57) none repeat scroll 0% 0%; width: 100%; position: absolute; left: 0px; text-align: center; color: rgb(0, 0, 0); top: 42%;">'+images_no_str+'</div></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';
            }

            // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_image/'+el.picture_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_picture('+el.picture_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_picture('+el.picture_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

            $(".home_gallery").append(to_append);
          });

loaded_result = items_per_load;

},
error: function (jqXHR, textStatus, errorThrown)
{  
}
});
});

</script>

<script type="text/javascript">
  function load_more () {
     // url = '<?php echo base_url("images/get_all_images_by_category_ajax"); ?>';
     url = '<?php echo base_url("gallery/get_all_galleries_by_search_ajax"); ?>';

     var data_to_send = {
      loaded_result: loaded_result,
      items_per_load: items_per_load,
      search_str: '<?php echo $search_str; ?>'
    };

    $.ajax({
      url : url,
      type: "POST",
      async: false,
      dataType: "json",
      data : data_to_send,
      success: function(data, textStatus, jqXHR)
      {

        to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header">'+'<?php echo $GLOBALS["all_settings"]["home_page_advertisement"]; ?>'+'</header></div></div>';

        var el = $(to_append);
        $("#masonry").append(el).masonry( 'appended', el, false ).masonry('reloadItems');

        $(data).each(function(index, el) {
            // filepath = '<?php echo base_url(); ?>'+'public/pictures_uploaded/gallery_'+el.gallery_id+'/'+el.file_name;
            filepath = '<?php echo base_url(); ?>'+'public/thumbnails_uploaded/gallery_'+el.gallery_id+'/'+el.file_name;

            if(el.no_of_pictures == 1){
      images_no_str = el.no_of_pictures+' Image';
      click_link = '<?php echo base_url("gallery/view_image"); ?>'+'/'+el.picture_id;
    }else{
      images_no_str = el.no_of_pictures+' Images';
      click_link = '<?php echo base_url("gallery/view_gallery"); ?>'+'/'+el.gallery_id;
    }

            if(logged_in){
              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_gallery('+el.gallery_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_gallery('+el.gallery_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_gallery('+el.gallery_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_gallery('+el.gallery_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

               to_append = '<div class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="'+click_link+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"><div style="background: rgba(213, 213, 213, 0.57) none repeat scroll 0% 0%; width: 100%; position: absolute; left: 0px; text-align: center; color: rgb(0, 0, 0); top: 42%;">'+images_no_str+'</div></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a onclick="vote_up_gallery('+el.gallery_id+')" href="javascript:void(0);"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0); onclick="vote_down_gallery('+el.gallery_id+')""><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

            }else{
              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

              // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_gallery/'+el.gallery_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

              to_append = '<div class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="'+click_link+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"><div style="background: rgba(213, 213, 213, 0.57) none repeat scroll 0% 0%; width: 100%; position: absolute; left: 0px; text-align: center; color: rgb(0, 0, 0); top: 42%;">'+images_no_str+'</div></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';
            }

            // to_append = '<div style="position: absolute; left: 0px; top: 0px;" class="col-md-2 col-masonry"><div class="product-thumb" style=""><header class="product-header"><a href="<?php echo base_url(); ?>gallery/view_image/'+el.picture_id+'"><img onerror="this.src=\'https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600\'" style="min-width: 158.333px;min-height: 118.75px;" src="'+filepath+'" alt="Image Alternative text" title="Our Coffee miss u"></a></header><div class="product-inner" style="padding: 6px;"><div class="product-meta" style="margin: 0px;"><ul class="product-price-list"><li><a href="javascript:void(0);" onclick="vote_up_picture('+el.picture_id+')"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></li><li><a href="javascript:void(0);" onclick="vote_down_picture('+el.picture_id+')"><i class="fa fa-arrow-down" aria-hidden="true"></i></a></li></ul></div><p class="product-location" style="margin: 5px;"><span class="vote_count">'+el.vote_up+'</span> Points</p></div></div></div>';

            var el = $(to_append);
            $("#masonry").append(el).masonry( 'appended', el, false ).masonry('reloadItems');


          });

loaded_result = loaded_result + items_per_load;

},
error: function (jqXHR, textStatus, errorThrown)
{  
}
});
}
</script>