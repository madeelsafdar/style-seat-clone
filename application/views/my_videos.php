<?php $this->load->view('includes/header'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row row-wrap">

				<div class="col-md-12" style="margin:0;">
					
					<div class="col-md-12" style="paddng:0;">
						<h2>My Videos</h2>
					</div>

					<div class="col-md-12" style="padding:0;">
						<hr style="border: 1px solid #bcbaba;margin-top: 5px;margin-bottom: 10px;">
					</div>
					
				</div>

				

				<?php
				foreach ($all_my_videos as $key => $my_video) {

					$embed_str = preg_replace(array('/width="\d+"/i', '/height="\d+"/i'),array('','style="width: 100%;height: 180px;"'),$my_video['embed_code']);

					if (strpos($embed_str, 'instagram') !== false) {
					// if (filter_var($embed_str, FILTER_VALIDATE_URL)) { 
						$embed_str = strip_tags($embed_str);
						?>
						<!-- <div class="col-md-4">

							<input  type="hidden" name="url_<?php //echo $count; ?>" id="url_<?php //echo $count; ?>" value="https://api.instagram.com/oembed?url=<?php //echo $embed_str; ?>">

						</div> -->
						<?php
						$embed_str = str_replace('http://', '', $embed_str);
						$embed_str = str_replace('https://', '', $embed_str);
						$embed_str = str_replace('www.', '', $embed_str);
						$embed_str = str_replace(' ', '', $embed_str);
						$embed_str=preg_replace('/\s+/', '', $embed_str);
						$url='https://api.instagram.com/oembed?url='.$embed_str;
						// var_dump($url);
						$json = file_get_contents($url);
						$obj = json_decode($json);
						?>
						<div class="col-md-4">
							<?php
							echo $obj->html;
							?>
						</div>
						<?php
						$count++;
					}else{
						?>
						<div class="col-md-4">
							<?php 
							echo $embed_str;
							?>
						</div>
						<?php
					}

				}
				?>

				
			</div>
			<div class="gap gap-small"></div>
		</div>
	</div>

</div>
<?php $this->load->view('includes/footer'); ?>