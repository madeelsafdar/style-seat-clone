<?php $this->load->view('includes/header'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row row-wrap">

				<div class="col-md-12" style="margin:0;">
					
					<div class="col-md-10" style="paddng:0;">
						<h2><?php echo $gallery_info->gallery_title; ?></h2>
					</div>
					<div class="col-md-2" style="paddng:0;">
						<?php
						$user = $this->ion_auth->user()->row();
						if($user && $user->id == $gallery_info->user_id){
							?>
							<a href="<?php echo base_url('gallery/edit_gallery'); ?>" class="btn btn-block btn-primary search-btn" >Edit Gallery</a>
							<?php
						}
						?>
					</div>

					<div class="col-md-12" style="padding:0;">
						<hr style="border: 1px solid #bcbaba;margin-top: 5px;margin-bottom: 10px;">
					</div>
					
				</div>

				

				<?php
				$images_comma_seperated = $gallery_info->images_comma_seperated;

				$images_info_array = explode('|', $images_comma_seperated);
				foreach ($images_info_array as $key => $images_info) {
					$images_info_array = explode(',', $images_info);


					$picture_file_name = $images_info_array[0];
					$picture_title = $images_info_array[1];
					$picture_id = $images_info_array[2];

					?>
					<div class="col-md-4">
						<h5><?php echo $picture_title; ?></h5>
						<!-- HOVER IMAGE -->
						<a class="hover-img" href="<?php echo base_url('gallery/view_image').'/'.$picture_id; ?>">
							<img src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $gallery_info->gallery_id; ?>/<?php echo $picture_file_name; ?>" alt="Image Alternative text" title="Old No7">
							<!-- <div class="hover-title">Posuere per sit</div> -->
						</a>
					</div>
					<?php
				}
				?>

				
			</div>
			<div class="gap gap-small"></div>
		</div>
	</div>

</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
	add_view_to_gallery('<?php echo $gallery_info->gallery_id; ?>');
</script>