<?php $this->load->view('includes/header'); ?>
<?php
$is_validation = false;
@session_start(); 
if(isset($_SESSION['values']) && isset($_SESSION['errors'])){ 
	$values = $_SESSION['values']; 
	$errors = $_SESSION['errors'];

	unset($_SESSION['values']); 
	unset($_SESSION['errors']); 
}
?>

<div class="container">
	<div class="row">
		<div class="col-md-12" style="padding: 0;">
			<!-- Messages -->
			<?php if($this->session->flashdata('success_message')){ ?>
			<div class="alert alert-success" id="success_message">
				<p>
					<?php echo $this->session->flashdata('success_message'); ?>
				</p>
			</div>
			<?php } ?>
			<?php if($this->session->flashdata('failure_message')){ ?>
			<div class="alert alert-danger alert-error" id="failure_message">
				<p>
					<?php echo $this->session->flashdata('failure_message'); ?>
				</p>
			</div>
			<?php } ?>
			<!-- Messages -->
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
		<div class="col-md-12 box">

			<h3>My Profile</h3>

			<hr>

			<?php
			$user = $this->ion_auth->user()->row();
			?>

			<form class="dialog-form" action="<?php echo base_url('users/save_profile'); ?>" method="post" id="" <?php if ($is_validation) { echo "data-parsley-validate"; } ?>>

				<div class="form-group">
					<label>First Name</label>
					<input type="text" placeholder="First Name" name="first_name" class="form-control" <?php
					if($is_validation){
						echo 'data-parsley-required-message="First Nam is required" data-parsley-trigger="change" required';
					}
					?>
					value="<?php
					if(isset($user) && $user != ''){
						echo $user->first_name;
					}
					?>"
					>
					<?php
					if(isset($errors['first_name']) && $errors['first_name'] != ''){
						echo $errors['first_name'];
					}
					?>
				</div>

				<div class="form-group">
					<label>Last Name</label>
					<input type="text" placeholder="Last Name" name="last_name" class="form-control" <?php
					if($is_validation){
						echo 'data-parsley-required-message="Last Name is required" data-parsley-trigger="change" required';
					}
					?>
					value="<?php
					if(isset($user) && $user != ''){
						echo $user->last_name;
					}
					?>"
					>
					<?php
					if(isset($errors['last_name']) && $errors['last_name'] != ''){
						echo $errors['last_name'];
					}
					?>
				</div>

				

				<?php
				$identifier = $user->identifier;
				if($identifier == ''){
					?>
					<div class="form-group">
						<label>E-mail</label>
						<input type="text" placeholder="Email" name="email" class="form-control" <?php
						if($is_validation){
							echo 'data-parsley-required-message="Email is required" data-parsley-trigger="change" required';
						}
						?>
						value="<?php
						if(isset($user) && $user != ''){
							echo $user->email;
						}
						?>"
						>
						<?php
						if(isset($errors['email']) && $errors['email'] != ''){
							echo $errors['email'];
						}
						?>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" placeholder="My secret password" class="form-control" <?php
						if($is_validation){
							echo 'data-parsley-required-message="Password is required" data-parsley-equalto-message="Password and Repeat Password should be same" data-parsley-equalto="#conf_password" required';
						}
						?>
						value=""
						>
						<?php
						if(isset($errors['password']) && $errors['password'] != ''){
							echo $errors['password'];
						}
						?>
					</div>
					<div class="form-group">
						<label>Repeat Password</label>
						<input type="password" name="conf_password" id="conf_password" placeholder="Type your password again" class="form-control" <?php
						if($is_validation){
							echo 'data-parsley-required-message="Confirm Password is required" required';
						}
						?>
						>
						<?php
						if(isset($errors['conf_password']) && $errors['conf_password'] != ''){
							echo $errors['conf_password'];
						}
						?>
					</div>
					<?php
				}
				?>

				
				<input type="submit" value="Save" name="normal_register" class="btn btn-primary">
			</form>
		</div>
	</div>
	<div class="gap"></div>

</div>

<?php $this->load->view('includes/footer'); ?>