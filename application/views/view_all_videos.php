<?php $this->load->view('includes/header'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row row-wrap">

				<div class="col-md-12" style="margin:0;">
					
					<div class="col-md-12" style="padding:0;">
						<h2>Videos</h2>
					</div>

					<div class="col-md-12" style="padding:0;">
						<hr style="border: 1px solid #bcbaba;margin-top: 5px;margin-bottom: 10px;">
					</div>
					
				</div>

			</div>

			<style type="text/css">
				.iframe_cont {
					padding: 15px;
					width: 100%;
				}
			</style>

			<div class="row row-wrap">

				<?php
				$instagram_count=0;
				foreach ($all_videos as $key => $video) {

					$embed_str = preg_replace(array('/width="\d+"/i', '/height="\d+"/i'),array('','style="width: 100%;height: 180px;"'),$video['embed_code']);

					if (strpos($embed_str, 'instagram') !== false) {
						$embed_str = strip_tags($embed_str);
						$embed_str = str_replace('http://', '', $embed_str);
						$embed_str = str_replace('https://', '', $embed_str);
						$embed_str = str_replace('www.', '', $embed_str);
						$embed_str = str_replace(' ', '', $embed_str);
						$embed_str=preg_replace('/\s+/', '', $embed_str);
						$url='https://api.instagram.com/oembed?url='.$embed_str;
						$json = file_get_contents($url);
						$obj = json_decode($json);
						?>
						<div class="col-md-4" style="padding-left:0;padding-right:0;">
							<div class="iframe_cont">
								<?php
								echo $obj->html;
								?>
							</div>
						</div>
						<?php
						$instagram_count++;
					}else{
						?>
						<div class="col-md-4">

							<?php 
							echo $embed_str;
							?>
							
						</div>
						<?php
					}

				}
				?>
				
				
				<div class="gap gap-small"></div>
			</div>
		</div>

	</div>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
	// $(".instagram-media-registered").find('p').hide();
</script>