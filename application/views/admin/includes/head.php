<!-- Dashboard Styles Start -->
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->

 



<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css" />
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/global/css/plugins.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url(); ?>public/admin/metronic/theme/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME STYLES -->

<link href="<?php echo base_url(); ?>public/admin/data_tables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/admin/css/style_custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>public/combined/bootstrap-select/dist/css/bootstrap-select.css">


<link href="<?php echo base_url(); ?>public/combined/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/front/netstudio/images/favicon.png?v=2" />
<!-- Dashboard Styles End -->