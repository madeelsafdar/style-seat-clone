<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper" style="margin-bottom: 15px;">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            
            <?php
            $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            ?>

            <li class="start <?php if (strpos($url,'dashboard') !== false) { echo 'active open'; } ?>">
                <a href="<?php echo base_url('admin'); ?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>

            <?php
            // if ($this->acl->isAllow('groups','index') || $this->acl->isAllow('users','index')) {
            ?>
            <!-- <li>
                <a href="javascript:;">
                    <i class="fa fa-users"></i>
                    <span class="title">Accounts</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu"> -->
                    <?php
                        // if ($this->acl->isAllow('groups','index')) {
                    ?>
                    <!-- <li>
                        <a href="<?php //echo base_url('admin/groups'); ?>">
                            <i class="fa fa-users"></i>
                            Groups</a>
                        </li> -->
                        <?php
                            // }
                        ?>

                        <?php
                            // if ($this->acl->isAllow('users','index')) {
                        ?>
                        <!-- <li>
                            <a href="<?php //echo base_url('admin/users'); ?>">
                                <i class="fa fa-user"></i>
                                Users</a>
                            </li> -->
                            <?php
                                // }
                            ?>
                       <!--  </ul>
                   </li> -->
                   <?php
                    // }
                   ?>

                   <li class="<?php if (strpos($url,'users') !== false || strpos($url,'groups') !== false) { echo 'active open'; } ?>">
                    <a href="javascript:;">
                        <i class="fa fa-users"></i>
                        <span class="title">Accounts</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="<?php if (strpos($url,'users') !== false) { echo 'active open'; } ?>">
                            <a href="<?php echo base_url('admin/users'); ?>">
                                <i class="fa fa-users"></i>
                                Users</a>
                            </li>
                            <li class="<?php if (strpos($url,'groups') !== false) { echo 'active open'; } ?>">
                                <a href="<?php echo base_url('admin/groups'); ?>">
                                    <i class="fa fa-users"></i>
                                    Groups</a>
                                </li>
                            </ul>
                        </li>

                        <?php 
                        $CI =& get_instance();
                        $settings=$CI->db->query('select * from settings where parent_id=0');
                        $settings=$settings->result_array();
                        ?>

                        <li class="<?php if (strpos($url,'settings') !== false || strpos($url,'email_template') !== false || strpos($url,'advertisements') !== false) { echo 'active open'; } ?>">
                            <a href="javascript:;">
                                <i class="fa fa-cog"></i>
                                <span class="title">Settings</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">

                                <?php  
                                foreach ($settings as $setting) {
                                    if($setting['setting_name'] == "home_page_advertisement" || $setting['setting_name'] == "advertisement"){

                                    }else{
?>
                                    <li class="<?php if (strpos($url,'settings/index/'.$setting['setting_id']) !== false ) { echo 'active open'; } ?>">
                                        <a href="<?php echo base_url('admin/settings/index').'/'.$setting['setting_id']; ?>">

                                            <?php 
                                            echo $setting['setting_alias'];
                                            ?></a>
                                        </li>
                                        <?php
                                    }
                                    

                                    }

                                    ?>

                                    <li class="<?php if (strpos($url,'advertisements') !== false ) { echo 'active open'; } ?>">
                                        <a href="<?php echo base_url('admin/advertisements'); ?>"><i class=""></i>Advertisements Settings</a>
                                        </li>

                                    <li class="<?php if (strpos($url,'email_template') !== false ) { echo 'active open'; } ?>">
                                        <a href="<?php echo base_url('admin/email_template'); ?>"><i class=""></i>Email Settings</a>
                                        </li>
                                    </ul>
                                </li>

                                

                                
                            </ul>
                <!-- END SIDEBAR MENU -->

                <!--  -->

                