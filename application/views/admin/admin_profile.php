
<?php $this->load->view('admin/includes/header'); ?>
<?php
$is_validation = true;

@session_start(); 
if(isset($_SESSION['values']) && isset($_SESSION['errors'])){ 
	$values = $_SESSION['values']; 
	$errors = $_SESSION['errors'];

	unset($_SESSION['values']); 
	unset($_SESSION['errors']); 
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed page-sidebar-closed-hide-logo page-container-bg-solid">

	<div class="page-container">

		<!-- BEGIN PAGE CONTENT-->


		<?php



		?>

		<form class="form-horizontal" role="form" action="<?php echo base_url(); ?>users/add_admin" method="POST" <?php if ($is_validation) { echo "data-parsley-validate"; } ?> enctype="multipart/form-data">
			<div class="row margin-top-20">
				<div class="col-md-4">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">

						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">

							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<div class="droparea">
									<img src="<?php echo base_url().'public/users_pictures/'.$admin_info->picture; ?>" class="img-responsive" id="file_preview" alt="">
								</div>
								<input type="file" name="file" id="file" accept="image/*" style="display: none;" >							
							</div>

							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<!-- <div class="profile-usertitle">
								<div class="profile-usertitle-name">
									 Marcus Doe
								</div>
								<div class="profile-usertitle-job">
									 Developer
								</div>
							</div> -->
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR BUTTONS -->
							<!-- <div class="profile-userbuttons">
								<button type="button" class="btn btn-circle green-haze btn-sm">Follow</button>
								<button type="button" class="btn btn-circle btn-danger btn-sm">Message</button>
							</div> -->
							<!-- END SIDEBAR BUTTONS -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
									<!-- <li class="active">
										<a href="extra_profile.html">
										<i class="icon-home"></i>
										Overview </a>
									</li> -->
									<!-- <li>
										<a href="extra_profile_account.html">
										<i class="icon-settings"></i>
										Account Settings </a>
									</li> -->
									<!-- <li>
										<a href="page_todo.html" target="_blank">
										<i class="icon-check"></i>
										Tasks </a>
									</li> -->
									<!-- <li>
										<a href="extra_profile_help.html">
										<i class="icon-info"></i>
										Help </a>
									</li> -->
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						<!-- PORTLET MAIN -->
						<!-- <div class="portlet light"> -->
						<!-- STAT -->
							<!-- <div class="row list-separated profile-stat">
								<div class="col-md-4 col-sm-4 col-xs-6">
									<div class="uppercase profile-stat-title">
										 37
									</div>
									<div class="uppercase profile-stat-text">
										 Projects
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-6">
									<div class="uppercase profile-stat-title">
										 51
									</div>
									<div class="uppercase profile-stat-text">
										 Tasks
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-6">
									<div class="uppercase profile-stat-title">
										 61
									</div>
									<div class="uppercase profile-stat-text">
										 Uploads
									</div>
								</div>
							</div> -->
							<!-- END STAT -->
							<!-- <div>
								<h4 class="profile-desc-title">About Marcus Doe</h4>
								<span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
								<div class="margin-top-20 profile-desc-link">
									<i class="fa fa-globe"></i>
									<a href="http://www.keenthemes.com">www.keenthemes.com</a>
								</div>
								<div class="margin-top-20 profile-desc-link">
									<i class="fa fa-twitter"></i>
									<a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
								</div>
								<div class="margin-top-20 profile-desc-link">
									<i class="fa fa-facebook"></i>
									<a href="http://www.facebook.com/keenthemes/">keenthemes</a>
								</div>
							</div>
						</div> -->
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<!-- <div class="profile-content">
						
						
				</div> -->
				<!-- END PROFILE CONTENT -->
			</div>

			<div class="col-md-8 col-sm-8">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-user-secret"></i> &nbsp  Admin Details
						</div>
							<!-- <div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div> -->
						</div>
						<div class="portlet-body form">
							
							


							


							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">First Name</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="admin_first_name" placeholder="First name"
										value="<?php
										if(isset($admin_info) && $admin_info->first_name != ''){
											echo $admin_info->first_name;
										}
										if(isset($values['admin_first_name']) && $values['admin_first_name'] != ''){
											echo $values['admin_first_name'];
										}
										?>"<?php
										if($is_validation){
											echo 'data-parsley-required-message="First name required" data-parsley-admin_first_name data-parsley-trigger="change" required';
										}
										?>>
										<?php
										if(isset($errors['admin_first_name']) && $errors['admin_first_name'] != ''){
											echo $errors['admin_first_name'];
										}
										?>
										
									</div>
								</div>



								<div class="form-group">
									<label class="col-md-3 control-label">Last Name</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="admin_last_name" placeholder="Last name"
										value="<?php
										if(isset($admin_info) && $admin_info->last_name != ''){
											echo $admin_info->last_name;
										}
										if(isset($values['admin_last_name']) && $values['admin_last_name'] != ''){
											echo $values['admin_last_name'];
										}
										?>"<?php
										if($is_validation){
											echo 'data-parsley-required-message="Last name required" data-parsley-admin_last_name data-parsley-trigger="change" required';
										}
										?>>
										<?php
										if(isset($errors['admin_last_name']) && $errors['admin_last_name'] != ''){
											echo $errors['admin_last_name'];
										}
										?>
										
									</div>
								</div>



									<!-- <div class="form-group">
										<label class="col-md-3 control-label">Designation</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="admin_designation" placeholder="Enter your company post" value="<?php
									if(isset($values['admin_designation']) && $values['admin_designation'] != ''){
										echo $values['admin_designation'];
									}
									?>"<?php
									if($is_validation){
										echo 'data-parsley-required-message="Designation required" data-parsley-admin_designation data-parsley-trigger="change" required';
									}
									?>>
									<?php
									if(isset($errors['admin_designation']) && $errors['admin_designation'] != ''){
										echo $errors['admin_designation'];
									}
									?>
											
										</div>
									</div> -->



									
									<div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-9">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-envelope"></i>
												</span>
												<input type="email" class="form-control" name="admin_e_mail" placeholder="Email Address"
												value="<?php
												if(isset($admin_info) && $admin_info->email != ''){
													echo $admin_info->email;
												}
												if(isset($values['admin_e_mail']) && $values['admin_e_mail'] != ''){
													echo $values['admin_e_mail'];
												}
												?>"<?php
												if($is_validation){
													echo 'data-parsley-required-message="E-mail required"  data-parsley-type="email" data-parsley-type-message="Invalid email" data-parsley-admin_e_mail data-parsley-trigger="change" required';
												}
												?>>
												<?php
												if(isset($errors['admin_e_mail']) && $errors['admin_e_mail'] != ''){
													echo $errors['admin_e_mail'];
												}
												?>
											</div>
										</div>
									</div>




									<div class="form-group">
										<label class="col-md-3 control-label">Password</label>
										<div class="col-md-9">
											<div class="input-group">
												<input type="password" class="form-control" name="admin_password" placeholder="Password"
												value="<?php if(isset($values['admin_password']) && $values['admin_password'] != ''){
													echo $values['admin_password'];
												}
												?>"
												<?php 
												if ($is_validation) {
													echo 'data-parsley-type="alphanum" data-parsley-length="[6, 20]" data-parsley-length-message="Password length must be atleast 6 and must not acceed 20" data-parsley-admin_password data-parsley-trigger="change" required';
												}
												?>>
												<?php
												if(isset($errors['admin_password']) && $errors['admin_password'] != ''){
													echo $errors['admin_password'];
												}
												?>

												
												<span class="input-group-addon">
													<i class="fa fa-user"></i>
												</span>
											</div>
										</div>
									</div>
									



									<div class="form-group">
										<label class="col-md-3 control-label">Confirm Password</label>
										<div class="col-md-9">
											<div class="input-group">
												<input type="password" class="form-control" name="admin_conf_password" placeholder="Confirm Password" value="">
												
												<span class="input-group-addon">
													<i class="fa fa-user"></i>
												</span>
											</div>
										</div>
									</div>
									



									<!-- <div class="form-group">
										<label class="col-md-3 control-label">Email id</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 email@example.com
											</p>
										</div>
									</div> -->

									
									
									
								<!-- 	<div class="form-group">
										<label class="col-md-3 control-label">Comments</label>
										<div class="col-md-9">
											<textarea class="form-control" rows="3"></textarea>
										</div>
									</div>
								-->									
								
								
								
								
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>

</div>
</body>
<?php $this->load->view('admin/includes/footer'); ?>
