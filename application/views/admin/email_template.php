<?php $this->load->view('admin/includes/header'); ?>


	
	<section class="content-header">
  <h1>
    Email Templates 
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    
    <li class="active">Email Templates</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <form method="post" action="<?php echo base_url(); ?>admin/email_template/save_email_template" id="form"  enctype="multipart/form-data">

      <div class="col-md-12">

        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Email Template Details</h3>
          </div>
          <div class="box-body">

            <?php
            // if(isset($edit) && $edit == 1){
            //   ?>
               <input type="hidden" name="email_template_id" value="<?php echo $email_template_info->email_template_id; ?>">
               <?php
            // }
            ?>

            <div class="form-group">
              <label>Email Template Title</label>
              <input readonly="true" type="text" name="email_template_title" id="email_template_title" 
              type="text" 
              placeholder="Email Template Title" 
              class="form-control" 
              value="<?php echo $email_template_info->email_template_title; ?>"/>
             
            </div>

            <div class="form-group">
              <label>Allowed Tags</label>
              <input type="text" readonly="true" class="form-control" 
              value="<?php echo $email_template_info->allowed_tag_words; ?>">
            </div>

            <div class="form-group">
              <label>Email Template Subject</label>
              <input type="text" name="email_template_subject" id="email_template_subject" 
              type="text" 
              placeholder="Email Template Subject" 
              class="form-control" 
              value="<?php echo $email_template_info->email_template_subject; ?>"/>
              
            </div>

            <div class="form-group">
              <label>Email Template Content</label>
              <textarea id="editorName" name="email_template_content" rows="10" cols="80" >
              <?php echo $email_template_info->email_template_content; ?>
              </textarea>

              
            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col (left) -->

      <div class="col-md-12">

        <div class="box box-primary">
          <div class="box-body">
            <!-- Date dd/mm/yyyy -->
            <div class="form-group" style="display: block; height: 33px;">
              <div class="col-md-2 pull-right" style="padding: 0;">
                <input type="submit" name="edit" value="Save" class="btn btn-block btn-primary btn-lg "/>
              </div>
              <div class="col-md-2 pull-right" style="padding: 0;margin-right:5px;">

              <a href="<?php echo base_url(); ?>" class="btn btn-block btn-danger btn-lg ">Cancel</a>
            </div>
            </div>



          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col (left) -->

    </form>

  </div>
  <!-- /.row -->

</section>


<?php $this->load->view('admin/includes/footer'); ?>

<script type="text/javascript">
    CKEDITOR.replace( 'editorName' );
</script>