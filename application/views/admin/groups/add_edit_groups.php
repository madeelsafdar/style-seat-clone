<?php $this->load->view('admin/includes/header'); ?>

<?php

$is_validation = true;

@session_start(); 
if(isset($_SESSION['values']) && isset($_SESSION['errors'])){ 
	$values = $_SESSION['values']; 
	$errors = $_SESSION['errors'];

	unset($_SESSION['values']); 
	unset($_SESSION['errors']); 
}

?>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
	<?php
	if(isset($add) && $add == 1){
		echo "Add New Group";
	}else if(isset($edit) && $edit == 1){
		echo "Edit Group";
	}
	?>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Accounts</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Groups</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#"><?php
	if(isset($add) && $add == 1){
		echo "Add New Group";
	}else if(isset($edit) && $edit == 1){
		echo "Edit Group";
	}
	?></a>
		</li>
	</ul>

</div>
<!-- END PAGE HEADER-->

<!-- Messages -->
<div class="note note-success" id="success_message">
	<p>
		<?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); } ?>
	</p>
</div>
<!-- Messages -->  

<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i> Group Form
				</div>
				<div class="tools">
					<a href="" class="collapse">
					</a>

				</div>
			</div>
			<div class="portlet-body form">
				<form role="form" id="group_form" method="post" action="<?php echo base_url('admin/groups/save_group'); ?>" <?php if ($is_validation) { echo "data-parsley-validate"; } ?>>
					<div class="form-body">

						<?php
						if(isset($edit) && $edit == 1){
							?>
							<input type="hidden" name="group_id" value="<?php echo $group_info->id; ?>">
							<?php
						}
						?>

						<div class="form-group">
							<label>Group Name</label>
							<div class="input-icon input-icon-sm">
								<i class="fa fa-bell-o"></i>
								<input type="text" class="form-control input-sm" placeholder="Group Name" class="required" name="name" id="name" value="<?php
								if(isset($values['name']) && $values['name'] != '' && isset($add) && $add == 1){
									echo $values['name'];
								}
								if(isset($group_info) && $group_info->name != ''){
									echo $group_info->name;
								}
								?>"
								<?php
								if($is_validation){
									echo 'data-parsley-required-message="Group Name is required" data-parsley-first_name data-parsley-trigger="change" required';
								}
								?>
								/>
								<?php
								if(isset($errors['name']) && $errors['name'] != ''){
									echo $errors['name'];
								}
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label">Description</label>
							<textarea rows="8" cols="" class="form-control" name="description" class="required" id="description"><?php
								if(isset($values['description']) && $values['description'] != '' && isset($add) && $add == 1){
									echo $values['description'];
								}
								if(isset($group_info) && $group_info->description != ''){
									echo $group_info->description;
								}
								?></textarea>
								<?php
								if(isset($errors['description']) && $errors['description'] != ''){
									echo $errors['description'];
								}
								?>
							</div>


						</div>
						<div class="form-actions">
							<button type="submit" name="<?php
							if(isset($add) && $add == 1){
								echo "add";
							}else if(isset($edit) && $edit == 1){
								echo "edit";
							}
							?>" value="Save" class="btn btn-success">Submit</button>
							<a href="<?php echo base_url('admin/groups'); ?>" class="btn btn-danger">Cancel</a>

						</div>
					</form>
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->


		</div>

	</div>
	<?php $this->load->view('admin/includes/footer'); ?>

	<script type="text/javascript">

		<?php 
		if(!$this->session->flashdata('success_message')){
			?>
			$("#success_message").hide();    
			<?php
		}
		?>

		setTimeout(function(){
			$("#success_message").hide();        
		}, 3000);

		<?php 
		if(!$this->session->flashdata('failure_message')){
			?>
			$("#failure_message").hide();    
			<?php
		}
		?>

		setTimeout(function(){
			$("#failure_message").hide();        
		}, 3000);



	</script>

	<script type="text/javascript">


		$("#group_form").validate({
			rules: {
				name: "required",
				description: "required"
			},
			messages: {
				name: {
					required: "Please enter Group Title.",
				},
				description: {
					required: "Please enter Group Description.",
				},
			}
		});

	</script>