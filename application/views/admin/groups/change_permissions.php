<?php $this->load->view('admin/includes/header'); ?>

<style type="text/css">
	.onoffswitch-inner:before {
		content: "Allowed";
		font-size: 10px;
	}
	.onoffswitch-inner:after {
		content: "Not-Allowed";
		font-size: 10px;
	}
</style>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
	Permissions 
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Accounts</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Groups</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Permissions</a>
		</li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">

		</div>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">


		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i><?php echo $group_info->name; ?> Permissions
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body flip-scroll">
				
				<form method="post" action="<?php echo base_url('admin/groups/save_permissions'); ?>">

					<input type="hidden" name="group_id" value="<?php echo $group_info->id; ?>">

					<table cellpadding="0" cellspacing="0" width="100%" class="tDefault">
						<thead>
							<tr>
								<td style="width:10px;">SN</td>
								<td colspan="2">Module Name</td>
								<td style="width: 112px;">Permission</td>
							</tr>
						</thead>
						<tbody>


							<?php
							$outer_count = 1;
							$current_module = '';
							foreach ($all_modules as $module) {

								$module_array = explode("/", $module['path']);

								if($module_array[1] == "index" && $module_array[0] != $current_module){
									$current_module = $module_array[0];
									$inner_count = 1;
									?>
									<tr class="parent_row">
										<td><?php echo $outer_count; ?></td>
										<td colspan="2"><?php echo $module['alias']; ?></td>
										<td >
											<div class="onoffswitch">
												<input type="checkbox" name="new_permissions[]" value="<?php echo $module['id']; ?>" class="icheck onoffswitch-checkbox parents" id="permission_module_<?php echo $current_module; ?>"
												<?php
												if($all_permissions_of_group[$module['id']] == 1){
													echo "checked";
												}
												?>
												>
												<label class="onoffswitch-label" for="permission_module_<?php echo $current_module; ?>">
													<span class="onoffswitch-inner"></span>
													<span class="onoffswitch-switch"></span>
												</label>
											</div> </td>
										</tr>
										<?php
										$outer_count++;
									}else{
										?>
										<tr>
											<td></td>
											<td style="width: 10px;"><?php echo $inner_count; ?></td>
											<td><?php echo $module['alias']; ?></td>
											<td><div class="onoffswitch">
												<input type="checkbox" name="new_permissions[]" value="<?php echo $module['id']; ?>" class="icheck onoffswitch-checkbox childrens permission_module_<?php echo $current_module; ?>" parent="permission_module_<?php echo $current_module; ?>" id="permission_submodule_<?php echo $current_module.'_'.$inner_count; ?>"
												<?php
												if($all_permissions_of_group[$module['id']] == 1){
													echo "checked";
												}
												?>
												>
												<label class="onoffswitch-label" for="permission_submodule_<?php echo $current_module.'_'.$inner_count; ?>">
													<span class="onoffswitch-inner"></span>
													<span class="onoffswitch-switch"></span>
												</label>
											</div> </td>
										</tr>
										<?php
										$inner_count++;
									}
								}
								?>
							</tbody>
							<tfoot class="tableFooter" style="display: table-footer-group;padding:10px;">
								<tr>
									<td colspan="4" style="padding: 10px;">
										<input type="submit" class="buttonS bLightBlue pull-right" value="Save Permissions" />
									</td>
								</tr>
							</tfoot>

						</table>

						

					</form>

				</div>
			</div>
			<!-- END SAMPLE TABLE PORTLET-->

		</div>
	</div>
	<!-- END PAGE CONTENT-->

	<?php $this->load->view('admin/includes/footer'); ?>

	<script type="text/javascript">
		$('.childrens').change(function() {    
			id_of_parent = $(this).attr('parent');

			if($(this).prop('checked')){
				$('#'+id_of_parent).prop('checked', true);
			}

		});

		$('.parents').change(function() {    
			class_of_children = $(this).attr('id');

			if(!$(this).prop('checked')){
				$('.'+class_of_children).removeAttr('checked');
			}

		});
	</script>