<?php $this->load->view('admin/includes/header'); ?>

<style type="text/css">
	.content div.selector{
		width: auto !important;
	}
	table .selector, table .selector > span, .dataTables_length .selector, .dataTables_length .selector > span{
		height: auto !important;
	}
	.dataTables_length .selector{
		margin-top: 0px;
	}
	.search-filter-input{
		margin-top: -3px;
		margin-bottom: -4px;
	}

	#failure_message{
		margin-top: 95px;
	}

	#success_message{
		margin-top: 95px;
	}

</style>

<div class="nNote nSuccess" id="success_message">
	<p>
		<?php if($this->session->flashdata('success_message')){ echo $this->session->flashdata('success_message'); } ?>
	</p>
</div>

<div class="nNote nFailure" id="failure_message">
	<p>
		<?php if($this->session->flashdata('failure_message')){ echo $this->session->flashdata('failure_message'); } ?>
	</p>
</div>

<!-- <div class="widget no_bg no_shadow no_border no_margin" >
	<div class="formRow no_border">
		<a href="<?php echo base_url('groups/add_group'); ?>" class="buttonM formSubmit bGreen"><i class="fa fa-plus"></i> Add Group</a>
	</div>
</div> -->

<div class="widget no_top_margin">
	<div class="whead">
		<h6><i class="fa fa-users"></i> Users in <?php echo $group_info->name; ?></h6>
	</div>
	<div id="dyn" class="shownpars">
		<table cellpadding="0" cellspacing="0" border="0" class="users_in_group_table" id="users_in_group_table">
			<thead>
				<tr>
					<th class="sn">
						SN
					</th>
					<th>
						Name
					</th>
					<th>
						Email
					</th>
					<th>
						Tel
					</th>
					<th class="actions-groups">
						Actions
					</th>
				</tr>
			</thead>
			<tbody id="groups_table_body">
				<?php
				$count = 1;
				foreach ($users_in_group as $user) {
					// var_dump($group);
					?>
					<tr>
						<td><?php echo $count; ?></td>
						<td><?php echo $user->first_name.' '.$user->last_name; ?></td>
						<td><?php echo $user->email; ?></td>
						<td><?php echo $user->phone; ?></td>
						<td>
							<?php
							if ($this->acl->isAllow('users','edit_user')) {
								?>
								<a href="<?php echo base_url('users/edit_user/'.$user->id); ?>" class="tablectrl_small no-float bGreen tipS" title="Edit">
									<span class="iconb" data-icon="&#xe1db;"></span>
								</a>
								<?php
							}
							?>

							<?php
							if ($this->acl->isAllow('users','ajax_delete_user')) {
								?>
								<a onclick="delete_me(this)" uid="<?php echo $user->id; ?>" class="tablectrl_small bRed tipS" title="Delete">
									<span class="iconb" data-icon="&#xe136;"></span>
								</a>
								<?php
							}
							?>
						</td>
					</tr>
					<?php
					$count++;
				}
				?>
			</tbody>

		</table>
	</div>
</div>


<div class="spacer"><span></span></div>


<!-- Delete Dialogues Start -->
<div id="delete_user_dialog" title="Delete User">
	<p>Are you sure you want to delete this user.</p>
</div>
<!-- Delete Dialogues End -->

<!-- Success Message Container Js Start -->

<script type="text/javascript">

	<?php 
	if(!$this->session->flashdata('success_message')){
		?>
		$("#success_message").hide();    
		<?php
	}
	?>

	setTimeout(function(){
		$("#success_message").hide();        
	}, 3000);

	<?php 
	if(!$this->session->flashdata('failure_message')){
		?>
		$("#failure_message").hide();    
		<?php
	}
	?>

	setTimeout(function(){
		$("#failure_message").hide();        
	}, 3000);

	

</script>

<!-- Success Message Container Js End -->

<script type="text/javascript">

	var users_in_group_table = $('#users_in_group_table').DataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"sSearch": "",
			"sLengthMenu": "Show Entries _MENU_"
		},
		columns: [
		{ name: 'sn' },
		{ name: 'name' },
		{ name: 'email' },
		{ name: 'phone' },
		{ name: 'actions' }
		],
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [4]}]
	});

	
</script>

<script type="text/javascript">
	$('#delete_user_dialog').dialog({
		autoOpen: false,
		width: 400,
		buttons:[
		{
			text: 'Confirm', 
			'class': 'bRed',
			click: function(){
				$(this).dialog("close");
				delete_after_confirm();
			}
		},
		{
			text: 'Cancel', 
			'class': 'bBlue',
			click: function(){
				$(this).dialog("close");
			}
		},
		],
		open: function(){
			$('.ui-dialog-titlebar').find('button').prepend('<i id="jquery_ui_close_mine" style="margin: -1px 0px 0px 0px !important;" class="fa fa-times"></i>');
		},
		close: function(ev, ui) {
			$('.ui-dialog-titlebar').find('button').find('#jquery_ui_close_mine').remove();
		}
	});
</script>

<script type="text/javascript">
	function delete_me(element) {
		$('#delete_user_dialog').dialog('open');
		user_id = $(element).attr("uid");
	}

	function delete_after_confirm() {
		$.post("users/ajax_delete_user", {
			"user_id": user_id
		}, function(data) {
			if(data == 'deleted'){

				$("#success_message p").html('User deleted successfully.');
				$("#success_message").show();

				setTimeout(function(){
					$("#success_message").hide();        
				}, 3000);

				users_in_group_table.row('.selected').remove().draw( false );
			}else if(data > 0 && data != 'deleted'){

				$("#failure_message p").html('User couldnt be delete.Please unassign associated projects.');
				$("#failure_message").show();

				setTimeout(function(){
					$("#failure_message").hide();        
				}, 3000);
			}
			
		});
	}

	$('#users_in_group_table tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		}
		else {
			users_in_group_table.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	} );

</script>

<script type="text/javascript">
	$(".dataTables_length select").uniform();
	$('div.dataTables_filter input').attr('placeholder', 'Search...');
</script>

<?php $this->load->view('admin/includes/footer'); ?>