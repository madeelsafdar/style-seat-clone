<?php $this->load->view('admin/includes/header'); ?>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
	Groups 
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Accounts</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Groups</a>
		</li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">

			<?php
			// if ($this->acl->isAllow('groups','add_group')) {
			?>
			<a href="<?php echo base_url('admin/groups/add_group'); ?>" type="button" class="btn btn-success">Add Group</a>
			<?php
			// }
			?>

		</div>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">

		<!-- Messages -->
		<div class="note note-success" id="success_message">
			<p>
				<?php if($this->session->flashdata('success_message')){ echo $this->session->flashdata('success_message'); } ?>
			</p>
		</div>

		<div class="note note-failure" id="failure_message">
			<p>
				<?php if($this->session->flashdata('failure_message')){ echo $this->session->flashdata('failure_message'); } ?>
			</p>
		</div>
		<!-- Messages -->


		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i>Groups Table
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body flip-scroll">
				<table class="table table-bordered table-striped table-condensed flip-content" id="groups_table">
					<thead class="flip-content">
						<tr>
							<th>
								SN
							</th>
							<th>
								Groups Name
							</th>
							<th>
								Description
							</th>
							<th>
								Actions
							</th>
						</tr>
					</thead>
					<tbody>

						<?php
						$count = 1;
						foreach ($all_groups as $group) {
							?>
							<tr >
								<td><?php echo $count; ?></td>
								<td><?php echo $group->name; ?></td>
								<td><?php echo $group->description; ?></td>
								<td>

									<?php
									// if ($this->acl->isAllow('groups','change_permissions')) {
										?>
										<a class="tablectrl_small no-float bGreen tipS" title="Change Permissions" href="<?php echo base_url('admin/groups/change_permissions/'.$group->id); ?>">
											<i class="fa fa-cogs"></i>
										</a>
										<?php
									// }
									?>

									<?php
									// if ($this->acl->isAllow('groups','edit_group')) {
									?>
									<a href="<?php echo base_url('admin/groups/edit_group/'.$group->id); ?>" class="tablectrl_small no-float bGreen tipS" title="Edit">
										<i class="fa fa-edit"></i>
									</a>
									<?php
									// }
									?>

									<?php
									// if ($this->acl->isAllow('groups','ajax_delete_group')) {
									?>
									<a onclick="delete_me(this)" gid="<?php echo $group->id; ?>" class="tablectrl_small bRed tipS" data-toggle="modal" href="#delete_group_dialog" title="Delete">
										<i class="fa fa-trash-o"></i>
									</a>
									<?php
									// }
									?>

								</td>
							</tr>
							<?php
							$count++;
						}
						?>

					</tbody>
				</table>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->

	</div>
</div>
<!-- END PAGE CONTENT-->


<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="delete_group_dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Delete Group</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this group.</p>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="delete_after_confirm()" id="" class="btn blue">Confirm</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


<?php $this->load->view('admin/includes/footer'); ?>

<!-- Success Message Container Js Start -->

<script type="text/javascript">

	<?php 
	if(!$this->session->flashdata('success_message')){
		?>
		$("#success_message").hide();    
		<?php
	}
	?>

	setTimeout(function(){
		$("#success_message").hide();        
	}, 3000);

	<?php 
	if(!$this->session->flashdata('failure_message')){
		?>
		$("#failure_message").hide();    
		<?php
	}
	?>

	setTimeout(function(){
		$("#failure_message").hide();        
	}, 3000);

	

</script>

<!-- Success Message Container Js End -->

<script type="text/javascript">



	var groups_table = $('#groups_table').DataTable({
		"bJQueryUI": true,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"sSearch": "",
			"sLengthMenu": "Show Entries _MENU_"
		},
		columns: [
		{ name: 'sn' },
		{ name: 'title' },
		{ name: 'description' },
		{ name: 'actions' }
		],
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [3]}]
	});

	

</script>

<script type="text/javascript">
	function delete_me(element) {
		group_id = $(element).attr("gid");
	}

	function delete_after_confirm() {
		$.post("<?php echo base_url(); ?>admin/groups/ajax_delete_group", {
			"group_id": group_id
		}, function(data) {
			if(data == 'deleted'){

				$("#success_message p").html('Group deleted successfully.');
				$("#success_message").show();

				setTimeout(function(){
					$("#success_message").hide();        
				}, 3000);

				groups_table.row('.selected').remove().draw( false );
			}else if(data != 'deleted'){

				$("#failure_message p").html('There was an error deleting this group.');
				$("#failure_message").show();

				setTimeout(function(){
					$("#failure_message").hide();        
				}, 3000);
			}
			$('#delete_group_dialog').modal('hide');
		});
	}

</script>

<script type="text/javascript">
	$('div.dataTables_filter input').attr('placeholder', 'Search...');
</script>