<?php $this->load->view('admin/includes/header'); ?>

<?php

$is_validation = true;

@session_start(); 
if(isset($_SESSION['values']) && isset($_SESSION['errors'])){ 
   $values = $_SESSION['values']; 
   $errors = $_SESSION['errors'];

   unset($_SESSION['values']); 
   unset($_SESSION['errors']); 
}

?>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
   <?php
   if(isset($add) && $add == 1){
      echo "Add New Advertisement";
   }else if(isset($edit) && $edit == 1){
      echo "Edit Advertisement";
   }
   ?>
</h3>
<div class="page-bar">
   <ul class="page-breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="index.html">Home</a>
         <i class="fa fa-angle-right"></i>
      </li>
      <li>
         <a href="#">Advertisements</a>
         <i class="fa fa-angle-right"></i>
      </li>
      <li>
         <a href="#"><?php
   if(isset($add) && $add == 1){
      echo "Add New Advertisement";
   }else if(isset($edit) && $edit == 1){
      echo "Edit Advertisement";
   }
   ?></a>
      </li>
   </ul>
   
</div>
<!-- END PAGE HEADER-->

<!-- Messages -->
<div class="note note-success" id="success_message">
   <p>
      <?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); } ?>
   </p>
</div>
<!-- Messages -->  

<!-- BEGIN PAGE CONTENT-->
<div class="row">
   <div class="col-md-12">
      <!-- BEGIN SAMPLE FORM PORTLET-->
      <div class="portlet box green">
         <div class="portlet-title">
            <div class="caption">
               <i class="fa fa-gift"></i> Advertisement Form
            </div>
            <div class="tools">
               <a href="" class="collapse">
               </a>

            </div>
         </div>
         <div class="portlet-body form">
            <form role="form" id="advertisement_form" method="post" action="<?php echo base_url('admin/advertisements/save_advertisement'); ?>" <?php if ($is_validation) { echo "data-parsley-validate"; } ?>>
               <div class="form-body">

                  <?php
                  if(isset($edit) && $edit == 1){
                     ?>
                     <input type="hidden" name="advertisement_id" value="<?php echo $advertisement_info->advertisement_id; ?>">
                     <?php
                  }
                  ?>

                  <div class="form-group">
                     <label>Title</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="text" class="form-control input-sm" placeholder="Advertisement Title" name="advertisement_title" id="advertisement_title" value="<?php
                        if(isset($values['advertisement_title']) && $values['advertisement_title'] != '' && isset($add) && $add == 1){
                           echo $values['advertisement_title'];
                        }
                        if(isset($advertisement_info) && $advertisement_info->advertisement_title != ''){
                           echo $advertisement_info->advertisement_title;
                        }
                        ?>" 
                        <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="advertisement_title is required" data-parsley-advertisement_title data-parsley-trigger="change" required';
                        }
                        ?>
                        />
                        <?php
                        if(isset($errors['advertisement_title']) && $errors['advertisement_title'] != ''){
                           echo $errors['advertisement_title'];
                        }
                        ?>
                     </div>
                  </div>

                  <div class="form-group">
                     <label>Advertisement Text</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <textarea class="form-control input-sm" placeholder="Advertisement Text" name="advertisement_text" id="advertisement_text" <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Advertisement Text is required" required';
                        }
                        ?>><?php
                        if(isset($values['advertisement_text']) && $values['advertisement_text'] != '' && isset($add) && $add == 1){
                           echo $values['advertisement_text'];
                        }
                        if(isset($advertisement_info) && $advertisement_info->advertisement_text != ''){
                           echo $advertisement_info->advertisement_text;
                        }
                        ?></textarea> 
                        
                        <?php
                        if(isset($errors['advertisement_text']) && $errors['advertisement_text'] != ''){
                           echo $errors['advertisement_text'];
                        }
                        ?>
                     </div>
                  </div>

               </div>
               <div class="form-actions">
                  <button type="submit" name="<?php
                  if(isset($add) && $add == 1){
                     echo "add";
                  }else if(isset($edit) && $edit == 1){
                     echo "edit";
                  }
                  ?>" value="Save" class="btn btn-success">Submit</button>
                  <a href="<?php echo base_url('admin/advertisements'); ?>" class="btn btn-danger">Cancel</a>

               </div>
            </form>
         </div>
      </div>
      <!-- END SAMPLE FORM PORTLET-->
      
      
   </div>

</div>
<?php $this->load->view('admin/includes/footer'); ?>

<script type="text/javascript">
   window.ParsleyValidator
   .addValidator('advertisement_title', function(value, requirement) {
      advertisement_title_regex = /^[a-z\d\ \s]+$/i;

      var flag = 0;

            /*if(value.length < requirement ){
             flag = 1;
          }*/

          if (!advertisement_title_regex.test(value)) {
           flag = 1;
        }

        if (flag == 0) {
           return true;
        } else {
           return false;
        }

     }, 32)
   .addMessage('en', 'Advertisement Title', "Enter a valid Advertisement Title.");

</script>

<!-- Success Message Container Js Start -->

<script type="text/javascript">

   <?php 
   if(!$this->session->flashdata('success_message')){
      ?>
      $("#success_message").hide();    
      <?php
   }
   ?>

   setTimeout(function(){
      $("#success_message").hide();        
   }, 3000);

   <?php 
   if(!$this->session->flashdata('failure_message')){
      ?>
      $("#failure_message").hide();    
      <?php
   }
   ?>

   setTimeout(function(){
      $("#failure_message").hide();        
   }, 3000);

   

</script>

<!-- Success Message Container Js End -->