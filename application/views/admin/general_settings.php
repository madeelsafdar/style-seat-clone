<?php $this->load->view('admin/includes/header'); ?>

<h1> <?php echo $setting_title;?> </h1>





<form role="form" method="POST" action="<?php echo base_url(); ?>admin/settings/update_setting_value" enctype="multipart/form-data">
	<input type="hidden" name="parent_setting_id" value="<?php echo $parent_setting_id;?>">
	<div class="form-body">

		<?php
		foreach ($sub_settings as $value) {
			if($value['setting_name'] == "home_page_advertisement" || $value['setting_name'] == "advertisement"){
			}else{
			?>

			<div class="form-group ">

				<label><?php echo $value['setting_alias']; ?></label>

				<?php
				if($value['setting_name'] == 'is_picture_logo'){
					?>
					<input class="form-control" type="checkbox" id="<?php echo $value['setting_name'] ?>" name="<?php echo $value['setting_name'] ?>" value="<?php echo $value['setting_name'] ?>" <?php if($value['setting_value']){ echo 'checked'; } ?>>
					<?php
				}else if($value['setting_name'] == 'picture_logo_url'){
					?>
					<div class="picture_upload_cont" style="margin-bottom: 10px;">

						<div class="thumb" style="text-align: center;width: 100%;height: 420px;overflow: hidden;border: 1px solid #504F4F;margin-bottom: 5px;border-radius: 5px;background: #E0DEDE;margin: 0px auto;margin-bottom: 5px;">
							<img id="preview_image" 
							alt="" style="max-width:100%;max-height:100%;" src="<?php echo $value['setting_value']; ?>"  />
						</div>

						<div style="text-align:center;" id="selected_image_name">
						</div>

						<div style="text-align:center;">

							<!-- <span class="btn btn-lg btn-primary btn-file" style="margin:0px auto;"> -->
							Select Logo 
							<input 
							name="input_logo_file"
							type="file" 
							id="input_logo_file" 

							>
							<!-- </span> -->

						</div>

					</div>
					<?php
				}else if($value['setting_name'] == 'home_page_advertisement'){
					?>
					<!-- <textarea class="form-control" id="<?php //echo $value['setting_name'] ?>" name="<?php //echo $value['setting_name'] ?>"><?php //if ($value['setting_value']) { //echo $value['setting_value']; } ?></textarea> -->
					<?php
				}else{
					?>
					<input type="text" name="<?php echo $value['setting_name'] ?>" class="form-control" value="<?php 
					if ($value['setting_value']) {
						echo $value['setting_value'];
					}

					?>" placeholder="<?php echo $value['setting_alias'] ?>" id="form_control_1"><br>
					<?php
				}
				?>
				
				
				

			</div>
			<?php } ?>

			<?php } ?>

		</div>

		
		<br>
		
		
		<input type="submit" class="btn btn-info" value="Save">
	</form>

	<?php $this->load->view('admin/includes/footer'); ?>

	<script type="text/javascript">
		function add_user_readURL(input) {

			if (input.files && input.files[0]) {




				var reader = new FileReader();

				reader.onload = function(e) {
					$('#preview_image').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		}

		$(document).ready(function() {

			$("#input_logo_file").change(function() {

				var filename = $('#input_logo_file').val();
				var element = "<div style='background: #E7E4E4;border: 1px solid #424242;margin-bottom: 10px;color: #424242;padding: 5px;border-radius: 5px;' >" + filename + "</div>";

				add_user_readURL(this);
				$("#selected_image_name").html(element);
				$("#image_name").val(filename);
				
			});
		});
	</script>