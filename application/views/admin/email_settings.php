<?php $this->load->view('admin/includes/header'); ?>


<section>
<div class="row">
	
	<div class="col-sm-12">
			

			<div class="box-header">
				<h3 class="box-title">Email Settings</h3>
			</div>

			

			<div class="box-body">
							<table id="email_templates_table" class="table table-bordered table-striped">
			            <thead>
			              <tr>
			                <th>#</th>
			                <th>Email Template Title</th>
			                <th>Actions</th>
			              </tr>
			            </thead>
			            <tbody>

			              <?php
			              $count = 1;
			              foreach ($all_email_templates as $email_template) {
			                ?>
			                <tr>
			                  <td><?php echo $count; ?></td>
			                   <td><?php echo $email_template->email_template_title; ?></td> 
			                  <td>

			                  <a href="<?php echo base_url('admin/email_template/open_email_template').'/'.$email_template->email_template_id; ?>" title="Edit" type="button" class="btn btn-xs btn-info dropdown-toggle">
			                    <i class="fa fa-pencil"></i>
			                  </a>

			                  <!-- <a href="javascript:void(0);" gn="<?php echo $email_template->email_template_title; ?>" gid="<?php echo $email_template->email_template_id; ?>" onclick="delete_me(this)" type="button" title="Delete" class="btn btn-xs btn-danger dropdown-toggle" data-toggle="dropdown">
			                    <i class="fa fa-trash-o"></i>
			                  </a> -->

			                </td>
			              </tr>
			              <?php
			              $count++;
			            }
			            ?>



			          </tbody>
			          
			        </table>
			</div>
	

	
	</div>
</div>
</section>

<?php $this->load->view('admin/includes/footer'); ?>


<script type="text/javascript">

  $.extend( $.fn.dataTableExt.oStdClasses, {
    "sFilterInput": "form-control",
    "sLengthSelect": "selectpicker col-md-5 no_padding"
  });

  var email_templates_table = $('#email_templates_table').DataTable({
    "bJQueryUI": false,
    "bAutoWidth": false,
    "sPaginationType": "full_numbers",
    "pageLength": settings.records_per_page,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [2]}],
    "language": {
        "searchPlaceholder": "Search...",
        "sSearch": "",
        "zeroRecords": "There are no email templates at the moment."
    },
    "filter":     false,
    "dom": '<"top">rt<"bottom col-md-12"<"col-md-4 no_padding"l><"col-md-4 information no_padding"i><"col-md-4 no_padding"p>><"clear">'
  });

  
</script>



<script type="text/javascript">

  $('#email_templates_table tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
      $(this).removeClass('selected');
    }
    else {
      email_templates_table.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
    }
  } );

  function delete_me(element){
    email_template_id = $(element).attr('gid');
    $("#delete_email_template_modal").modal("show");

  }

  function delete_after_confirm() {
    $.post("<?php echo base_url('my_email_templates/ajax_delete_email_template'); ?>", {
      "email_template_id": email_template_id
    }, function(data) {
      if(data == 'deleted'){

        $("#success_message h4").html('Email Template deleted successfully.');
        $("#success_message").show();
        $("#message_cont").show();   

        setTimeout(function(){
          $("#success_message").hide();  
          $("#message_cont").hide();          
        }, 3000);

        email_templates_table.row('.selected').remove().draw( false );
      }else if(data > 0 && data != 'deleted'){

        $("#failure_message h4").html('Email Template couldnt be delete.');
        $("#failure_message").show();
        $("#message_cont").show();   

        setTimeout(function(){
          $("#failure_message").hide();  
          $("#message_cont").hide();         
        }, 3000);
      }
      
    });

    $("#delete_email_template_modal").modal("hide");
  }
</script>