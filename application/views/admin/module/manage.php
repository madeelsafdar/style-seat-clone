<?php $this->load->view('admin/includes/header'); ?>

<!-- Page Heading -->
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			Manage <?php echo $this->general->convert_to_label($table); ?>
		</h1>
		
	</div>
</div>
<!-- Page Heading -->

<?php
	// var_dump($all_records);
// var_dump($all_fields);
$this->load->config('website');
?>

<!-- Content Here -->

<div class="row">
	<div class="col-lg-12">
		<div class="">
			<table class="table table-bordered table-hover" id="myTable" style="margin: 0;width: 100%;">
				<thead>
					<tr>

						<?php
						if ($primary_key != '') {
							?>
							<th><?php echo $this->general->convert_to_label($primary_key); ?></th>
							<?php
						}
						?>
						
						<?php
						foreach ($all_fields as $field_info) {
							if($field_info['COLUMN_TYPE'] == 'int(11)' && $field_info['COLUMN_KEY'] != 'PRI'){
								
							}else{
								

								if($field_info['COLUMN_NAME'] == 'status'){
									foreach ($foreign_fields as $foreign_field) {
										?>
										<th><?php echo $this->general->convert_to_label($foreign_field->COLUMN_NAME); ?></th>
										<?php
									}
									?>
									<th><?php echo $this->general->convert_to_label($field_info['COLUMN_NAME']); ?></th>
									<?php
								}else{
									?>
									<th><?php echo $this->general->convert_to_label($field_info['COLUMN_NAME']); ?></th>
									<?php
								}
							}
						}
						?>

						<th>Actions</th>

					</tr>
				</thead>
				<tbody>
				</tbody>
				<?php
				$show_individual_search = $this->config->item('show_individual_search');
				if($show_individual_search == 1){
					?>
					<tfoot>
						<tr>
							<?php
							$count = 0;

							if ($primary_key != '') {
								?>
								<th><input type="text" field-no="<?php echo $count; ?>" name="sSearch_<?php echo $count; ?>"></th>
								<?php
								$count++;
							}
							?>

							<?php
							foreach ($all_fields as $field_info) {
								?>
								<th><input type="text" field-no="<?php echo $count; ?>" name="sSearch_<?php echo $count; ?>"></th>
								<?php
								$count++;
							}
							?>
						</tr>
					</tfoot>
					<?php
				}
				?>
			</table>
			
		</div>
	</div>

</div>
<!-- /.row -->

<!-- Content Here -->

<?php $this->load->view('admin/includes/footer'); ?>

<?php
$fields_array = array();
$status_col_pos = 1;
$created_at_col_pos = 1;
$updated_at_col_pos = 1;
$total_columns_count = 1;

$fields_array[]['mData'] = $primary_key;
foreach ($all_fields as $field_info) {

	if($field_info['COLUMN_TYPE'] == 'int(11)' && $field_info['COLUMN_KEY'] != 'PRI'){
		
	}else{
		if($field_info['COLUMN_NAME'] == 'status'){
			foreach ($foreign_fields as $foreign_field) {
				$fields_array[]['mData'] = $foreign_field->COLUMN_NAME;
				$total_columns_count++;
			}
			$fields_array[]['mData'] = $field_info['COLUMN_NAME'];
			$status_col_pos = $total_columns_count;
			$total_columns_count++;
		}else{
			$fields_array[]['mData'] = $field_info['COLUMN_NAME'];

			if($field_info['COLUMN_NAME'] == 'created_at'){
				$fields_array[$total_columns_count]['sType'] = 'date';
				$created_at_col_pos = $total_columns_count;
			}

			if($field_info['COLUMN_NAME'] == 'updated_at'){
				$fields_array[$total_columns_count]['sType'] = 'date';
				$updated_at_col_pos = $total_columns_count;
			}

			$total_columns_count++;
		}



	}

	
}


?>

<script type="text/javascript" src="<?php echo base_url()."/public/admin/date_format/date.format.js"; ?>"></script>

<script type="text/javascript" language="javascript">

	function get_human_date_with_day_name(mysql_datetime)
	{

		var now = new Date(mysql_datetime).getTime();
		to_return_date = dateFormat(now, "dddd, mmmm dS, yyyy");
		return to_return_date;

	}

	function get_human_simple_date(mysql_datetime)
	{

		var now = new Date(mysql_datetime).getTime();
		to_return_date = dateFormat(now, "d-mm-yyyy");
		return to_return_date;

	}

	function get_human_date_without_day_name(mysql_datetime)
	{

		var now = new Date(mysql_datetime).getTime();
		to_return_date = dateFormat(now, "mmm d, yyyy h:MM TT");
		return to_return_date;

	}

	function get_human_date_time(mysql_datetime)
	{

		var now = new Date(mysql_datetime).getTime();
		to_return_date = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT");
		return to_return_date;

	}

	console.log(<?php echo json_encode($fields_array); ?>);

	var hide_show_search = <?php if($this->config->item('show_combined_search')){ echo 'true'; }else{ echo 'false'; } ?>;
	var status_col_pos = <?php echo $status_col_pos; ?>;
	var created_at_col_pos = <?php echo $created_at_col_pos; ?>;
	var updated_at_col_pos = <?php echo $updated_at_col_pos; ?>;
	var primary_key = '<?php echo $primary_key; ?>';

	var mytable = $('#myTable').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bFilter": hide_show_search,
		"order": [[ created_at_col_pos, "asc" ]],
		"sAjaxSource": "<?php echo base_url('admin/module/get_records').'/'.$table; ?>",
		"aoColumns": <?php echo json_encode($fields_array); ?>,
		"aoColumnDefs": [ {
			"aTargets": [ <?php echo $total_columns_count; ?> ],
			"mData": "actions",
			"mRender": function ( data, type, full ) {
				console.log(full);
				// return '<a href="<?php echo base_url("admin/module/edit")."/".$table."/"."'+full[primary_key]+'"; ?>">Edit</a> <a href="<?php echo base_url("admin/module/delete")."/".$table."/"."'+full[primary_key]+'"; ?>">Delete</a> <a href="<?php echo base_url("admin/module/change_status")."/".$table."/"."'+full[primary_key]+'"; ?>">Enable/Disable</a>';

				return '<a href="<?php echo base_url("admin/module/view_images")."/".$table."/"."'+full[primary_key]+'"; ?>">View Images</a>';
			}
		} ],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			if ( aData.status == 1 )
			{
				$('td:eq('+status_col_pos+')', nRow).html( '<span class="btn btn-xs btn-success">Active</span>' );
			}else if ( aData.status == 0 ){
				$('td:eq('+status_col_pos+')', nRow).html( '<span class="btn btn-xs btn-danger">Inactive</span>' );
			}

			if (aData.created_at)
			{
				$('td:eq('+created_at_col_pos+')', nRow).html( get_human_date_without_day_name(aData.created_at) );
			}

			if (aData.updated_at)
			{
				$('td:eq('+updated_at_col_pos+')', nRow).html( get_human_date_without_day_name(aData.updated_at) );
			}
		}
	} );

mytable.columns().eq(0).each(function (colIdx) {
	$('input', mytable.column(colIdx).footer()).on('keydown keyup', function () {
		mytable
		.column(colIdx)
		.search(this.value)
		.draw();
	});
});
</script>