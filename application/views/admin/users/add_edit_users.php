<?php $this->load->view('admin/includes/header'); ?>

<?php

$is_validation = true;

@session_start(); 
if(isset($_SESSION['values']) && isset($_SESSION['errors'])){ 
   $values = $_SESSION['values']; 
   $errors = $_SESSION['errors'];

   unset($_SESSION['values']); 
   unset($_SESSION['errors']); 
}

?>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
   <?php
   if(isset($add) && $add == 1){
      echo "Add New User";
   }else if(isset($edit) && $edit == 1){
      echo "Edit User";
   }
   ?>
</h3>
<div class="page-bar">
   <ul class="page-breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="index.html">Home</a>
         <i class="fa fa-angle-right"></i>
      </li>
      <li>
         <a href="#">Accounts</a>
         <i class="fa fa-angle-right"></i>
      </li>
      <li>
         <a href="#">Users</a>
         <i class="fa fa-angle-right"></i>
      </li>
      <li>
         <a href="#"><?php
   if(isset($add) && $add == 1){
      echo "Add New User";
   }else if(isset($edit) && $edit == 1){
      echo "Edit User";
   }
   ?></a>
      </li>
   </ul>
   
</div>
<!-- END PAGE HEADER-->

<!-- Messages -->
<div class="note note-success" id="success_message">
   <p>
      <?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); } ?>
   </p>
</div>
<!-- Messages -->  

<!-- BEGIN PAGE CONTENT-->
<div class="row">
   <div class="col-md-12">
      <!-- BEGIN SAMPLE FORM PORTLET-->
      <div class="portlet box green">
         <div class="portlet-title">
            <div class="caption">
               <i class="fa fa-gift"></i> User Form
            </div>
            <div class="tools">
               <a href="" class="collapse">
               </a>

            </div>
         </div>
         <div class="portlet-body form">
            <form role="form" id="user_form" method="post" action="<?php echo base_url('admin/users/save_user'); ?>" <?php if ($is_validation) { echo "data-parsley-validate"; } ?>>
               <div class="form-body">

                  <?php
                  if(isset($edit) && $edit == 1){
                     ?>
                     <input type="hidden" name="user_id" value="<?php echo $user_info->id; ?>">
                     <?php
                  }
                  ?>

                  <div class="form-group">
                     <label>First Name</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="text" class="form-control input-sm" placeholder="First Name" name="first_name" id="first_name" value="<?php
                        if(isset($values['first_name']) && $values['first_name'] != '' && isset($add) && $add == 1){
                           echo $values['first_name'];
                        }
                        if(isset($user_info) && $user_info->first_name != ''){
                           echo $user_info->first_name;
                        }
                        ?>"
                        <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="First Name is required" data-parsley-first_name data-parsley-trigger="change" required';
                        }
                        ?>
                        />
                        <?php
                        if(isset($errors['first_name']) && $errors['first_name'] != ''){
                           echo $errors['first_name'];
                        }
                        ?>

                     </div>
                  </div>

                  <div class="form-group">
                     <label>Last Name</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="text" class="form-control input-sm" placeholder="Last Name" name="last_name" id="last_name" value="<?php
                        if(isset($values['last_name']) && $values['last_name'] != '' && isset($add) && $add == 1){
                           echo $values['last_name'];
                        }
                        if(isset($user_info) && $user_info->last_name != ''){
                           echo $user_info->last_name;
                        }
                        ?>" 
                        <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Last Name is required" data-parsley-last_name data-parsley-trigger="change" required';
                        }
                        ?>
                        />
                        <?php
                        if(isset($errors['last_name']) && $errors['last_name'] != ''){
                           echo $errors['last_name'];
                        }
                        ?>
                     </div>
                  </div>

                  <div class="form-group">
                     <label>Email</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="text" class="form-control input-sm" placeholder="Email" name="email" id="email" value="<?php
                        if(isset($values['email']) && $values['email'] != '' && isset($add) && $add == 1){
                           echo $values['email'];
                        }
                        if(isset($user_info) && $user_info->email != ''){
                           echo $user_info->email;
                        }
                        ?>" 
                        <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Email is required" data-parsley-email data-parsley-trigger="change" required';
                        }
                        ?>
                        />
                        <?php
                        if(isset($errors['email']) && $errors['email'] != ''){
                           echo $errors['email'];
                        }
                        ?>
                     </div>
                  </div>

                  <div class="form-group">
                     <label>Phone</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="text" class="form-control input-sm" placeholder="Phone" name="phone" id="phone" value="<?php
                        if(isset($values['phone']) && $values['phone'] != '' && isset($add) && $add == 1){
                           echo $values['phone'];
                        }
                        if(isset($user_info) && $user_info->phone != ''){
                           echo $user_info->phone;
                        }
                        ?>" 
                        <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Phone is required" required';
                        }
                        ?>
                        />
                        <?php
                        if(isset($errors['phone']) && $errors['phone'] != ''){
                           echo $errors['phone'];
                        }
                        ?>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label>Password</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="password" class="form-control input-sm" placeholder="Password" name="password" id="password" value="" 
                        <?php
                        if($is_validation && isset($add) && $add == 1){
                           echo 'data-parsley-required-message="Password is required" data-parsley-equalto="#conf_password" required';
                        }
                        ?>
                        />
                        <?php
                        if(isset($errors['password']) && $errors['password'] != ''){
                           echo $errors['password'];
                        }
                        ?>
                     </div>
                  </div>

                  <div class="form-group">
                     <label>Confirm Password</label>
                     <div class="input-icon input-icon-sm">
                        <i class="fa fa-bell-o"></i>
                        <input type="password" class="form-control input-sm" placeholder="Confirm Password" name="conf_password" id="conf_password" value="" />
                     </div>
                  </div>

                  <div class="form-group">
                     <label>Group</label>
                     <div class="input-icon input-icon-sm">
                        <select name="group_id" class="full-width"
                        <?php
                        if($is_validation){
                           echo 'data-parsley-required-message="Group is required" required';
                        }
                        ?>
                        >

                        <option value="">Select Group</option>

                        <?php
                        foreach ($all_group as $group) {
                           ?>
                           <option value="<?php echo $group->id; ?>" 
                              <?php 
                              if(isset($user_info) && $user_info->group_id != ''){
                                 if($user_info->group_id == $group->id){ echo "selected"; } 
                              }
                              ?>
                              ><?php echo $group->name; ?></option>
                              <?php
                           }
                           ?>
                        </select>
                     </div>
                  </div>

               </div>
               <div class="form-actions">
                  <button type="submit" name="<?php
                  if(isset($add) && $add == 1){
                     echo "add";
                  }else if(isset($edit) && $edit == 1){
                     echo "edit";
                  }
                  ?>" value="Save" class="btn btn-success">Submit</button>
                  <a href="<?php echo base_url('admin/users'); ?>" class="btn btn-danger">Cancel</a>

               </div>
            </form>
         </div>
      </div>
      <!-- END SAMPLE FORM PORTLET-->
      
      
   </div>

</div>
<?php $this->load->view('admin/includes/footer'); ?>

<script type="text/javascript">

   window.ParsleyValidator
   .addValidator('first_name', function(value, requirement) {
      first_name_regex = /^[a-zA-Z]+$/i;

      var flag = 0;

            /*if(value.length < requirement ){
             flag = 1;
          }*/

          if (!first_name_regex.test(value)) {
            flag = 1;
         }

         if (flag == 0) {
            return true;
         } else {
            return false;
         }

      }, 32)
   .addMessage('en', 'first_name', "Enter a valid First Name.");

   window.ParsleyValidator
   .addValidator('last_name', function(value, requirement) {
      last_name_regex = /^[a-zA-Z]+$/i;

      var flag = 0;

            /*if(value.length < requirement ){
             flag = 1;
          }*/

          if (!last_name_regex.test(value)) {
            flag = 1;
         }

         if (flag == 0) {
            return true;
         } else {
            return false;
         }

      }, 32)
   .addMessage('en', 'last_name', "Enter a valid Last Name.");


   window.ParsleyValidator
   .addValidator('username', function(value, requirement) {
      username_regex = /^[a-z\d\ \s]+$/i;

      var flag = 0;

            /*if(value.length < requirement ){
             flag = 1;
          }*/

          if (!username_regex.test(value)) {
           flag = 1;
        }

        if (flag == 0) {
           return true;
        } else {
           return false;
        }

     }, 32)
   .addMessage('en', 'username', "Enter a valid Username.");

   window.ParsleyValidator
   .addValidator('email', function(value, requirement) {
      email_regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;

      var flag = 0;

            /*if(value.length < requirement ){
             flag = 1;
          }*/

          if (!email_regex.test(value)) {
           flag = 1;
        }

        if (flag == 0) {
           return true;
        } else {
           return false;
        }

     }, 32)
   .addMessage('en', 'email', "Enter a valid Email.");

</script>

<!-- Success Message Container Js Start -->

<script type="text/javascript">

   <?php 
   if(!$this->session->flashdata('success_message')){
      ?>
      $("#success_message").hide();    
      <?php
   }
   ?>

   setTimeout(function(){
      $("#success_message").hide();        
   }, 3000);

   <?php 
   if(!$this->session->flashdata('failure_message')){
      ?>
      $("#failure_message").hide();    
      <?php
   }
   ?>

   setTimeout(function(){
      $("#failure_message").hide();        
   }, 3000);

   

</script>

<!-- Success Message Container Js End -->