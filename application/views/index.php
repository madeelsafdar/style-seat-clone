<?php $this->load->view('includes/header'); ?>
<!-- Banner Section Start -->
<div class="banner--section">
	<!-- Banner Slider Start -->
	<div class="banner--slider owl-carousel bg--gradient-theme" data-owl-dots="true">
		<!-- Banner Item Start -->
		<div class="banner--item">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="vc--parent">
							<div class="vc--child">
								<!-- Banner Content Start -->
								<div class="banner--content">
									<div class="header">
										<h1 class="h1">Your Complete Solution For <strong>Social Media Management</strong></h1>
									</div>

									<div class="body">
										<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, believable.</p>
									</div>

									<div class="buttons">
										<a href="#" class="btn btn-default">Learn More</a>
										<a href="contact.html" class="btn btn-default active">Contact Us</a>
									</div>
								</div>
								<!-- Banner Content End -->
							</div>
						</div>
					</div>

					<div class="col-sm-6">
						<div class="vc--parent">
							<div class="vc--child">
								<!-- Banner Image Start -->
								<div class="banner--img">
									<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/banner-img/slider-img-01.png" alt="">
								</div>
								<!-- Banner Image End -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Banner Item End -->

		<!-- Banner Item Start -->
		<div class="banner--item">
			<div class="container">
				<div class="row">
					<div class="col-sm-5">
						<div class="vc--parent">
							<div class="vc--child">
								<!-- Banner Image Start -->
								<div class="banner--img">
									<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/banner-img/slider-img-02.png" alt="">
								</div>
								<!-- Banner Image End -->
							</div>
						</div>
					</div>

					<div class="col-sm-7">
						<div class="vc--parent">
							<div class="vc--child">
								<!-- Banner Content Start -->
								<div class="banner--content">
									<div class="header">
										<h1 class="h1">Your Complete Solution For <strong>Social Media Management</strong></h1>
									</div>

									<div class="body">
										<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, believable.</p>
									</div>

									<div class="buttons">
										<a href="#" class="btn btn-default">Learn More</a>
										<a href="contact.html" class="btn btn-default active">Contact Us</a>
									</div>
								</div>
								<!-- Banner Content End -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Banner Item End -->
	</div>
	<!-- Banner Slider End -->
</div>
<!-- Banner Section End -->

<!-- About Section Start -->
<div class="about--section pd--100-0-40">
	<div class="container">
		<div class="row">
			<!-- About Content Start -->
			<div class="about--content col-md-6 pbottom--60">
				<!-- Section Title Start -->
				<div class="section--title section--title-left mbottom--40">
					<p>About Us</p>

					<h2 class="h2">We Are Social Media Management Company</h2>
				</div>
				<!-- Section Title End -->

				<div class="body">
					<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem.</p>

					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years</p>
				</div>

				<div class="buttons">
					<a href="about.html" class="btn btn-default active">Learn More</a>
					<a href="#" class="btn btn-default">Get A Quote</a>
				</div>
			</div>
			<!-- About Content End -->

			<!-- About Video Start -->
			<div class="about--video col-md-6 pbottom--60">
				<div class="inner">
					<a href="https://www.youtube.com/watch?v=2GqExKSwTEA" class="active" data-popup="video">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/about-img/video-poster.jpg" alt="">
						<i class="fa fa-play"></i>
					</a>
				</div>
			</div>
			<!-- About Video End -->
		</div>
	</div>
</div>
<!-- About Section End -->

<!-- Services Section Start -->
<div class="services--section pd--100-0 bg--color-lightgray">
	<div class="container">
		<!-- Section Title Start -->
		<div class="section--title mbottom--60">
			<p>Our Services</p>

			<h2 class="h2">What We Provide</h2>
		</div>
		<!-- Section Title End -->

		<div class="row AdjustRow">
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-01.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Strategy &amp; Consulting</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
			
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-02.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Social Media Management</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
			
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-03.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Social Media Campaign</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
			
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-04.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Social Media Advertising</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
			
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-05.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Pay-Per Click</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
			
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-06.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Branding &amp; Design</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
			
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-07.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Event Management</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>

			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Service Item Start -->
				<div class="service--item">
					<div class="icon">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/services-img/icon-08.png" alt="">
					</div>

					<div class="title">
						<h3 class="h4"><a href="service-details.html">Web Design &amp; Development</a></h3>
					</div>

					<div class="content">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
					</div>
				</div>
				<!-- Service Item End -->
			</div>
		</div>

		<div class="section--footer text-center">
			<a href="services.html" class="btn btn-default">More Services</a>
		</div>
	</div>
</div>
<!-- Services Section End -->

<!-- Pricing Table Section Start -->
<div class="pricing-table--section pd--100-0">
	<div class="container">
		<!-- Section Title Start -->
		<div class="section--title mbottom--60">
			<p>Pricing Table</p>

			<h2 class="h2">Best Pricing Plan</h2>
		</div>
		<!-- Section Title End -->

		<div class="row reset--gutter">
			<!-- Pricing Table Item Start -->
			<div class="pricing-table--item col-md-3 hidden-sm hidden-xs">
				<div class="header">
					<div class="vc--parent">
						<div class="vc--child">
							<h2 class="h3">Features of Package</h2>
						</div>
					</div>
				</div>

				<div class="features">
					<ul class="nav">
						<li><strong>Included Sites</strong></li>
						<li><strong>Steup Fees</strong></li>
						<li><strong>New Post in Everyday</strong></li>
						<li><strong>24/7 Customer Service</strong></li>
						<li><strong>Unique Content Creation</strong></li>
						<li><strong>Dedicated Account Manager</strong></li>
						<li><strong>Fan Comments Responding</strong></li>
						<li><strong>Analytic Reporting</strong></li>
					</ul>
				</div>
			</div>
			<!-- Pricing Table Item End -->

			<!-- Pricing Table Item Start -->
			<div class="pricing-table--item col-md-3 col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2 col-xxs-12">
				<div class="header">
					<div class="vc--parent">
						<div class="vc--child">
							<h2 class="h3">Starter</h2>
							<h3 class="h2"><sup>$</sup>109.99<small>/mo</small></h3>
						</div>
					</div>
				</div>

				<div class="features">
					<ul class="nav">
						<li data-label="Included Sites">
							<ul class="social">
								<li><i class="fa fa-facebook"></i></li>
							</ul>
						</li>
						<li data-label="Steup Fees">$19.99</li>
						<li data-label="New Post in Everyday">01 Post</li>
						<li data-label="24/7 Customer Service"><i class="fa fa-check"></i></li>
						<li data-label="Unique Content Creation">10 Post</li>
						<li data-label="Dedicated Account Manager"><i class="fa fa-check"></i></li>
						<li data-label="Fan Comments Responding"><i class="fa fa-remove"></i></li>
						<li data-label="Analytic Reporting">01/Week</li>
					</ul>
				</div>

				<div class="footer">
					<a href="#" class="btn btn-default">Get Starded</a>
				</div>
			</div>
			<!-- Pricing Table Item End -->

			<!-- Pricing Table Item Start -->
			<div class="pricing-table--item col-md-3 col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2 col-xxs-12">
				<div class="header">
					<div class="vc--parent">
						<div class="vc--child">
							<h2 class="h3">Standard</h2>
							<h3 class="h2"><sup>$</sup>179.99<small>/mo</small></h3>
						</div>
					</div>
				</div>

				<div class="features">
					<ul class="nav">
						<li data-label="Included Sites">
							<ul class="social">
								<li><i class="fa fa-facebook"></i></li>
								<li><i class="fa fa-twitter"></i></li>
								<li><i class="fa fa-google-plus"></i></li>
							</ul>
						</li>
						<li data-label="Steup Fees">$39.99</li>
						<li data-label="New Post in Everyday">04 Post</li>
						<li data-label="24/7 Customer Service"><i class="fa fa-check"></i></li>
						<li data-label="Unique Content Creation">30 Post</li>
						<li data-label="Dedicated Account Manager"><i class="fa fa-check"></i></li>
						<li data-label="Fan Comments Responding"><i class="fa fa-check"></i></li>
						<li data-label="Analytic Reporting">02/Week</li>
					</ul>
				</div>

				<div class="footer">
					<a href="#" class="btn btn-default">Get Starded</a>
				</div>
			</div>
			<!-- Pricing Table Item End -->

			<!-- Pricing Table Item Start -->
			<div class="pricing-table--item col-md-3 col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2 col-xxs-12">
				<div class="header">
					<div class="vc--parent">
						<div class="vc--child">
							<h2 class="h3">Business</h2>
							<h3 class="h2"><sup>$</sup>249.99<small>/mo</small></h3>
						</div>
					</div>
				</div>

				<div class="features">
					<ul class="nav">
						<li data-label="Included Sites">
							<ul class="social">
								<li><i class="fa fa-facebook"></i></li>
								<li><i class="fa fa-twitter"></i></li>
								<li><i class="fa fa-google-plus"></i></li>
							</ul>
						</li>
						<li data-label="Steup Fees">$59.99</li>
						<li data-label="New Post in Everyday">10 Post</li>
						<li data-label="24/7 Customer Service"><i class="fa fa-check"></i></li>
						<li data-label="Unique Content Creation">100 Post</li>
						<li data-label="Dedicated Account Manager"><i class="fa fa-check"></i></li>
						<li data-label="Fan Comments Responding"><i class="fa fa-check"></i></li>
						<li data-label="Analytic Reporting">03/Week</li>
					</ul>
				</div>

				<div class="footer">
					<a href="#" class="btn btn-default">Get Starded</a>
				</div>
			</div>
			<!-- Pricing Table Item End -->
		</div>
	</div>
</div>
<!-- Pricing Table Section End -->

<!-- Gallery Section Start -->
<div class="gallery--section pd--100-0 bg--color-lightgray">
	<div class="container">
		<!-- Section Title Start -->
		<div class="section--title mbottom--40">
			<p>Work</p>

			<h2 class="h2">Our Recent Case Studies</h2>
		</div>
		<!-- Section Title End -->

		<!-- Gallery Filter Menu Start -->
		<div class="gallery--filter-menu">
			<ul class="nav">
				<li data-target="*" class="active">All</li>
				<li data-target="strategy">Strategy</li>
				<li data-target="management">Management</li>
				<li data-target="campaign">Campaign</li>
				<li data-target="advertising">Advertising</li>
				<li data-target="ppc">Pay-Per Click</li>
				<li data-target="branding-designing">Branding &amp; Designing</li>
				<li data-target="event-management">Event Management</li>
				<li data-target="web-design-dev">Web Design &amp; Development</li>
			</ul>
		</div>
		<!-- Gallery Filter Menu End -->

		<!-- Gallery Items Start -->
		<div class="gallery--items row">
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="strategy management branding-designing">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-01.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Strategy &amp; Consultant</h3>

								<ul class="nav">
									<li>Social</li>
									<li>Management</li>
									<li>Branding</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
			
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="campaign advertising web-design-dev">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-02.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Campaign &amp; Advertising</h3>

								<ul class="nav">
									<li>Campaign</li>
									<li>Advertising</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
			
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="ppc branding-designing web-design-dev">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-03.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Pay-Per Click &amp; Branding</h3>

								<ul class="nav">
									<li>Pay-Per Click</li>
									<li>Branding</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
			
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="branding-designing event-management">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-04.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Branding &amp; Event Manag...</h3>

								<ul class="nav">
									<li>Branding</li>
									<li>Event Manag...</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->

			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="strategy management branding-designing">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-05.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Strategy &amp; Consultant</h3>

								<ul class="nav">
									<li>Social</li>
									<li>Management</li>
									<li>Branding</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
			
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="campaign advertising web-design-dev">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-06.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Campaign &amp; Advertising</h3>

								<ul class="nav">
									<li>Campaign</li>
									<li>Advertising</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
			
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="ppc branding-designing web-design-dev">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-07.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Pay-Per Click &amp; Branding</h3>

								<ul class="nav">
									<li>Pay-Per Click</li>
									<li>Branding</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
			
			<!-- Gallery Item Start -->
			<div class="gallery--item col-md-3 col-xs-6" data-cat="branding-designing event-management">
				<div class="gallery--img">
					<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/gallery-img/gallery-item-08.jpg" alt="">

					<div class="gallery--overlay">
						<div class="vc--parent">
							<div class="vc--child">
								<h3 class="h5">Branding &amp; Event Manag...</h3>

								<ul class="nav">
									<li>Branding</li>
									<li>Event Manag...</li>
								</ul>

								<a href="case-study-details.html" class="btn btn-default">View Details</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery Item End -->
		</div>
		<!-- Gallery Items End -->

		<!-- Section Footer Start -->
		<div class="section--footer text-center mtop--10">
			<a href="case-studies.html" class="btn btn-default">View All Case Studies</a>
		</div>
		<!-- Section Footer End -->
	</div>
</div>
<!-- Gallery Section End -->

<!-- Testimonial Section Start -->
<div class="testimonial--section pd--100-0-40 bg--overlay-90" data-bg-img="img/testimonial-img/bg.jpg">
	<div class="container">
		<!-- Section Title Start -->
		<div class="section--title mbottom--60">
			<p>Honesty is Our Capital</p>

			<h2 class="h2">Reviews &amp; Clients</h2>
		</div>
		<!-- Section Title End -->

		<div class="row">
			<div class="col-md-6 pbottom--60">
				<!-- Testimonial Slider Start -->
				<div class="testimonial--slider owl-carousel" data-owl-dots="true">
					<!-- Testimonial Item Start -->
					<div class="testimonial--item">
						<div class="header clearfix">
							<div class="img float--left">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/client-01.jpg" alt="">
							</div>

							<div class="info">
								<h3 class="h4">John Doe</h3>

								<p>Businessman, Toronto, Canada</p>

								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</div>
							</div>
						</div>

						<div class="body">
							<blockquote>
								<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like rea dable Lorem Ipsum available, but the majority have suffered alteration</p>
							</blockquote>
						</div>
					</div>
					<!-- Testimonial Item End -->

					<!-- Testimonial Item Start -->
					<div class="testimonial--item">
						<div class="header clearfix">
							<div class="img float--left">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/client-02.jpg" alt="">
							</div>

							<div class="info">
								<h3 class="h4">Denise Gomez</h3>

								<p>Businessman, Toronto, Canada</p>

								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</div>
							</div>
						</div>

						<div class="body">
							<blockquote>
								<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like rea dable Lorem Ipsum available, but the majority have suffered alteration</p>
							</blockquote>
						</div>
					</div>
					<!-- Testimonial Item End -->

					<!-- Testimonial Item Start -->
					<div class="testimonial--item">
						<div class="header clearfix">
							<div class="img float--left">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/client-03.jpg" alt="">
							</div>

							<div class="info">
								<h3 class="h4">Brandon Bradley</h3>

								<p>Businessman, Toronto, Canada</p>

								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</div>
							</div>
						</div>

						<div class="body">
							<blockquote>
								<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like rea dable Lorem Ipsum available, but the majority have suffered alteration</p>
							</blockquote>
						</div>
					</div>
					<!-- Testimonial Item End -->
				</div>
				<!-- Testimonial Slider End -->
			</div>

			<div class="col-md-6 pbottom--60">
				<!-- Testimonial Slider Start -->
				<div class="testimonial--slider owl-carousel" data-owl-dots="true" data-owl-margin="30">
					<!-- Testimonial Brands Start -->
					<div class="testimonial--brands">
						<div class="row">
							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-01.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-02.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-03.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-04.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-05.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-06.jpg" alt="">
							</div>
						</div>
					</div>
					<!-- Testimonial Brands End -->

					<!-- Testimonial Brands Start -->
					<div class="testimonial--brands">
						<div class="row">
							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-01.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-02.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-03.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-04.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-05.jpg" alt="">
							</div>

							<div class="col-xs-4 col-xxs-6">
								<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/testimonial-img/brand-06.jpg" alt="">
							</div>
						</div>
					</div>
					<!-- Testimonial Brands End -->
				</div>
				<!-- Testimonial Slider End -->
			</div>
		</div>
	</div>
</div>
<!-- Testimonial Section End -->

<!-- FAQ Section Start -->
<div class="faq--section pd--100-0-40 bg--color-lightgray">
	<div class="container">
		<div class="row">
			<!-- FAQ Content Start -->
			<div class="faq--content col-md-6 pbottom--60">
				<!-- Section Title Start -->
				<div class="section--title section--title-left mbottom--60">
					<p>Needs of SMM</p>

					<h2 class="h2">Why Do You Need Social Media Management</h2>
				</div>
				<!-- Section Title End -->

				<!-- FAQ Items Start -->
				<div class="faq--items panel-group" id="faqItems">
					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title" data-toggle="collapse" data-target="#faqItem01" data-parent="#faqItems">
								<span>Your customers are on social media.</span>
							</h3>
						</div>

						<div id="faqItem01" class="panel-collapse collapse in">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem02" data-parent="#faqItems">
								<span>People are talking about your company on social media.</span>
							</h3>
						</div>

						<div id="faqItem02" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem03" data-parent="#faqItems">
								<span>There are people searching for your company.</span>
							</h3>
						</div>

						<div id="faqItem03" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem04" data-parent="#faqItems">
								<span>Stay Ahead of Trends</span>
							</h3>
						</div>

						<div id="faqItem04" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem05" data-parent="#faqItems">
								<span>Establish Your Business as an Authority</span>
							</h3>
						</div>

						<div id="faqItem05" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem06" data-parent="#faqItems">
								<span>Increase Social Media Engagement</span>
							</h3>
						</div>

						<div id="faqItem06" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem07" data-parent="#faqItems">
								<span>Manage and Track Campaigns</span>
							</h3>
						</div>

						<div id="faqItem07" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->

					<!-- FAQ Item Start -->
					<div class="faq--item panel">
						<div class="panel-heading">
							<h3 class="panel-title collapsed" data-toggle="collapse" data-target="#faqItem08" data-parent="#faqItems">
								<span>Attract New Customers</span>
							</h3>
						</div>

						<div id="faqItem08" class="panel-collapse collapse">
							<div class="panel-body">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
					<!-- FAQ Item End -->
				</div>
				<!-- FAQ Items End -->
			</div>
			<!-- FAQ Content End -->

			<!-- FAQ Form Start -->
			<div class="faq--form col-md-6 pbottom--60">
				<form action="forms/faq-form.php" data-form="ajax">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<h2 class="h4">Fill the form below and we'll contact you shortly.</h2>
						</div>
					</div>

					<div class="status"></div>

					<div class="row">
						<div class="col-xs-6 col-xxs-12">
							<div class="form-group">
								<label>
									<span>Website</span>
									<input type="text" name="website" class="form-control" required>
								</label>
							</div>
						</div>

						<div class="col-xs-6 col-xxs-12">
							<div class="form-group">
								<label>
									<span>Name</span>
									<input type="text" name="name" class="form-control" required>
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-6 col-xxs-12">
							<div class="form-group">
								<label>
									<span>E-mail</span>
									<input type="email" name="email" class="form-control" required>
								</label>
							</div>
						</div>

						<div class="col-xs-6 col-xxs-12">
							<div class="form-group">
								<label>
									<span>Phone</span>
									<input type="text" name="phone" class="form-control" required>
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-6 col-xxs-12">
							<div class="form-group">
								<label>
									<span>Company</span>
									<input type="text" name="company" class="form-control" required>
								</label>
							</div>
						</div>

						<div class="col-xs-6 col-xxs-12">
							<div class="form-group">
								<label>
									<span>Budget For This Project</span>
									<input type="text" name="budget" class="form-control" required>
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>
							<span>Put Your Ideas (Optional)</span>
							<textarea name="message" class="form-control"></textarea>
						</label>
					</div>

					<input type="hidden" name="submitType" value="ajax">

					<button type="submit" class="btn btn-block btn-default">Hear From An Expert</button>
				</form>
			</div>
			<!-- FAQ Form End -->
		</div>
	</div>
</div>
<!-- FAQ Section End -->

<!-- Team Section Start -->
<div class="team--section pd--100-0">
	<div class="container">
		<!-- Section Title Start -->
		<div class="section--title mbottom--60">
			<p>Team</p>

			<h2 class="h2">We Are Awesome Team</h2>
		</div>
		<!-- Section Title End -->

		<div class="row">
			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Team Member Start -->
				<div class="team--member">
					<div class="img">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-01-front.jpg" alt="" class="front">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-01-back.jpg" alt="" class="back">
					</div>

					<div class="info">
						<h3 class="h4">Lauren Gordon</h3>

						<p>CEO, Co-Founder</p>

						<ul class="social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- Team Member End -->
			</div>

			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Team Member Start -->
				<div class="team--member">
					<div class="img">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-02-front.jpg" alt="" class="front">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-02-back.jpg" alt="" class="back">
					</div>

					<div class="info">
						<h3 class="h4">Mary Hall</h3>

						<p>CEO, Co-Founder</p>

						<ul class="social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- Team Member End -->
			</div>

			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Team Member Start -->
				<div class="team--member">
					<div class="img">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-03-front.jpg" alt="" class="front">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-03-back.jpg" alt="" class="back">
					</div>

					<div class="info">
						<h3 class="h4">Brenda Shaw</h3>

						<p>CEO, Co-Founder</p>

						<ul class="social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- Team Member End -->
			</div>

			<div class="col-md-3 col-xs-6 col-xxs-12">
				<!-- Team Member Start -->
				<div class="team--member">
					<div class="img">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-04-front.jpg" alt="" class="front">
						<img src="<?php echo base_url(); ?>public/themes/front/html-template/img/team-img/member-04-back.jpg" alt="" class="back">
					</div>

					<div class="info">
						<h3 class="h4">Denise Munoz</h3>

						<p>CEO, Co-Founder</p>

						<ul class="social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- Team Member End -->
			</div>
		</div>

		<div class="section--footer mtop--10 text-center">
			<a href="team.html" class="btn btn-default">View All Members</a>
		</div>
	</div>
</div>
<!-- Team Section End -->

<!-- Subscribe Section Start -->
<div class="subscribe--section pd--100-0 bg--overlay-90" data-bg-img="img/subscribe-img/bg.jpg">
	<div class="container">
		<!-- Section Title Start -->
		<div class="section--title mbottom--60">
			<p>Newsletter</p>

			<h2 class="h2">Sign Up To Get Newsletter</h2>
		</div>
		<!-- Section Title End -->

		<!-- Subscribe Form Start -->
		<div class="subscribe--form">
			<p>We will never spam you, or sell your email to third parties.</p>
			<p>By clicking "Subcribe Now" you accept our Terms of Use and Privacy Policy.</p>

			<form action="https://themelooks.us13.list-manage.com/subscribe/post?u=79f0b132ec25ee223bb41835f&amp;id=f4e0e93d1d" method="post" name="mc-embedded-subscribe-form" target="_blank" data-form="validate">
				<div class="input-group">
					<input type="email" name="EMAIL" placeholder="Enter Your E-mail Address" class="form-control" autocomplete="off" required>

					<div class="input-group-btn">
						<button type="submit"><i class="fa fa-send"></i></button>
					</div>
				</div>
			</form>
		</div>
		<!-- Subscribe Form End -->
	</div>
</div>
<!-- Subscribe Section End -->
<?php $this->load->view('includes/footer'); ?>