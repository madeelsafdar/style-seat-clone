<?php $this->load->view('includes/header'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row row-wrap">

				<div class="col-md-12" style="margin:0;">
					
					<div class="col-md-12" style="paddng:0;">
						<h2>My Images</h2>
					</div>

					<div class="col-md-12" style="padding:0;">
						<hr style="border: 1px solid #bcbaba;margin-top: 5px;margin-bottom: 10px;">
					</div>
					
				</div>

				

				<?php
				foreach ($all_my_images as $key => $my_image) {
					?>
					<div class="col-md-4">
						<h5><?php echo $my_image['picture_title']; ?></h5>
						<!-- HOVER IMAGE -->
						<a class="hover-img" href="<?php echo base_url('gallery/view_image').'/'.$my_image['picture_id']; ?>">
							<img onerror="this.src='https://placeholdit.imgix.net/~text?txtsize=70&txt=Image%20not%20available&w=800&h=600'" src="<?php echo base_url(); ?>public/pictures_uploaded/gallery_<?php echo $my_image['gallery_id']; ?>/<?php echo $my_image['file_name']; ?>" alt="Image Alternative text" title="Old No7">
							<!-- <div class="hover-title">Posuere per sit</div> -->
						</a>
					</div>
					<?php
				}
				?>

				
			</div>
			<div class="gap gap-small"></div>
		</div>
	</div>

</div>
<?php $this->load->view('includes/footer'); ?>