<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * My Controller
 *
 * @product name	Your Project Name
 * @category		Core Controller
 * @author			Sajjad Mahmood
 * @description		Every controller must need to extends with this controller. It will load some common data from database 
 */

class MY_Controller extends CI_Controller {

	//Class-wide variable to store stats line
	protected $mSettingsData;
	
	public function __construct() {
		parent::__construct();

		

		$this->load->model('email_template_model');
	}
	
	public function send_email($sendto='', $email_template_id='', $parse_data=''){
		//$this->send_email_model->send_email($sendto, $email_template_id, $parse_data);
		
		$add_user_email_template = $this->email_template_model->get_email_template_by_id($email_template_id);

		$this->load->library('email');
		$config['charset'] = 'utf-8';
		$config['protocol'] = 'smtp';  
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->load->library('parser');

		$allowed_tag_words=$add_user_email_template->allowed_tag_words;
		$delete_these = array("{", "}", " ");
		$allowed_tag_words = str_replace($delete_these, "", $allowed_tag_words);
		$allowed_tag_words_array=explode(',', $allowed_tag_words);

		$data =  array();
		foreach ($allowed_tag_words_array as $key => $allowed_tag_word) {

			if(isset($parse_data[$allowed_tag_word])&&$parse_data[$allowed_tag_word]!=""){
				$data[$allowed_tag_word]=$parse_data[$allowed_tag_word];
			}
			else
			{
				$data[$allowed_tag_word]="";
			}
		}

		$message = $this->parser->parse_string($add_user_email_template->email_template_content, $data, TRUE);
		$sender_name_email = $this->email_template_model->get_sender_name_email();


		$sender_name = $sender_name_email->sender_name;
		$sender_email = $sender_name_email->sender_email;

// Always set content-type when sending HTML email

$headers = 'From: ' . $sender_name . ' <' . $sender_email . ">\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";



mail($sendto,$add_user_email_template->email_template_subject,$message,$headers);

		// $this->email->from($sender_name, $sender_email);
		// $this->email->to($sendto);

		// $this->email->subject($add_user_email_template->email_template_subject);
		// $this->email->message($message);

		//  $this->email->send();
		//   $this->email->print_debugger();
		 
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/helpers/MY_Controller.php */