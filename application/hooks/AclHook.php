<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AclHook{
	
	private $ci;
	
	public function __construct(){
		$this->ci = &get_instance();
	}

	public function exec(){
		$query = "SELECT 
		settings.setting_name,
		settings.setting_value
		FROM settings ";
		$result = $this->ci->db->query($query);
		$all_settings = $result->result_array();

		$all_settings_array = array();
		foreach ($all_settings as $setting) {
			$all_settings_array[$setting['setting_name']] = $setting['setting_value'];
		}

		$GLOBALS['all_settings'] = $all_settings_array;

		$query = "SELECT 
		advertisements.*
		FROM advertisements";
		$result = $this->ci->db->query($query);
		$all_advertisements = $result->result_array();

		$all_advertisements_array = array();
		foreach ($all_advertisements as $advertisement) {
			$all_advertisements_array[] = '<div style="width: 148px;height: 160px;background: #333333;overflow: hidden;">'.$advertisement['advertisement_text'].'</div>';
		}

		$GLOBALS['all_advertisements'] = $all_advertisements_array;
		$GLOBALS['no_of_advertisements'] = count($all_advertisements_array);


		$query = "SELECT 
		categories.category_id,
		categories.category_title
		FROM categories ";
		$result = $this->ci->db->query($query);
		$all_categories = $result->result_array();

		$all_categories_array = array();
		$count = 0;
		foreach ($all_categories as $category) {
			$all_categories_array[$count]['category_title'] = $category['category_title'];
			$all_categories_array[$count]['category_id'] = $category['category_id'];
			$count++;
		}

		$GLOBALS['all_categories'] = $all_categories_array;
	}

}