<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class acl_hook{
	
	private $ci;
	
	public function __construct(){
		$this->ci = &get_instance();
	}

	public function exec(){


		$this->ci->load->model('action_model');
		$paths = $this->ci->action_model->get_all_actions();

		if($this->ci->ion_auth->logged_in()){
			$user = $this->ci->ion_auth->user()->row();
			setUser('username', $user->username);
			setUser('id', $user->id);
			setUser('email', $user->email);
			setUser('admin', $this->ci->ion_auth->is_admin());
			setUser('logged', true);
			$groupUser = $this->ci->ion_auth->get_users_groups(getUser('id'))->result();
			$group = array();
			foreach($groupUser as $tmp) 
				$group[] = $tmp->id;
			setUser('groups',$group);

		}else{
			setUser('logged', false);
		}

		if($this->ci->uri->segment(1) != "auth" && $this->ci->uri->segment(1) != "" && ( $this->ci->uri->segment(1) != "general" && $this->ci->uri->segment(2) != "view_mockup") ){
			if($this->ci->ion_auth->logged_in()){

				$controller = $this->ci->uri->segment(1);

				if($this->ci->uri->segment(2)){
					$function = $this->ci->uri->segment(2);
				}else{
					$function = 'index';
				}

				if (in_array($controller.'/'.$function, $paths)) {
					if(!$this->ci->acl->isAllow($controller,$function)){
						// show_404();
						 redirect('my404', 'refresh');
					}		
				}
			}else{
				$this->ci->session->set_flashdata('message', "You are not currently logged in");
				redirect("auth/login/redirect");
			}
		}

$query = "SELECT 
						settings.setting_name,
						settings.setting_value
					FROM settings ";
			$result = $this->ci->db->query($query);
			$all_settings = $result->result_array();

			$all_settings_array = array();
			foreach ($all_settings as $setting) {
				$all_settings_array[$setting['setting_name']] = $setting['setting_value'];
			}

			 $GLOBALS['all_settings'] = $all_settings_array;




			 $query = "SELECT 
						settings.setting_name,
						settings.setting_value
					FROM settings ";
			$result = $this->ci->db->query($query);
			$all_settings = $result->result_array();

			$all_settings_array = array();
			foreach ($all_settings as $setting) {
				$all_settings_array[$setting['setting_name']] = $setting['setting_value'];
			}

			 $GLOBALS['topnav_settings'] = $all_settings_array;


	}

}