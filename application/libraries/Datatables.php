<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datatables
{
	/*
	 * $sTable: Source table in database.
	 * $aColumns: Columns in source table.
	 * $idColumn: The ID column within columns.
	 * $useNumbering: The flag used to indicate the use of numbering for query results.
	 */
	public function getData($sTable, $aColumns, $idColumn, $useNumbering, $join_array, $join_col_names, $where_statements)
	{
		// Loads CodeIgniter's Database Configuration
		$CI =& get_instance();
		$CI->load->database();
		
		// Paging
		if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
		{
			$CI->db->limit($CI->db->escape_str($_GET['iDisplayLength']), $CI->db->escape_str($_GET['iDisplayStart']));
		}
		
		// Ordering
		if(isset($_GET['iSortCol_0']))
		{
			for($i=0; $i<intval($_GET['iSortingCols']); $i++)
			{
				if($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == 'true')
				{
					

					// if (strpos($aColumns[intval($CI->db->escape_str($_GET['iSortCol_'.$i]))],' as ') !== false) {
					// 	$order_by_pieces = explode(" as ", $aColumns[intval($CI->db->escape_str($_GET['iSortCol_'.$i]))]);
					// 	var_dump($order_by_pieces);
					// 	// $order_by_str = 'adeel';
					// }else{
						$order_by_str = $aColumns[intval($CI->db->escape_str($_GET['iSortCol_'.$i]))];
					// }

					$CI->db->order_by($order_by_str, $CI->db->escape_str($_GET['sSortDir_'.$i]));
				}
			}
		}
		
		// Individual column filtering
		if(isset($_GET['sSearch']) && !empty($_GET['sSearch']))
		{
			for($i=0; $i<count($aColumns); $i++)
			{
				if(isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true')
				{
					$CI->db->or_like($aColumns[$i], $CI->db->escape_like_str($_GET['sSearch']));
				}
			}
		}

		//Adeel Addition for Ssreach..Start
		
		$no_of_columns = $_GET['iColumns'];

		for ($i=0; $i < $no_of_columns ; $i++) { 
			if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i] != '' && $_GET['bSearchable_'.$i] == 'true'){
				$CI->db->or_like($aColumns[$i], $CI->db->escape_like_str($_GET['sSearch_'.$i]));
			}
		}
		//Adeel Addition for Ssreach..End

		//Adeel Addition for Join Start

		if (!empty($join_array)) {
			foreach ($join_array as $table => $join_statement) {
				$CI->db->join($table, $join_statement, 'left');
			}
		}

		//Adeel Addition for Join End

		//Adeel Addition for where Start

		if (!empty($where_statements)) {
			foreach ($where_statements as $where_statement) {
				$CI->db->where($where_statement['column']." ".$where_statement['operator'], $where_statement['value']); 
			}
		}

		//Adeel Addition for where End

		// June 14, 2014:
		// Minor tweak due to the difference between the number of columns between the table in the database and the datatable shown in the UI when using numbering
		if ($useNumbering)
		{
			array_shift($aColumns);
		}
		// Select data
		$CI->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $CI->db->get($sTable);
		
		// Data set length after filtering
		$CI->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $CI->db->get()->row()->found_rows;
		
		// Total data set length
		$iTotal = $CI->db->count_all($sTable);
		
		// Output
		$output = array(
			'sEcho' => intval($_GET['sEcho']),
			'iTotalRecords' => $iTotal,
			'iTotalDisplayRecords' => $iFilteredTotal,
			'aaData' => array()
			);

		// print_r($rResult->result_array());
		
		$num = 1; // Used for numbering query results
		foreach($rResult->result_array() as $aRow)
		{
			$row = array();
			
			if ($useNumbering)
			{
				$row[] = $num."";
				$num++;
			}

			if(!empty($join_array)){
				foreach($join_col_names as $col)
				{
					$row[] = $aRow[str_replace('.','_',$col)];
					if ($col == $idColumn) { $row['DT_RowId'] = $aRow[$col]; } 
				}
			}else{
				
				foreach($aColumns as $col)
				{
					// echo $col;
					$row[] = $aRow[str_replace('.','_',$col)];
					if ($col == $idColumn) { $row['DT_RowId'] = $aRow[$col]; } 
				}
			}



			$output['aaData'][] = $row;
		}

		// echo "<pre>";
		// print_r($output);
		// die;

		return json_encode($output);
	}
}
?>