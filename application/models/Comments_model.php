<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class comments_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

	}

	public function get_comment_of_picture($image_id = '')
	{
		$query = "SELECT *,comments.created_at as comment_creation_datetime FROM comments INNER JOIN users ON(users.id = comments.user_id) WHERE picture_id = $image_id ORDER BY created_at DESC";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function get_comment_of_gallery($gallery_id = '')
	{
		$query = "SELECT *,gallery_comments.created_at as comment_creation_datetime FROM gallery_comments INNER JOIN users ON(users.id = gallery_comments.user_id) WHERE gallery_id = $gallery_id ORDER BY created_at DESC";
		$result = $this->db->query($query);
		return $result->result_array();
	}

}