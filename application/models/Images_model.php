<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class images_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

	}

	public function get_image_info($image_id = '')
	{
		$query = "SELECT pictures.*, 
		(SELECT COUNT(picture_votes1.picture_votes_id) FROM picture_votes AS picture_votes1 
			WHERE picture_votes1.picture_id = pictures.picture_id AND picture_votes1.is_up = 1) AS vote_up, 
(SELECT COUNT(picture_votes2.picture_votes_id) FROM picture_votes AS picture_votes2 
	WHERE picture_votes2.picture_id = pictures.picture_id AND picture_votes2.is_up = 0) AS vote_down, 
(SELECT GROUP_CONCAT(image_categories.category_id,',',categories.category_title SEPARATOR '|') FROM image_categories INNEr JOIN categories ON (categories.category_id = image_categories.category_id) WHERE picture_id = pictures.picture_id) AS categories_of_picture, 
(SELECT GROUP_CONCAT(image_categories.category_id SEPARATOR '|') FROM image_categories INNEr JOIN categories ON (categories.category_id = image_categories.category_id) WHERE picture_id = pictures.picture_id) AS categories_of_picture_ids 
FROM pictures 
WHERE pictures.picture_id = $image_id";
$result = $this->db->query($query);
return $result->row();
}

public function get_all_images($loaded_result = '',$items_per_load = '')
{
	$query = "SELECT pictures.*, 
	(SELECT COUNT(picture_votes1.picture_votes_id) FROM picture_votes AS picture_votes1 
		WHERE picture_votes1.picture_id = pictures.picture_id AND picture_votes1.is_up = 1) AS vote_up, 
(SELECT COUNT(picture_votes2.picture_votes_id) FROM picture_votes AS picture_votes2 
	WHERE picture_votes2.picture_id = pictures.picture_id AND picture_votes2.is_up = 0) AS vote_down 
FROM pictures 
ORDER BY created_at DESC LIMIT $loaded_result, $items_per_load";
		// $query = "SELECT * FROM pictures";
$result = $this->db->query($query);
return $result->result_array();
}

public function get_all_images_by_category($loaded_result = '',$items_per_load = '',$category_id = '')
{
	$query = "SELECT pictures.*, 
	(SELECT COUNT(picture_votes1.picture_votes_id) FROM picture_votes AS picture_votes1 
		WHERE picture_votes1.picture_id = pictures.picture_id AND picture_votes1.is_up = 1) AS vote_up, 
(SELECT COUNT(picture_votes2.picture_votes_id) FROM picture_votes AS picture_votes2 
	WHERE picture_votes2.picture_id = pictures.picture_id AND picture_votes2.is_up = 0) AS vote_down 
FROM pictures 
INNER JOIN image_categories
ON (image_categories.picture_id = pictures.picture_id)
WHERE image_categories.category_id = $category_id
ORDER BY created_at DESC LIMIT $loaded_result, $items_per_load";
		// $query = "SELECT * FROM pictures";
$result = $this->db->query($query);
return $result->result_array();
}

public function get_picture_views($picture_id = '')
{
	$query = "SELECT views FROM pictures WHERE picture_id = $picture_id";
	$result = $this->db->query($query);
	return $result->row();
}

public function check_if_favroute_before($picture_id = '')
{
	$user = $this->ion_auth->user()->row();
	$query = "SELECT favroute_picture_id FROM favroute_pictures WHERE picture_id = $picture_id AND user_id = $user->id";
	$result = $this->db->query($query);
	$result = $result->row();
	if ($result) {
		return $result->favroute_picture_id;
	}else{
		return false;
	}
	
}

public function get_next_image_id($picture_id = '')
{
	$query = "
	SELECT
	pictures.*
	FROM
	pictures
	WHERE
	gallery_id = (SELECT gallery_id FROM pictures WHERE picture_id = $picture_id)
	AND
	picture_id > $picture_id
	";

	$result = $this->db->query($query);
	return $result->row();
}

public function get_prev_image_id($picture_id = '')
{
	$query = "
	SELECT
	pictures.*
	FROM
	pictures
	WHERE
	gallery_id = (SELECT gallery_id FROM pictures WHERE picture_id = $picture_id)
	AND
	picture_id < $picture_id
	";

	$result = $this->db->query($query);
	return $result->row();
}

public function get_all_my_images()
{
	$user = $this->ion_auth->user()->row();
	$query = "SELECT * FROM pictures WHERE user_id = $user->id";
	$result = $this->db->query($query);
	return $result->result_array();
}

public function get_random_image()
{

	$query = "SELECT pictures.*, 
	(SELECT COUNT(picture_votes1.picture_votes_id) FROM picture_votes AS picture_votes1 
		WHERE picture_votes1.picture_id = pictures.picture_id AND picture_votes1.is_up = 1) AS vote_up, 
(SELECT COUNT(picture_votes2.picture_votes_id) FROM picture_votes AS picture_votes2 
	WHERE picture_votes2.picture_id = pictures.picture_id AND picture_votes2.is_up = 0) AS vote_down, 
(SELECT GROUP_CONCAT(image_categories.category_id,',',categories.category_title SEPARATOR '|') FROM image_categories INNEr JOIN categories ON (categories.category_id = image_categories.category_id) WHERE picture_id = pictures.picture_id) AS categories_of_picture, 
(SELECT GROUP_CONCAT(image_categories.category_id SEPARATOR '|') FROM image_categories INNEr JOIN categories ON (categories.category_id = image_categories.category_id) WHERE picture_id = pictures.picture_id) AS categories_of_picture_ids 
FROM pictures 
ORDER BY RAND() LIMIT 1";
$result = $this->db->query($query);
return $result->row();
}

public function get_related_images($image_id = '')
{
	$query = "
	SELECT GROUP_CONCAT(DISTINCT CONCAT(pictures.picture_id,',',pictures.file_name,',',pictures.picture_title,',',pictures.gallery_id) SEPARATOR '|') AS images_of_similar_category
	FROM pictures
	INNER JOIN image_categories
	ON (image_categories.picture_id = pictures.picture_id)
	WHERE image_categories.category_id IN (SELECT image_categories.category_id
		FROM pictures
		INNER JOIN image_categories
		ON (image_categories.picture_id = pictures.picture_id)
		WHERE pictures.picture_id = $image_id) LIMIT 10
";

$result = $this->db->query($query);
return $result->row();
}

}