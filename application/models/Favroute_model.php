<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class favroute_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

	}

	public function get_all_my_favroutes()
	{
		$user = $this->ion_auth->user()->row();
		$query = "SELECT * FROM favroute_pictures INNER JOIN pictures ON(pictures.picture_id = favroute_pictures.picture_id) WHERE favroute_pictures.user_id = $user->id";
		$result = $this->db->query($query);
		return $result->result_array();
	}

}