<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class advertisements_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

	}

	public function get_all_advertisements()
	{
		$query = "SELECT * FROM advertisements";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function get_advertisement_info_by_id($advertisement_id = '')
    {
        $query = "SELECT
        `advertisements`.*   
        FROM
        `advertisements`
        WHERE (`advertisements`.`advertisement_id` = $advertisement_id)";
        $result = $this->db->query($query);
        return $result->row();
    }

}
?>