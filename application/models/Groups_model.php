<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class groups_model extends CI_Model{

	function __construct()
    {
        parent::__construct();
    }


    public function get_all_groups()
    {
    	$query = "SELECT 
			 									 *
												 FROM 
												 groups WHERE id != 1 ORDER BY id DESC";

        $result = $this->db->query($query);
        return $result->result();
    }

    public function get_group_info($group_id = '')
    {
    	$query = "SELECT 
			 									 *
												 FROM 
												 groups WHERE id = $group_id  ORDER BY id DESC";

        $result = $this->db->query($query);
        return $result->row();
    }
	
    public function get_no_of_users_in_group($group_id = ''){
        $query = "SELECT
                        COUNT(`id`) AS no_of_users
                    FROM
                        `users_groups`
                    WHERE (`group_id` =$group_id)";
        $result = $this->db->query($query);

        return $result->row()->no_of_users;

    }

    public function get_users_in_a_group($group_id = '')
    {
        $query = "SELECT
                        users.id
                        ,users.email
                        ,users.first_name
                        ,users.last_name
                        ,users.phone
                    FROM
                        `users_groups`
                        INNER JOIN `users` 
                            ON (`users_groups`.`user_id` = `users`.`id`)
                    WHERE (`users_groups`.`group_id` = $group_id)";
        $result = $this->db->query($query);

        return $result->result();
    }

}