<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class general_model extends CI_Model{

    private $db_prefix;

	function __construct()
    {
        parent::__construct();
        $this->db_prefix = $this->db->dbprefix;
    }

    public function get_next_auto_id($table)
    {
    	$query = "SHOW TABLE STATUS LIKE '".$this->db_prefix."$table'";
        $result = $this->db->query($query);
    	$table_info = $result->row();

    	return $table_info->Auto_increment;
    }

    public function get_url_contents_mine() {

        // $url = $this->input->post('url');
        // $url = "http://www.w3schools.com/";
        $url = "http://www.vodly.to/";
        $proxyActivation=false;

        global $proxy;
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7");
        curl_setopt($c, CURLOPT_REFERER, $url);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        if($proxyActivation) {
            curl_setopt($c, CURLOPT_PROXY, $proxy);
        }
        $contents = curl_exec($c);
        curl_close($c);
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = true;
        @$dom->loadHTML($contents);
        $form = $dom->getElementsByTagName("body")->item(0);

        if($contents){
            echo $contents;
        }else{
            echo "";
        }

    }

}