<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class permissions_model extends CI_Model{

	function __construct()
	{
		parent::__construct();
	}

	public function get_group_info($group_id = '')
	{
		$query = "SELECT * FROM groups where id = $group_id";
		$result = $this->db->query($query);
		return $result->row();
	}


	public function get_all_modules()
	{
		$query = "SELECT 
		*
		FROM 
		acl_action";

		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function get_permissions_of_group($group_id = ''){

		$query1 = "SELECT 
					id		
					FROM 
					acl_action";

		$result1 = $this->db->query($query1);

		$all_modules_ids = array();
		foreach ($result1->result_array() as $module_id) {
			$all_modules_ids[] = $module_id['id'];
		}

		$query2 = '';

		foreach ($all_modules_ids as $module_id) {
			$query2 .= "SELECT '".$module_id."' AS id , IFNULL((SELECT 1 FROM group_action WHERE id_group = ".$group_id." AND id_action = ".$module_id."), 0) AS allowed UNION ";
		}

		$lastSpacePosition = strrpos($query2, ' ');
		$query2 = substr($query2, 0, $lastSpacePosition);

		$lastSpacePosition = strrpos($query2, ' ');
		$query2 = substr($query2, 0, $lastSpacePosition);

		$result2 = $this->db->query($query2);

		$permissions_of_group = array();
		foreach ($result2->result_array() as $value) {
			$permissions_of_group[$value['id']] = $value['allowed'];
		}

		return $permissions_of_group;

	}

	public function get_front_permissions_of_group($group_id = ''){

		$query1 = "SELECT 
					id		
					FROM 
					acl_action
					WHERE is_admin = 0";

		$result1 = $this->db->query($query1);

		$all_modules_ids = array();
		foreach ($result1->result_array() as $module_id) {
			$all_modules_ids[] = $module_id['id'];
		}

		$query2 = '';

		foreach ($all_modules_ids as $module_id) {
			$query2 .= "SELECT '".$module_id."' AS id , IFNULL((SELECT 1 FROM group_action WHERE is_admin = 0 id_group = ".$group_id." AND id_action = ".$module_id."), 0) AS allowed UNION ";
		}

		$lastSpacePosition = strrpos($query2, ' ');
		$query2 = substr($query2, 0, $lastSpacePosition);

		$lastSpacePosition = strrpos($query2, ' ');
		$query2 = substr($query2, 0, $lastSpacePosition);

		$result2 = $this->db->query($query2);

		$permissions_of_group = array();
		foreach ($result2->result_array() as $value) {
			$permissions_of_group[$value['id']] = $value['allowed'];
		}

		return $permissions_of_group;

	}
	
	public function get_allowed_module_ids_of_group($group_id='')
	{
		$query = "SELECT id_action from group_action WHERE id_group = $group_id";
		$result = $this->db->query($query);

		$module_ids_array = array();
		foreach ($result->result_array() as $module_id) {
			$module_ids_array[] = $module_id['id_action'];
		}

		return $module_ids_array;

	}

	public function get_paths_of_all_modules(){
		$query = "SELECT `path` FROM acl_action";
		$result = $this->db->query($query);
		return $result->result();
	}

}