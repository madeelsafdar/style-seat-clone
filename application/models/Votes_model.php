<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class votes_model extends CI_Model{

	function __construct()
    {
        parent::__construct();
    }

    public function getUserIP()
    {
         $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
    }

    public function check_if_voted_before($picture_id = '')
    {
        $ip_address = $this->getUserIP();
    	$query = "SELECT * FROM picture_votes WHERE picture_id = $picture_id AND ip_address = '$ip_address'";
    	$result = $this->db->query($query);
    	if($result->num_rows() > 0){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function check_if_voted_before_gallery($gallery_id = '')
    {
        $ip_address = $this->getUserIP();
        $query = "SELECT * FROM gallery_votes WHERE gallery_id = $gallery_id AND ip_address = '$ip_address'";
        $result = $this->db->query($query);
        if($result->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

}