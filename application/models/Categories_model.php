<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class categories_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

	}

	public function get_all_categories()
	{
		$query = "SELECT * FROM categories";
		$result = $this->db->query($query);
		return $result->result_array();
	}

}