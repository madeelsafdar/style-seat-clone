<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class users_model extends CI_Model{

	function __construct()
    {
        parent::__construct();
    }

    public function get_all_users_id_and_name()
    {
        $query_get_all_users_id_and_name = "SELECT 
        id,
        first_name,
        last_name
        FROM 
        users";

        $result_get_all_users_id_and_name = $this->db->query($query_get_all_users_id_and_name);
        return $result_get_all_users_id_and_name->result_array();
    }

    public function get_all_users_id_and_name_except_admin()
    {
    	$query_get_all_users_id_and_name_except_admin = "SELECT 
        id,
        first_name,
        last_name
        FROM 
        users";

        $result_get_all_users_id_and_name_except_admin = $this->db->query($query_get_all_users_id_and_name_except_admin);
        return $result_get_all_users_id_and_name_except_admin->result_array();
    }

    public function get_all_users_by_search_str($search_str = '')
    {
        $query_get_all_users_by_search_str = "SELECT 
        id,
        first_name,
        last_name
        FROM 
        users
        WHERE
        id != 1
        AND
        (CONCAT(first_name,' ',last_name) LIKE '%" . $search_str . "%')";

        $result_get_all_users_by_search_str = $this->db->query($query_get_all_users_by_search_str);
        return $result_get_all_users_by_search_str->result_array();
    }

    public function get_emails_by_user_id_array($ids_array = '')
    {

        if($ids_array != ''){
            $query = 'SELECT email
            FROM `users` 
            WHERE `id` IN (' . implode($ids_array, ', ') . ')';

            $result = $this->db->query($query);

            $emails = array();
            foreach ($result->result_array() as $email) {
                $emails[] = $email['email'];
            }

            return $emails;

        }

    }

    public function get_user_info_by_id_array($ids_array = '')
    {

        if($ids_array != ''){
            $query = 'SELECT 
            id,
            first_name,
            last_name,
            email
            FROM `users` 
            WHERE `id` IN (' . implode($ids_array, ', ') . ')';

            $result = $this->db->query($query);

            $user_info = array();
            $count = 0;
            foreach ($result->result_array() as $user) {
                $user_info[$count]['email'] = $user['email'];
                $user_info[$count]['id'] = $user['id'];
                $user_info[$count]['first_name'] = $user['first_name'];
                $user_info[$count]['last_name'] = $user['last_name'];
                $count++;
            }

            return $user_info;

        }

    }

    public function get_all_users_except_admin()
    {
        $query = "SELECT
        `email`
        , `first_name`
        , `id`
        , `username`
        , `last_name`
        FROM
        `users`
        WHERE
        id != 1 ORDER BY id DESC";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function get_user_info_by_id($user_id = '')
    {
        $query = "SELECT
        `users`.*   
        , `groups`.`id` AS `group_id`
        FROM
        `users_groups`
        INNER JOIN `groups` 
        ON (`users_groups`.`group_id` = `groups`.`id`)
        INNER JOIN `users` 
        ON (`users_groups`.`user_id` = `users`.`id`)
        WHERE (`users`.`id` = $user_id)";
        $result = $this->db->query($query);
        return $result->row();
    }

    public function get_no_of_projects_of_user($user_id = '')
    {
     $query = "SELECT
     COUNT(`project_id`) as no_of_projects
     FROM
     `users_projects`
     WHERE (`user_id` = $user_id)";
     $result = $this->db->query($query);
     return $result->row()->no_of_projects;
 }

 public function check_otp_password($otp_password = '',$jobseeker_mobile_number = '')
 {

    if($jobseeker_mobile_number != ''){
        $jobseeker_mobile_number = trim($jobseeker_mobile_number);
        $extra_cond = " AND  `mobile_number` LIKE '$jobseeker_mobile_number' ";
    }else{
        $extra_cond = "";
    }

    $query = "
    SELECT *
    FROM `jobseekers`
    WHERE `one_time_password` LIKE '$otp_password'
    $extra_cond
    ";

    $result = $this->db->query($query);

    if($result->row()){
        return $result->row();
    }else{
        return false;
    }
}
public function check_jobseeker_num($jobseeker_num="")
{

    if($jobseeker_num[0] == '+'){
        $substite = "00".ltrim($jobseeker_num, '+');
    }elseif ($jobseeker_num[0] == '0' && $jobseeker_num[1] == '0') {
        $substite = '+'.ltrim($jobseeker_num, '00');
    }

    $query = "
    SELECT
    mobile_number
    FROM
    jobseekers
    WHERE
    mobile_number = '$jobseeker_num'
    OR 
    mobile_number = '$substite'
    ";
    $result = $this->db->query($query);

    if($result->row()){
        return true;
    }else{
        return false;
    }
}


public function check_email($email="")
{
    $query = "
    SELECT
    email
    FROM
    users
    WHERE
    email = '$email'
    ";
    $result = $this->db->query($query);

    if($result->row()){
        return true;
    }else{
        return false;
    }
}

public function get_user_info($user_id='')
{

    $query = "CALL get_user_info($user_id)";

    $result = $this->db->query($query);

    mysqli_next_result( $this->db->conn_id );

    if($result->num_rows() > 0){
        return $result->row();
    }else{
        return false;
    }
}

public function check_email_unique_edit($email)
{
    $query = "
    SELECT
    id
    FROM
    users
    WHERE email = '$email'
    ";
    $result = $this->db->query($query);
    if($result->row()){
        return true;
    }else{
        return false;
    }
}


public function check_activation_code($user_id = '',$activation_code = '')
{
    $query = "SELECT * FROM users WHERE id = $user_id AND activation_code = '$activation_code'";
    $result = $this->db->query($query);
    if($result->row()){
        return true;
    }else{
        return false;
    }
}

public function check_user_by_identifier($identifier)
{
    $query = "SELECT * FROM users WHERE identifier = '$identifier'";
    $result = $this->db->query($query);
    if($result->row()){
        return $result->row();
    }else{
        return false;
    }

}

}