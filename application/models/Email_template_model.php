<?php

	
	class email_template_model extends CI_Model
	{
		
		public function get_all_email_templates()
    {
      $query = "
                SELECT
                *
                FROM
                email_templates
               ";
      $result = $this->db->query($query);
      $result = $result->result();
      return $result;
    }
	
	 public function get_sender_name_email(){

					$get_settings_query="SELECT
(SELECT setting_value FROM settings WHERE setting_id = 21)
AS sender_name,
(SELECT setting_value FROM settings WHERE setting_id = 28)
AS sender_email";
					 $settings=$this->db->query($get_settings_query);
					                          return $settings->row();
			}

    public function get_email_template_by_id($email_template_id='')
    {
      $query = "
                SELECT
                *
                FROM
                email_templates
                WHERE
                email_template_id =$email_template_id
               ";

      $result = $this->db->query($query);
      $result = $result->row();
      return $result;

    }
	}



?>