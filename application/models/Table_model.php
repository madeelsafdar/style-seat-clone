<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class table_model extends CI_Model {

	private $table = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->config('website');
	}

	public function get_all_foreign_keys($table = '')
	{
		$query = "SELECT
		`column_name`, 
		`referenced_table_schema` AS foreign_db, 
		`referenced_table_name` AS foreign_table, 
		`referenced_column_name`  AS foreign_column 
		FROM
		`information_schema`.`KEY_COLUMN_USAGE`
		WHERE
		`constraint_schema` = SCHEMA()
		AND
		`table_name` = '$table'
		AND
		`referenced_column_name` IS NOT NULL
		ORDER BY
		`column_name`";
		$result = $this->db->query($query); 
		return $result->result_array();
	}

	public function get_title_field($table = '')
	{

		$query = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = '$table'
		AND table_schema = '".$this->db->database."'
		AND COLUMN_NAME LIKE '%_title%' ";	
		$result = $this->db->query($query); 

		return $result->row();
	}

	public function get_field_names_without_primary_key($table = '')
	{

		$hide_record_ids = $this->config->item('hide_record_ids');

		if(isset($hide_record_ids) && $hide_record_ids != ''){
			$primary_key_condition = ' AND COLUMN_KEY != "PRI" ';
		}else{
			$primary_key_condition = '';
		}

		$query = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = '$table'
		AND table_schema = '".$this->db->database."'
		$primary_key_condition ";	
		$result = $this->db->query($query); 

		return $result->result_array();
	}

	public function get_field_names_with_primary_key($table = '')
	{

		$query = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = '$table'
		AND table_schema = '".$this->db->database."' ";	
		$result = $this->db->query($query); 

		return $result->result_array();
	}

	public function get_primary_key($table='')
	{
		$query = "SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'";
		$result = $this->db->query($query);
		$result = $result->row();
		return $result->Column_name;
	}

	public function get_all_tables()
	{
		$query = "SELECT * from INFORMATION_SCHEMA.TABLES WHERE table_schema = '".$this->db->database."'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function get_all_records($table = '')
	{
		$this->table = $table;
		$result = $this->db->get($table);
		return $result->result_array();
	}

	public function get_record_info_by_id($table = '',$record_id = '')
	{
		$query = "SELECT * from $table WHERE ".$this->get_primary_key($table)." = '".$record_id."'";
		$result = $this->db->query($query);
		return $result->row();
	}

	public function delete_record($table = '',$record_id = '')
	{
		$this->db->delete($table, array($this->get_primary_key($table) => $record_id)); 
	}

	public function change_record_status($table = '',$record_id = '')
	{
		$datestring = "Y-m-d h:i:s";
		$time = time();
		$query = "UPDATE $table SET status = !status, updated_at = '".date($datestring, $time)."' WHERE ".$this->get_primary_key($table)." = $record_id";
		$result = $this->db->query($query);
	}

	public function dt_get_records($table = '',$iDisplayLength = '',$iDisplayStart = '',$sSearch = '',$sSortDir_0 = '',$iSortCol_0 = '',$individual_search = '')
	{
		$this->table = $table;
		$all_fields = $this->get_field_names_without_primary_key($table);
		$all_foreign_keys = $this->get_all_foreign_keys($table);

		$query = 'SELECT '.$table.'.*';

		foreach ($all_foreign_keys as $foreign_key) {
			$title_field = $this->get_title_field($foreign_key['foreign_table']);
			$query .= ' ,'.$foreign_key['foreign_table'].'.'.$title_field->COLUMN_NAME;
		}

		$query .= ' FROM '.$table;
		$search_str = '';
		$order_str = '';

		foreach ($all_foreign_keys as $foreign_key) {
			$query .= ' LEFT JOIN '.$foreign_key['foreign_table'];
			$query .= ' ON('.$table.'.'.$foreign_key['column_name'].' = '.$foreign_key['foreign_table'].'.'.$foreign_key['foreign_column'].') ';
		}

		if($sSearch != ''){

			$search_str .= ' WHERE (';

				foreach ($all_fields as $field_info) {
					$search_str .= ' '.$table.'.'.$field_info['COLUMN_NAME'].' LIKE "%'.$sSearch.'%" OR';
				}

				$search_str = rtrim($search_str,'OR');
				$search_str .= ') ';
$query .= $search_str;
}






if($iSortCol_0 != 0){
	$order_str .= ' ORDER BY '.$table.'.'.$all_fields[$iSortCol_0-1]['COLUMN_NAME'].' '.$sSortDir_0.' ';
	$query .= $order_str;
}else{
	// $order_str .= ' ORDER BY '.$table.'.'.$this->get_primary_key($table).' '.$sSortDir_0.' ';
	$order_str .= ' ORDER BY '.$table.'.created_at'.' '.$sSortDir_0.' ';
	$query .= $order_str;
}

$query .= ' LIMIT '.$iDisplayStart.','.$iDisplayLength.' ';

$result = $this->db->query($query);
return $result->result_array();
}

public function dt_get_records_count($table = '',$sSearch = '',$sSortDir_0 = '',$iSortCol_0 = '' ,$individual_search = '')
{
	$this->table = $table;
	$all_fields = $this->get_field_names_without_primary_key($table);
	$all_foreign_keys = $this->get_all_foreign_keys($table);

	$query = 'SELECT '.$table.'.*';

	foreach ($all_foreign_keys as $foreign_key) {
		$query .= ' ,'.$foreign_key['foreign_table'].'.*';
	}

	$query .= ' FROM '.$table;
	$search_str = '';
	$order_str = '';

	foreach ($all_foreign_keys as $foreign_key) {
		$query .= ' LEFT JOIN '.$foreign_key['foreign_table'];
		$query .= ' ON('.$table.'.'.$foreign_key['column_name'].' = '.$foreign_key['foreign_table'].'.'.$foreign_key['foreign_column'].') ';
	}

	if($sSearch != ''){

		$search_str .= ' WHERE (';

			foreach ($all_fields as $field_info) {
				$search_str .= ' '.$table.'.'.$field_info['COLUMN_NAME'].' LIKE "%'.$sSearch.'%" OR';
			}

			$search_str = rtrim($search_str,'OR');
			$search_str .= ') ';
$query .= $search_str;
}






if($iSortCol_0 != 0){
	$order_str .= ' ORDER BY '.$table.'.'.$all_fields[$iSortCol_0-1]['COLUMN_NAME'].' '.$sSortDir_0.' ';
	$query .= $order_str;
}else{
	$order_str .= ' ORDER BY '.$table.'.'.$this->get_primary_key($table).' '.$sSortDir_0.' ';
	$query .= $order_str;
}

// $query .= ' LIMIT '.$iDisplayStart.','.$iDisplayLength.' ';

$result = $this->db->query($query);
return $result->num_rows();
}

}
