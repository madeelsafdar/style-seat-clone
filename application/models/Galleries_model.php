<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class galleries_model extends CI_Model{

	function __construct()
    {
        parent::__construct();

    }

    public function get_last_album_id_of_user($user_id = '')
    {
    	$query = "SELECT * FROM galleries WHERE user_id = $user_id ORDER BY  created_at DESC LIMIT 1";
    	$result = $this->db->query($query);
    	return $result->row();
    }

    // public function get_gallery_images_by_id($gallery_id = ''){
    //      $query = "SELECT
    //     galleries.*,
    //     (SELECT GROUP_CONCAT(pictures.file_name,',',pictures.picture_title,',',pictures.picture_id,',',pictures.views,',',(SELECT COUNT(*) FROM picture_votes WHERE picture_votes.picture_id = pictures.picture_id AND picture_votes.is_up = 1) SEPARATOR '|') FROM pictures WHERE pictures.gallery_id = galleries.gallery_id) AS images_comma_seperated
    //     FROM
    //     galleries
    //     INNER JOIN pictures
    //     ON(pictures.gallery_id = galleries.gallery_id)
    //     WHERE
    //     galleries.gallery_id = $gallery_id";
    //     $result = $this->db->query($query);
    //     return $result->row();

    // }

    public function get_gallery_images_by_id($gallery_id = '',$picture_number = ''){

         $next_picture = $picture_number + 1;

         if($picture_number > 1){
            $prev_picture = $picture_number - 1;
         }else{
            $prev_picture = 0;
         }

         $query = "SELECT
                    galleries.*
                    ,COUNT(pictures.picture_id) AS total_pictures

                    ,(
                    SELECT 
                    pictures_2.picture_id
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $picture_number,1
                    ) AS picture_id

                    ,(
                    SELECT 
                    pictures_2.picture_title
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $picture_number,1
                    ) AS picture_title

                    ,(
                    SELECT 
                    pictures_2.discription
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $picture_number,1
                    ) AS discription

                    ,(
                    SELECT 
                    pictures_2.file_name
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $picture_number,1
                    ) AS file_name

                    ,(
                    SELECT 
                    pictures_2.views
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $picture_number,1
                    ) AS views

                    ,(
                    SELECT 
                    COUNT(picture_votes.picture_votes_id)
                    FROM pictures AS pictures_2 
                    LEFT JOIN picture_votes
                    ON(picture_votes.picture_id = pictures_2.picture_id)
                    WHERE pictures_2.gallery_id = galleries.gallery_id 
                    AND picture_votes.is_up = 1
                    LIMIT $picture_number,1
                    ) AS votes

                    ,(
                    SELECT 
                    pictures_2.picture_id
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $next_picture,1
                    ) AS next_picture_id

                    ,(
                    SELECT 
                    pictures_2.picture_id
                    FROM pictures AS pictures_2 
                    WHERE pictures_2.gallery_id = galleries.gallery_id LIMIT $prev_picture,1
                    ) AS prev_picture_id

                    FROM
                    galleries
                    INNER JOIN pictures
                    ON(pictures.gallery_id = galleries.gallery_id)
                    WHERE
                    galleries.gallery_id = '$gallery_id'";
        $result = $this->db->query($query);
        return $result->row();

    }

    public function get_gallery_categories($gallery_id='')
    {
       $query = "SELECT
       (SELECT GROUP_CONCAT(gallery_categories.category_id SEPARATOR '|') FROM gallery_categories INNEr JOIN categories ON (categories.category_id = gallery_categories.category_id) WHERE gallery_id = galleries.gallery_id) AS categories_of_gallery_ids 
       FROM
       galleries
       WHERE
       galleries.gallery_id = $gallery_id";
       $result = $this->db->query($query);
       return $result->row();

   }

   public function get_all_galleries_by_id($user_id = '')
   {
    $query = "SELECT
    galleries.*,
    (SELECT GROUP_CONCAT(pictures.file_name,',',pictures.picture_title,',',pictures.picture_id SEPARATOR '|') FROM pictures WHERE pictures.gallery_id = galleries.gallery_id) AS images_comma_seperated
    FROM
    galleries
    WHERE
    galleries.user_id = $user_id";
    $result = $this->db->query($query);
    return $result->result_array();
}

public function get_all_galleries($loaded_result = '',$items_per_load = '',$set_post_order_by_created_at = '',$set_post_order_by_views = ''){

    $order_str = "";

    if ($set_post_order_by_created_at != '' && $set_post_order_by_created_at != 0) {
        $order_str = " ORDER BY galleries.created_at DESC ";
    }else  if ($set_post_order_by_views != '' && $set_post_order_by_views != 0) {
        $order_str = " ORDER BY galleries.gallery_views DESC ";
    }else{
        $order_str = " ORDER BY galleries.created_at DESC ";
    }

    $query = "
    SELECT
    galleries.*
    ,(SELECT COUNT(gallery_votes1.gallery_votes_id) FROM gallery_votes AS gallery_votes1 
        WHERE gallery_votes1.gallery_id = galleries.gallery_id AND gallery_votes1.is_up = 1) AS vote_up
,(SELECT COUNT(gallery_votes2.gallery_votes_id) FROM gallery_votes AS gallery_votes2 
    WHERE gallery_votes2.gallery_id = galleries.gallery_id AND gallery_votes2.is_up = 0) AS vote_down
,(SELECT COUNT(pictures.picture_id) FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS no_of_pictures
,(SELECT pictures.picture_id FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_id
,(SELECT pictures.picture_title FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_title
,(SELECT pictures.file_name FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS file_name
,(SELECT COUNT(galleries.gallery_id) FROM galleries $order_str) AS total_records
FROM
galleries
$order_str LIMIT ".$loaded_result.", ".$items_per_load."
";

$result = $this->db->query($query);
return $result->result_array();
}

public function get_all_galleries_by_category($loaded_result = '',$items_per_load = '',$category_id = '')
{
    $query = "
    SELECT
    galleries.*
    ,(SELECT COUNT(gallery_votes1.gallery_votes_id) FROM gallery_votes AS gallery_votes1 
        WHERE gallery_votes1.gallery_id = galleries.gallery_id AND gallery_votes1.is_up = 1) AS vote_up
,(SELECT COUNT(gallery_votes2.gallery_votes_id) FROM gallery_votes AS gallery_votes2 
    WHERE gallery_votes2.gallery_id = galleries.gallery_id AND gallery_votes2.is_up = 0) AS vote_down
,(SELECT COUNT(pictures.picture_id) FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS no_of_pictures
,(SELECT pictures.picture_id FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_id
,(SELECT pictures.picture_title FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_title
,(SELECT pictures.file_name FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS file_name
,(SELECT COUNT(galleries.gallery_id) FROM galleries INNER JOIN gallery_categories
ON (gallery_categories.gallery_id = galleries.gallery_id)
WHERE gallery_categories.category_id = $category_id ORDER BY galleries.created_at DESC) AS total_records
FROM
galleries

INNER JOIN gallery_categories
ON (gallery_categories.gallery_id = galleries.gallery_id)
WHERE gallery_categories.category_id = $category_id

ORDER BY galleries.created_at DESC LIMIT $loaded_result, $items_per_load
";

$result = $this->db->query($query);
return $result->result_array();
}

public function get_all_galleries_by_search_str($loaded_result = '',$items_per_load = '',$search_str = '')
{
    $query = "
    SELECT
    galleries.*
    ,(SELECT COUNT(gallery_votes1.gallery_votes_id) FROM gallery_votes AS gallery_votes1 
        WHERE gallery_votes1.gallery_id = galleries.gallery_id AND gallery_votes1.is_up = 1) AS vote_up
,(SELECT COUNT(gallery_votes2.gallery_votes_id) FROM gallery_votes AS gallery_votes2 
    WHERE gallery_votes2.gallery_id = galleries.gallery_id AND gallery_votes2.is_up = 0) AS vote_down
,(SELECT COUNT(pictures.picture_id) FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS no_of_pictures
,(SELECT pictures.picture_id FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_id
,(SELECT pictures.picture_title FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_title
,(SELECT pictures.file_name FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS file_name
,(SELECT COUNT(galleries.gallery_id) FROM galleries INNER JOIN gallery_categories
ON (gallery_categories.gallery_id = galleries.gallery_id)
WHERE galleries.gallery_title LIKE '%$search_str%' OR galleries.gallery_description LIKE '%$search_str%' ORDER BY galleries.created_at DESC) AS total_records
FROM
galleries

INNER JOIN gallery_categories
ON (gallery_categories.gallery_id = galleries.gallery_id)
WHERE galleries.gallery_title LIKE '%$search_str%' OR galleries.gallery_description LIKE '%$search_str%'

ORDER BY galleries.created_at DESC LIMIT $loaded_result, $items_per_load
";

$result = $this->db->query($query);
return $result->result_array();
}

public function get_gallery_views($gallery_id = ''){
    $query = "SELECT views FROM galleries WHERE gallery_id = $gallery_id";
    $result = $this->db->query($query);
    return $result->row();
}

public function get_related_galleries($image_id = '')
{
    $query = "

    SELECT 
    *
    ,(SELECT pictures.picture_id FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_id
    ,(SELECT pictures.picture_title FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS picture_title
    ,(SELECT pictures.file_name FROM pictures WHERE pictures.gallery_id = galleries.gallery_id ORDER BY created_at DESC LIMIT 1 ) AS file_name
    FROM galleries
    INNER JOIN gallery_categories
    ON (galleries.gallery_id = gallery_categories.gallery_id)
    WHERE gallery_categories.category_id IN (
        SELECT 
        gallery_categories.category_id 
        FROM pictures 
        INNER JOIN gallery_categories 
        ON(gallery_categories.gallery_id = pictures.gallery_id) 
        WHERE picture_id = $image_id
        )";
$result = $this->db->query($query);
return $result->result_array();
}

}