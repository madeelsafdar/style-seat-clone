<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class video_model extends CI_Model{

	function __construct()
	{
		parent::__construct();

	}

	public function get_all_my_videos()
	{
		$user = $this->ion_auth->user()->row();
		$query = "SELECT * FROM embeded_video_user WHERE embeded_video_user.user_id = $user->id ";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function get_all_videos()
	{
		$query = "SELECT * FROM embeded_video_user";
		$result = $this->db->query($query);
		return $result->result_array();
	}

}