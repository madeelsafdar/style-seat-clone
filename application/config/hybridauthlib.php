<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

// object(Hybrid_User_Profile)#34 (22) { ["identifier"]=> int(515252977) ["webSiteURL"]=> NULL ["profileURL"]=> string(33) "http://twitter.com/adeelsafdar007" ["photoURL"]=> string(75) "http://pbs.twimg.com/profile_images/481230094506024961/Xy3UP1Vd_normal.jpeg" ["displayName"]=> string(14) "adeelsafdar007" ["description"]=> string(0) "" ["firstName"]=> string(12) "Adeel Safdar" ["lastName"]=> NULL ["gender"]=> NULL ["language"]=> NULL ["age"]=> NULL ["birthDay"]=> NULL ["birthMonth"]=> NULL ["birthYear"]=> NULL ["email"]=> NULL ["emailVerified"]=> NULL ["phone"]=> NULL ["address"]=> NULL ["country"]=> NULL ["region"]=> string(0) "" ["city"]=> NULL ["zip"]=> NULL } 

// object(Hybrid_User_Profile)#34 (22) { ["identifier"]=> string(21) "106392365398031026532" ["webSiteURL"]=> NULL ["profileURL"]=> string(49) "https://profiles.google.com/106392365398031026532" ["photoURL"]=> string(92) "https://lh3.googleusercontent.com/-hqIYVPDVYAo/AAAAAAAAAAI/AAAAAAAABH0/AeBbKdRJJPs/photo.jpg" ["displayName"]=> string(12) "adeel safdar" ["description"]=> NULL ["firstName"]=> string(5) "adeel" ["lastName"]=> string(6) "safdar" ["gender"]=> string(4) "male" ["language"]=> string(5) "en-GB" ["age"]=> NULL ["birthDay"]=> NULL ["birthMonth"]=> NULL ["birthYear"]=> NULL ["email"]=> string(24) "adeelsafdar007@gmail.com" ["emailVerified"]=> string(24) "adeelsafdar007@gmail.com" ["phone"]=> NULL ["address"]=> NULL ["country"]=> NULL ["region"]=> NULL ["city"]=> NULL ["zip"]=> NULL } 

$config =
	array(
		// set on "base_url" the relative url that point to HybridAuth Endpoint
		'base_url' => '/hauth/endpoint',

		"providers" => array (
			// openid providers
			"OpenID" => array (
				"enabled" => true
			),

			"Yahoo" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "dj0yJmk9Z01tVFlXME9QNkNMJmQ9WVdrOWJtMW9kRmRITjJNbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0wNA--", "secret" => "d40dd96c7a0f88255ef90158f682c1435d614fbd" ),
			),

			"AOL"  => array (
				"enabled" => true
			),

			"Google" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "418399209199-b8le9lhmua6u9uohfk2l5s05koslj88q.apps.googleusercontent.com", "secret" => "G87xgtITS-1LkPPpWHZFw4I_" ),
			),

			"Facebook" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "594524383918641", "secret" => "8a217ee8e4cbaaae2e0cddf04ba6e3e8" ),
			),

			"Twitter" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "B1FDmKeMQ3kiXbrvFU82vzcYM", "secret" => "zGcBVAErWwNm5H5c4xFZ0uTIsvMAAE59cUP6bgsWt3UhwIqBZ3" )
			),

			// windows live
			"Live" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" )
			),

			"MySpace" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"LinkedIn" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"Foursquare" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" )
			),
		),

		// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
		"debug_mode" => (ENVIRONMENT == 'development'),

		"debug_file" => APPPATH.'/logs/hybridauth.log',
	);


/* End of file hybridauthlib.php */
/* Location: ./application/config/hybridauthlib.php */